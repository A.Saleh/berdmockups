<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Community creation page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">

</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="index.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="dashboard.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main> 
        
        <form class="ui form communities-creation">
            
            <div class="ui centered container grid">
                <div class="row">
                    <div class="center aligned eight wide computer sixteen wide mobile twelve wide tablet column">
                        <h2 class="ui header mt-5">Setup your new community</h2>
                        <div class="ui divider">

                        </div>
                    </div>
                </div>
                <div class="left aligned row">
                    <div class="eight wide computer sixteen wide mobile twelve wide tablet column">
                        <div class="field invenio-text-input-field">
                            <label for="metadata.title" class="field-label-class invenio-field-label">
                                <i aria-hidden="true" class="book icon"></i>Community name</label>
                                <div class="ui fluid input"><input name="metadata.title" id="metadata.title" type="text" value="">
                            </div>
                        </div>
                        <div class="field text-muted"><label for="slug" class="field-label-class invenio-field-label">
                            <i aria-hidden="true" class="barcode icon"></i>Identifier</label>
                            <div class="ui fluid input"><input name="slug" id="slug" type="text" value="">
                        </div>
                    </div>
                    
                        <h3 class="ui header">Community visibility</h3>
                        <div class="field invenio-radio-field">
                            <div class="ui checked radio checkbox">
                                <input class="hidden" name="access.visibility" readonly="" tabindex="0" type="radio" value="public" checked="">
                                <label for="access.visibility" class="field-label-class invenio-field-label">
                                    <i aria-hidden="true" class="group icon"></i>Public</label>
                                </div>
                            </div>
                            <div class="field invenio-radio-field">
                                <div class="ui radio checkbox">
                                    <input class="hidden" name="access.visibility" readonly="" tabindex="0" type="radio" value="public">
                                    <label for="access.visibility" class="field-label-class invenio-field-label">
                                        <i aria-hidden="true" class="lock icon"></i>Restricted</label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="center aligned column">
                                <button class="ui icon positive left labeled button" type="button" onclick="window.location='community_management_page_settings_tab.php';">
                                    <i aria-hidden="true" class="plus icon"></i>Create community</button>
                            </div>
                        </div>
                    </div>
        </form>
               

    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>

</html>