<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Request</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex mt-4">
            <div class="d-flex flex-column col-8">
                <h1 class="mt-4">Request a Dataset for Research Project</h1> 
                <hr>
                <div>
                    <h3 class="mt-5">Please enter the information about your research project</h3>
                    <p style="font-size: 14px">(Boxes with a * are obligatory)</p> 
                    <hr>
                </div>
                <form style="width: 550px;">
                    <h5 class="mt-3 mb-3"><nobr>Enter the title of your research project*</nobr></h5>
                    <div class="mb-3 input-group mt-2">
                        <div class="input-group-text"><i class="bi bi-card-text"></i></div>
                        <input type="text" class="form-control" id="inputName" placeholder="Title">
                    </div>
                    <h5 class="mt-5"><nobr>Describe what your research project is about*</nobr></h5>
                    <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                </form>
                <div>
                    <h3 class="mt-5">Please enter the information about the dataset you want to request</h3>
                    <p style="font-size: 14px">(Boxes with a * are obligatory)</p> 
                    <hr>
                </div>
                <form style="width: 550px;">
                    <h5 class="mt-3 mb-3"><nobr>Describe the dataset with a title of your choice</nobr></h5>
                    <div class="mb-3 input-group mt-2">
                        <div class="input-group-text"><i class="bi bi-card-text"></i></div>
                        <input type="text" class="form-control" id="inputName" placeholder="Title">
                    </div>
                    <h5 class="mt-5"><nobr>What metadata should your target dataset have?</nobr></h5>
                    <p style="font-size: 14px">(<a href="#" style="color: black">Learn More</a>)</p>
                    <div class="dropdown mb-3">
                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Resource Type
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Publication" onclick="updateDataFormat(this.value)">Publication</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Dataset" onclick="updateDataFormat(this.value)">Dataset</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Image" onclick="updateDataFormat(this.value)">Image</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Video" onclick="updateDataFormat(this.value)">Video</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Audio" onclick="updateDataFormat(this.value)">Audio</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Other" onclick="updateDataFormat(this.value)">Other</a></li>
                        </ul>
                    </div>
                    <div class="dropdown mb-3">
                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Language
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="javascript:void(0)">English</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">German</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">French</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">Italian</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">Spanish</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">Other</a></li>
                        </ul>
                    </div>
                    <div class="dropdown mb-3">
                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Data-Format
                        </button>
                        <ul class="dropdown-menu" id="dataformat" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="javascript:void(0)">pdf</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">doc</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">mp3</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">wma</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">mp4</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)">Other</a></li>
                        </ul>
                    </div>
                    <h5 class="mt-5">Give a detailed description about the required dataset and why it's important for your project*</h5>
                    <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                    <button type="submit" class="mt-4 btn btn-success">Submit</button>
                </form>
            </div>
            <div class="d-flex col-1"></div>
            <div class="d-flex flex-column ps-2 col-3 mt-4 justify-content-evenly" style="border: black 1px solid; height: 340px; width: 320px; position: sticky; top: 50px;">
                <div>
                    <h5>Need help with the request?</h5>
                    <div>
                        <button type="button" class="btn btn-light mx-2 mt-2">&#8594 Click Here</button>
                    </div>
                </div>
                <div>
                    <h5>Don't have all the informations avaliable right now and want to continue later?</h5>
                    <div>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-lock">&nbspSave Progress</button>
                        <h5 class="mt-3 mx-2">or</h5>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-x" onclick="history.back()">&nbspCancel</button>
                    </div>
                </div>    
            </div>
        </div>

        <script>
            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });
        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>