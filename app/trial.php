<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>

<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
                
        <div class="container record-detail">
            <div class="row">
                <div class="col-sm-8 col-md-8 col-left">
                    <p>
                        <time datetime="August 19, 2022" data-toggle="tooltip" title="" data-original-title="Publication date">August 19, 2022</time>
                        <span class="pull-right">
                        <span class="label label-default">Figure</span>
                        <span class="label label-success">
                        Open Access
                        </span>
                        </span>
                    </p>
                    <h1>FIGURE 4 in Two new species of the genera Leiophron Nees and Paroligoneurus Muesebeck (Hymenoptera: Braconidae) from northern India</h1>
                    <p>
                        <span class="text-muted" data-toggle="tooltip" title="" data-original-title="ICAR-National Bureau of Agricultural Insect Resources, Post Bag No. 2491, H. A. Farm Post, Bellary Road, Hebbal, Bangalore 560 024, Karnataka, India">Gupta, Ankita</span>; 
                        <a href="https://orcid.org/0000-0002-6495-4853"><img class="inline-orcid" src="/static/img/orcid.png"></a>
                        <span class="text-muted" data-toggle="tooltip" title="" data-original-title="Naturalis Biodiversity Center, Darwinweg 2, 2333 CR Leiden, the Netherlands. https://orcid.org/0000-0002-6495-4853">Achterberg, Cornelis Van</span>; 
                        <a href="https://orcid.org/0000-0002-2243-3777"><img class="inline-orcid" src="/static/img/orcid.png"></a>
                        <span class="text-muted" data-toggle="tooltip" title="" data-original-title="ICAR-National Bureau of Agricultural Insect Resources, Post Bag No. 2491, H. A. Farm Post, Bellary Road, Hebbal, Bangalore 560 024, Karnataka, India &amp; https://orcid.org/0000-0002-2243-3777">Pattar, Rohit</span>; 
                        <a href="https://orcid.org/0000-0002-3910-7839"><img class="inline-orcid" src="/static/img/orcid.png"></a>
                        <span class="text-muted" data-toggle="tooltip" title="" data-original-title="ICAR-National Bureau of Agricultural Insect Resources, Post Bag No. 2491, H. A. Farm Post, Bellary Road, Hebbal, Bangalore 560 024, Karnataka, India &amp; https://orcid.org/0000-0002-3910-7839">Navik, Omprakash</span>; 
                        <a href="https://orcid.org/0000-0002-3151-8959"><img class="inline-orcid" src="/static/img/orcid.png"></a>
                        <span class="text-muted" data-toggle="tooltip" title="" data-original-title="ICAR-National Bureau of Agricultural Insect Resources, Post Bag No. 2491, H. A. Farm Post, Bellary Road, Hebbal, Bangalore 560 024, Karnataka, India &amp; https://orcid.org/0000-0002-3151-8959">Mahendiran, G.</span>
                    </p>
                    <div class="record-description">FIGURE 4. Paroligoneurus indicus sp. nov. A. Head in frontal aspect; B. Head in dorsal aspect; C. Head and mesosoma (dorsal); D. Propodeum, T1 and T2; E. Wings; F. Head and mesopleuron (lateral).</div>
                    <div class="alert alert-warning record-notes">
                        Published as part of <i>Gupta, Ankita, Achterberg, Cornelis Van, Pattar, Rohit, Navik, Omprakash &amp; Mahendiran, G., 2022, Two new species of the genera Leiophron Nees and Paroligoneurus Muesebeck (Hymenoptera: Braconidae) from northern India, pp. 593-599 in Zootaxa 5175 (5)</i> on page 598, DOI: 10.11646/zootaxa.5175.5.8, <a href="http://zenodo.org/record/7009604">http://zenodo.org/record/7009604</a>
                    </div>
                    <div class="panel panel-default" id="preview">
                        <div class="panel-heading">
                        <a class="panel-toggle" data-toggle="collapse" href="#collapseOne">
                        Preview
                        <span class="pull-right show-on-collapsed"><i class="fa fa-chevron-right"></i></span>
                        <span class="pull-right hide-on-collapsed"><i class="fa fa-chevron-down"></i></span>
                        </a>
                        </div>
                        <div id="collapseOne" class="collapse in">
                        <iframe class="preview-iframe" id="preview-iframe" width="100%" height="400" src="/record/7009616/preview/figure.png"></iframe>
                        </div>
                    </div>
                    <div class="panel panel-default files-box" id="files">
                        <div class="panel-heading">
                        <a class="panel-toggle" data-toggle="collapse" href="#collapseTwo">
                        Files
                        <span class="pull-right show-on-collapsed"><i class="fa fa-chevron-right"></i></span>
                        <span class="pull-right hide-on-collapsed"><i class="fa fa-chevron-down"></i></span>
                        </a>
                        <small class="text-muted"> (14.2 MB)</small>
                        </div>
                        <div class="collapse in" id="collapseTwo">
                        <table class="table table-striped">
                            <thead>
                                <tr class="">
                                    <th>Name</th>
                                    <th>Size</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                    <a class="filename" href="/record/7009616/files/figure.png?download=1">figure.png</a>
                                    <br><small class="text-muted nowrap">md5:3b6222504b9e52fc02faa0849476cbbd <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="This is the file fingerprint (MD5 checksum), which can be used to verify the file integrity."></i></small>
                                    </td>
                                    <td class="nowrap">14.2 MB</td>
                                    <td class="nowrap"><span class="pull-right"><button class="btn preview-link btn-xs btn-default" data-filename="figure.png"><i class="fa fa-eye"></i> Preview</button> <a class="btn btn-xs btn-default" href="/record/7009616/files/figure.png?download=1"><i class="fa fa-download"></i> Download</a></span></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div id="citations" class="ng-scope">
                        <invenio-search disable-url-handler="true" search-endpoint="https://zenodo-broker.web.cern.ch/api/relationships" search-extra-params="{&quot;group_by&quot;: &quot;version&quot;, &quot;id&quot;: &quot;10.5281/zenodo.7009615&quot;, &quot;page&quot;: 1, &quot;size&quot;: 10}" search-hidden-params="{&quot;relation&quot;: &quot;isCitedBy&quot;, &quot;scheme&quot;: &quot;doi&quot;}" search-headers="{&quot;Accept&quot;: &quot;application/json&quot;}">
                        <div class="panel panel-default" id="citation">
                            <div class="panel-heading">
                                <!-- Beta ribbon -->
                                <div class="row" style="margin-bottom: -15px;">
                                    <div class="col-sm-1">
                                    <div class="ribbon-wrapper-green">
                                        <a href="https://help.zenodo.org/#citations" target="_blank" rel="noopener noreferrer">
                                            <div class="corner-ribbon top-left ribbon-green">Beta</div>
                                        </a>
                                    </div>
                                    </div>
                                    <div class="col-sm-11" style="margin-top: 5px;">
                                    <a class="panel-toggle" data-toggle="collapse" href="#collapseCitations" style="margin-left:-25px;">
                                    Citations
                                    </a>
                                    <a href="https://help.zenodo.org/#citations" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-question-circle"></i>
                                    </a>
                                    <small class="text-muted">
                                        <invenio-search-count template="/static/templates/citations/count.html">
                                            <!-- ngIf: vm.invenioSearchResults.hits.total > 0 -->
                                            <!-- ngIf: !vm.invenioSearchResults.hits.total > 0 --><span ng-if="!vm.invenioSearchResults.hits.total > 0" class="ng-scope">
                                            <span class="badge citationBadge"> 0</span>
                                            </span><!-- end ngIf: !vm.invenioSearchResults.hits.total > 0 -->
                                        </invenio-search-count>
                                    </small>
                                    <a class="panel-toggle" data-toggle="collapse" href="#collapseCitations" style="margin-left:-25px;">
                                    <span class="pull-right show-on-collapsed"><i class="fa fa-chevron-right"></i></span>
                                    <span class="pull-right hide-on-collapsed"><i class="fa fa-chevron-down"></i></span>
                                    </a>
                                    </div>
                                </div>
                                <!-- Without beta ribbon -->
                                <!--<a class="panel-toggle" data-toggle="collapse" href="#collapseCitations">-->
                                <!--Citations-->
                                <!--<small class="text-muted">-->
                                <!--<invenio-search-count-->
                                <!--template="/static/templates/citations/count.html">-->
                                <!--</invenio-search-count>-->
                                <!--</small>-->
                                <!--<span class="pull-right show-on-collapsed"><i class="fa fa-chevron-right"></i></span>-->
                                <!--<span class="pull-right hide-on-collapsed"><i class="fa fa-chevron-down"></i></span>-->
                                <!--</a>-->
                            </div>
                            <div id="collapseCitations" class="collapse in">
                                <div class="search-page">
                                    <div class="container-fluid facets">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <invenio-search-facets ng-init="vm.record_id='10.5281/zenodo.7009616'; vm.version_id='10.5281/zenodo.7009615'" template="/static/templates/citations/facets.html">
                                                <div class="row">
                                                <div class="col-sm-3">Show only:</div>
                                                <div class="col-sm-9" style="margin-left:-35px">
                                                    <!-- Type filter-->
                                                    <div class="row">
                                                        <!-- ngRepeat: item in vm.invenioSearchResults.aggregations.type.buckets track by $index -->
                                                        <!-- ngRepeat: type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index --><!-- ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><span class="titlecase filter ng-scope" ng-if="vm.invenioSearchResults.aggregations.type.buckets.length != 4" ng-repeat="type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index">
                                                        <input type="checkbox" class="disabled" disabled="">
                                                        <label class="ng-binding">literature (0)</label>
                                                        </span><!-- end ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><!-- end ngRepeat: type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index --><!-- ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><span class="titlecase filter ng-scope" ng-if="vm.invenioSearchResults.aggregations.type.buckets.length != 4" ng-repeat="type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index">
                                                        <input type="checkbox" class="disabled" disabled="">
                                                        <label class="ng-binding">dataset (0)</label>
                                                        </span><!-- end ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><!-- end ngRepeat: type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index --><!-- ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><span class="titlecase filter ng-scope" ng-if="vm.invenioSearchResults.aggregations.type.buckets.length != 4" ng-repeat="type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index">
                                                        <input type="checkbox" class="disabled" disabled="">
                                                        <label class="ng-binding">software (0)</label>
                                                        </span><!-- end ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><!-- end ngRepeat: type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index --><!-- ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><span class="titlecase filter ng-scope" ng-if="vm.invenioSearchResults.aggregations.type.buckets.length != 4" ng-repeat="type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index">
                                                        <input type="checkbox" class="disabled" disabled="">
                                                        <label class="ng-binding">unknown (0)</label>
                                                        </span><!-- end ngIf: vm.invenioSearchResults.aggregations.type.buckets.length != 4 --><!-- end ngRepeat: type in vm.invenioSearchResults.aggregations.type.buckets | missingTypes track by $index -->
                                                    </div>
                                                    <!-- Version filter-->
                                                    <div class="row filter">
                                                        <input id="version-filter" type="checkbox" ng-checked="!vm.invenioSearchArgs.group_by === 'version'" ng-click="vm.invenioSearchArgs.type=[];
                                                            vm.invenioSearchArgs.group_by = (vm.invenioSearchArgs.group_by === 'identity' ? 'version' : 'identity');
                                                            vm.invenioSearchArgs.id = (vm.invenioSearchArgs.group_by === 'version' ? vm.version_id : vm.record_id);">
                                                        <label for="version-filter">Citations to this version</label>
                                                    </div>
                                                </div>
                                                </div>
                                            </invenio-search-facets>
                                        </div>
                                        <div class="col-sm-3">
                                            <invenio-search-bar template="/static/templates/invenio_search_ui/searchbar.html" placeholder="Search">
                                                <!--
                                                This file is part of Invenio.
                                                Copyright (C) 2016-2018 CERN.
                                                
                                                Invenio is free software; you can redistribute it and/or modify it
                                                under the terms of the MIT License; see LICENSE file for more details.
                                                -->
                                                <form class="navbar-form navbar-left ng-pristine ng-valid" role="search">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" placeholder="Search" ng-model="vm.userQuery" ng-model-options="{ debounce: { 'default': 500, 'blur': 0 } }" ng-keyup="$event.keyCode == 13 &amp;&amp; updateQuery()">
                                                        <div class="input-group-btn">
                                                            <button ng-click="updateQuery()" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </invenio-search-bar>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="container-fluid">
                                    <invenio-search-error template="/static/templates/zenodo_search_ui/error.html" message="Error">
                                        <div ng-show="vm.invenioSearchError.name" class="ng-hide">
                                            <div class="alert alert-danger ng-binding">
                                                <i class="fa fa-bolt"></i> <strong translate="">Error:</strong> 
                                                <!-- ngIf: vm.invenioSearchErrorResults.error_id -->
                                            </div>
                                        </div>
                                    </invenio-search-error>
                                    </div>
                                    <invenio-search-results template="/static/templates/citations/results.html">
                                    <!-- ngIf: vm.invenioSearchResults.hits.total > 0 -->
                                    <!-- ngIf: vm.invenioSearchResults.hits.total == 0 -->
                                    <div ng-if="vm.invenioSearchResults.hits.total == 0" class="no-results text-center ng-scope">
                                        No citations.
                                    </div>
                                    <!-- end ngIf: vm.invenioSearchResults.hits.total == 0 -->
                                    </invenio-search-results>
                                    <div class="row">
                                    <div class="col-md-2 col-sm-12">
                                    </div>
                                    <div class="col-md-7 col-sm-12 text-center">
                                        <invenio-search-pagination template="/static/templates/citations/pagination.html">
                                            <!-- ngIf: vm.invenioSearchResults.hits.total > 0 -->
                                        </invenio-search-pagination>
                                    </div>
                                    <div class="col-md-3 col-sm-12" style="padding-top: 5px;">
                                        <invenio-search-select-box sort-key="size" available-options="{
                                            &quot;options&quot;: [
                                            {
                                            &quot;title&quot;: &quot;10&quot;,
                                            &quot;value&quot;: &quot;10&quot;
                                            },
                                            {
                                            &quot;title&quot;: &quot;20&quot;,
                                            &quot;value&quot;: &quot;20&quot;
                                            },
                                            {
                                            &quot;title&quot;: &quot;50&quot;,
                                            &quot;value&quot;: &quot;50&quot;
                                            }
                                            ]}" template="/static/templates/citations/selectbox.html">
                                            <!-- ngIf: vm.invenioSearchResults.hits.total > 0 -->
                                        </invenio-search-select-box>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </invenio-search>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-right">
                    <div class="well">
                        <!-- Stats -->
                        <div class="row stats-box">
                        <div id="accordion">
                            <!-- Banner -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="stats-data">0</span>
                                </div>
                                <div class="col-sm-6">
                                    <span class="stats-data">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <i class="fa fa-eye " data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Total views."></i> views
                                </div>
                                <div class="col-sm-6">
                                    <i class="fa fa-download " data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Total downloads."></i> downloads
                                </div>
                            </div>
                            <!-- Collapsed details -->
                            <div id="toggle-stats" class="row">
                                <div class="col-sm-12">
                                    <a style="cursor: pointer;" class="panel-toggle" data-toggle="collapse" data-target="#collapse-stats" aria-expanded="true" aria-controls="collapse-stats">
                                    See more details...
                                    </a>
                                </div>
                            </div>
                            <div id="collapse-stats" class="collapse" aria-labelledby="toggle-stats" data-parent="#accordion">
                                <table class="table stats-table">
                                    <!-- Skip table header if no versions -->
                                    <tbody>
                                    <tr>
                                        <th></th>
                                        <th>All versions</th>
                                        <th>This version</th>
                                    </tr>
                                    <tr>
                                        <td>Views <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Total views."></i></td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Downloads <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Total downloads."></i></td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Data volume <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Total download volume."></i></td>
                                        <td>0 Bytes</td>
                                        <td>0 Bytes</td>
                                    </tr>
                                    <tr>
                                        <td>Unique views <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Views in one hour user-sessions are counted only once."></i></td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>Unique downloads <i class="fa fa-question-circle text-muted" data-toggle="tooltip" tooltip="" data-placement="top" title="" data-original-title="Downloads in one hour user-sessions are counted only once."></i></td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <a href="https://help.zenodo.org/#statistics">More info on how stats are collected.</a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="well metadata">
                        <small class="text-muted">Part of</small>
                        <a href="/communities/biosyslit/about/"><img src="/api/files/00000000-0000-0000-0000-000000000000/biosyslit/logo.png" class="img-thumbnail" width="100%"></a>
                        <small class="text-muted">Indexed in</small>
                        <a href="https://explore.openaire.eu/search/dataset?pid=10.5281/zenodo.7009616">
                        <img src="/static/img/openaire-horizontal-old.png" class="img-thumbnail" width="100%">
                        </a>
                    </div>
                    <div class="well metadata">
                        <dl>
                        <dt>Publication date:</dt>
                        <dd>August 19, 2022</dd>
                        <dt>DOI:</dt>
                        <dd>
                            <span class="get-badge" data-toggle="`tooltip" data-placement="bottom" title="Get the DOI badge!">
                            <img data-toggle="modal" data-target="[data-modal='10.5281/zenodo.7009616']" src="/badge/DOI/10.5281/zenodo.7009616.svg" alt="10.5281/zenodo.7009616">
                            </span>
                            <div class="modal fade badge-modal" data-modal="10.5281/zenodo.7009616">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-body">
                                        <h4>Zenodo DOI Badge</h4>
                                        <h4>
                                            <small>DOI</small>
                                        </h4>
                                        <h4>
                                            <pre>10.5281/zenodo.7009616</pre>
                                        </h4>
                                        <h4>
                                            <small>Markdown</small>
                                        </h4>
                                        <h4>
                                            <pre>[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7009616.svg)](https://doi.org/10.5281/zenodo.7009616)</pre>
                                        </h4>
                                        <h4>
                                            <small>reStructedText</small>
                                        </h4>
                                        <h4>
                                            <pre>.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.7009616.svg
                        :target: https://doi.org/10.5281/zenodo.7009616</pre>
                                        </h4>
                                        <h4>
                                            <small>HTML</small>
                                        </h4>
                                        <h4>
                                            <pre>&lt;a href="https://doi.org/10.5281/zenodo.7009616"&gt;&lt;img src="https://zenodo.org/badge/DOI/10.5281/zenodo.7009616.svg" alt="DOI"&gt;&lt;/a&gt;</pre>
                                        </h4>
                                        <h4>
                                            <small>Image URL</small>
                                        </h4>
                                        <h4>
                                            <pre>https://zenodo.org/badge/DOI/10.5281/zenodo.7009616.svg</pre>
                                        </h4>
                                        <h4>
                                            <small>Target URL</small>
                                        </h4>
                                        <h4>
                                            <pre>https://doi.org/10.5281/zenodo.7009616</pre>
                                        </h4>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </dd>
                        <dt>Keyword(s):</dt>
                        <dd>
                            <a class="label-link" href="/search?q=keywords%3A%22Biodiversity%22">
                            <span class="label label-default">Biodiversity</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Taxonomy%22">
                            <span class="label label-default">Taxonomy</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Animalia%22">
                            <span class="label label-default">Animalia</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Arthropoda%22">
                            <span class="label label-default">Arthropoda</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Insecta%22">
                            <span class="label label-default">Insecta</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Hymenoptera%22">
                            <span class="label label-default">Hymenoptera</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Braconidae%22">
                            <span class="label label-default">Braconidae</span>
                            </a>
                            <a class="label-link" href="/search?q=keywords%3A%22Paroligoneurus%22">
                            <span class="label label-default">Paroligoneurus</span>
                            </a>
                        </dd>
                        <dt>Published in:</dt>
                        <dd>
                            Zootaxa: 5175 pp. 593-599 (5).
                        </dd>
                        <dt>Related identifiers:</dt>
                        <dd>
                            Cited by<br>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="https://doi.org/10.5281/zenodo.7009541">10.5281/zenodo.7009541</a> (Taxonomic treatment)
                                </li>
                                <li>
                                    <a href="http://treatment.plazi.org/id/03D57A3C6D5BFF8CB1A841D271545DD1">http://treatment.plazi.org/id/03D57A3C6D5BFF8CB1A841D271545DD1</a> (Taxonomic treatment)
                                </li>
                            </ul>
                        </dd>
                        <dd>
                            Part of<br>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="https://doi.org/10.11646/zootaxa.5175.5.8">10.11646/zootaxa.5175.5.8</a> (Journal article)
                                </li>
                                <li>
                                    <i>urn:lsid:plazi.org:pub:FFEC02446D58FF8AB13F447470165F1C</i> (LSID) (Journal article)
                                </li>
                                <li>
                                    <a href="http://publication.plazi.org/id/FFEC02446D58FF8AB13F447470165F1C">http://publication.plazi.org/id/FFEC02446D58FF8AB13F447470165F1C</a> (Journal article)
                                </li>
                                <li>
                                    <a href="https://zenodo.org/record/7009604">https://zenodo.org/record/7009604</a> (Journal article)
                                </li>
                            </ul>
                        </dd>
                        <dt>Communities:</dt>
                        <dd>
                            <ul class="list-unstyled">
                                <li><a href="/communities/biosyslit/">Biodiversity Literature Repository</a></li>
                            </ul>
                        </dd>
                        <dt>License (for files):</dt>
                        <dd><a rel="license" href=""><i class="fa fa-external-link"></i> License Not Specified</a></dd>
                        </dl>
                    </div>
                    <div class="well metadata">
                        <h4>Versions</h4>
                        <table class="table">
                        <tbody>
                            <tr class="info">
                                <td>
                                    <a href="/record/7009616">Version 1 </a>
                                    <small class="text-muted">10.5281/zenodo.7009616</small>
                                </td>
                                <td align="right"><small class="text-muted">Aug 19, 2022</small></td>
                            </tr>
                        </tbody>
                        </table>
                        <small>
                        <strong>Cite all versions?</strong> You can cite all versions by using the DOI <a href="https://doi.org/10.5281/zenodo.7009615">10.5281/zenodo.7009615</a>. This DOI represents all versions, and will always resolve to the latest one. <a href="http://help.zenodo.org/#versioning">Read more</a>.
                        </small>
                    </div>
                    <div class="well">
                        <h4>Share</h4>
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="https://doi.org/10.5281/zenodo.7009616">
                        <a class="addthis_button_mendeley at300b" target="_blank" title="Mendeley" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(175, 18, 43); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-mendeley-1" title="Mendeley" alt="Mendeley" class="at-icon at-icon-mendeley" style="width: 32px; height: 32px;">
                                    <title id="at-svg-mendeley-1">Mendeley</title>
                                    <g>
                                    <path d="M26.24 18.08c-1.42-.08-1.95-.81-1.86-2.09.07-1.08.27-2.15.4-3.22-.02-1.68-.91-3.02-2.4-3.47-1.53-.45-2.87-.14-3.96 1.11-2.24 2.56-2.53 2.55-4.85-.04-1.11-1.24-2.76-1.6-4.24-.95-1.45.64-2.29 2.16-2.08 3.82.08.6.27 1.19.36 1.8.33 2.05-.1 2.61-2.15 3.04-1.03.22-1.85.76-1.98 1.92-.13 1.18.16 2.31 1.36 2.66.74.22 1.79.08 2.47-.31.93-.53 1.03-1.58.71-2.65-.6-1.98-.03-2.94 2.04-3.23.81-.11 1.7-.02 2.48.23 1.26.4 1.63 1.33 1.13 2.56-.43 1.07-.43 2.02.38 2.89.81.87 2.33 1.05 3.4.42 1.08-.64 1.44-1.75 1.01-3.06-.59-1.75-.08-2.69 1.75-3.02.68-.12 1.41-.11 2.09.02 1.77.35 2.3 1.33 1.78 3.08-.49 1.64.19 2.98 1.66 3.31 1.29.29 2.53-.59 2.79-1.95.27-1.56-.6-2.77-2.29-2.87zM15.89 18a2.463 2.463 0 0 1-2.36-2.48c.01-1.41 1.11-2.48 2.52-2.45 1.41.02 2.48 1.14 2.43 2.54-.04 1.37-1.21 2.44-2.59 2.39z"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <a class="addthis_button_citeulike at300b" target="_blank" title="CiteULike" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(8, 136, 200); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-citeulike-2" title="CiteULike" alt="CiteULike" class="at-icon at-icon-citeulike" style="width: 32px; height: 32px;">
                                    <title id="at-svg-citeulike-2">CiteULike</title>
                                    <g>
                                    <path d="M26.41 8.25H4.82a1.25 1.25 0 0 1 0-2.5h21.59a1.25 1.25 0 0 1 0 2.5zM26.41 14.25H4.82a1.25 1.25 0 0 1 0-2.5h21.59a1.25 1.25 0 0 1 0 2.5zM26.41 20.25H4.82a1.25 1.25 0 0 1 0-2.5h21.59a1.25 1.25 0 0 1 0 2.5zM26.41 26.25H4.82a1.25 1.25 0 0 1 0-2.5h21.59a1.25 1.25 0 0 1 0 2.5z"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <a class="addthis_button_twitter at300b" title="Twitter" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(29, 161, 242); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-3" title="Twitter" alt="Twitter" class="at-icon at-icon-twitter" style="width: 32px; height: 32px;">
                                    <title id="at-svg-twitter-3">Twitter</title>
                                    <g>
                                    <path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <a class="addthis_button_facebook addthis_button_preferred_1 at300b" title="Facebook" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(59, 89, 152); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-5" title="Facebook" alt="Facebook" class="at-icon at-icon-facebook" style="width: 32px; height: 32px;">
                                    <title id="at-svg-facebook-5">Facebook</title>
                                    <g>
                                    <path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <a class="addthis_button_print addthis_button_preferred_2 at300b" title="Print" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(115, 138, 141); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-print-6" title="Print" alt="Print" class="at-icon at-icon-print" style="width: 32px; height: 32px;">
                                    <title id="at-svg-print-6">Print</title>
                                    <g>
                                    <path d="M24.67 10.62h-2.86V7.49H10.82v3.12H7.95c-.5 0-.9.4-.9.9v7.66h3.77v1.31L15 24.66h6.81v-5.44h3.77v-7.7c-.01-.5-.41-.9-.91-.9zM11.88 8.56h8.86v2.06h-8.86V8.56zm10.98 9.18h-1.05v-2.1h-1.06v7.96H16.4c-1.58 0-.82-3.74-.82-3.74s-3.65.89-3.69-.78v-3.43h-1.06v2.06H9.77v-3.58h13.09v3.61zm.75-4.91c-.4 0-.72-.32-.72-.72s.32-.72.72-.72c.4 0 .72.32.72.72s-.32.72-.72.72zm-4.12 2.96h-6.1v1.06h6.1v-1.06zm-6.11 3.15h6.1v-1.06h-6.1v1.06z"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <a class="addthis_button_compact at300m" href="#">
                            <span class="at-icon-wrapper" style="background-color: rgb(255, 101, 80); line-height: 32px; height: 32px; width: 32px;">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-4" title="More" alt="More" class="at-icon at-icon-addthis" style="width: 32px; height: 32px;">
                                    <title id="at-svg-addthis-4">AddThis</title>
                                    <g>
                                    <path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path>
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <div class="atclear"></div>
                        </div>
                        <!-- AddThis Button END -->
                        <h4>Cite as</h4>
                        <div id="invenio-csl" class="ng-scope">
                        <invenio-csl ng-init="vm.citationResult = 'Gupta, Ankita, Achterberg, Cornelis Van, Pattar, Rohit, Navik, Omprakash, &amp; Mahendiran, G. (2022). FIGURE 4 in Two new species of the genera Leiophron Nees and Paroligoneurus Muesebeck (Hymenoptera: Braconidae) from northern India. In Zootaxa (Vol. 5175, Number 5, pp. 593–599). Zenodo. https://doi.org/10.5281/zenodo.7009616'" class="ng-scope">
                            <invenio-csl-citeproc endpoint="/api/records/7009616" template="/static/templates/invenio_csl/citeproc.html">
                                <p><span class="ng-binding">Gupta, Ankita, Achterberg, Cornelis Van, Pattar, Rohit, Navik, Omprakash, &amp; Mahendiran, G. (2022). FIGURE 4 in Two new species of the genera Leiophron Nees and Paroligoneurus Muesebeck (Hymenoptera: Braconidae) from northern India. In Zootaxa (Vol. 5175, Number 5, pp. 593–599). Zenodo. https://doi.org/10.5281/zenodo.7009616</span></p>
                            </invenio-csl-citeproc>
                            <invenio-csl-error template="/static/node_modules/invenio-csl-js/dist/templates/error.html">
                                <div ng-show="vm.error" class="ng-hide">
                                    <div class="alert alert-danger ng-binding"></div>
                                </div>
                            </invenio-csl-error>
                            <invenio-csl-loading template="/static/node_modules/invenio-csl-js/dist/templates/loading.html">
                                <div ng-show="vm.loading" class="ng-hide">Loading...</div>
                            </invenio-csl-loading>
                            <invenio-csl-typeahead lazy="true" placeholder="Start typing a citation style..." remote="/api/csl/styles" template="/static/node_modules/invenio-csl-js/dist/templates/typeahead.html" item-template="/static/templates/invenio_csl/item.html"><input type="text" class="form-control" placeholder="Start typing a citation style...">
                            </invenio-csl-typeahead>
                        </invenio-csl>
                        </div>
                    </div>
                    <div class="well">
                        <h4>Export</h4>
                        <ul class="list-inline">
                        <li><a href="/record/7009616/export/hx">BibTeX</a></li>
                        <li><a href="/record/7009616/export/csl">CSL</a></li>
                        <li><a href="/record/7009616/export/dcite4">DataCite</a></li>
                        <li><a href="/record/7009616/export/xd">Dublin Core</a></li>
                        <li><a href="/record/7009616/export/dcat">DCAT</a></li>
                        <li><a href="/record/7009616/export/json">JSON</a></li>
                        <li><a href="/record/7009616/export/schemaorg_jsonld">JSON-LD</a></li>
                        <li><a href="/record/7009616/export/geojson">GeoJSON</a></li>
                        <li><a href="/record/7009616/export/xm">MARCXML</a></li>
                        <li><a href="https://www.mendeley.com/import/?url=https://zenodo.org/record/7009616"><i class="fa fa-external-link"></i> Mendeley</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </main>

<footer>
    <!-- Footer -->
    <div class="mt-5" id="footer-placeholder"></div>
    <script>
        $(function(){
            $("#footer-placeholder").load("/html/footer.html");
        });
    </script>
    <!-- End of footer -->
</footer>

<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>