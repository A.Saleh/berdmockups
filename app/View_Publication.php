<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conference Paper</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Semantic UI -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"> -->
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>
        
        
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Since there isn't a connected database yet, we need to import the comments for the datasets*/
            <?php include "./js/comments.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Find the comments for the dataset over it's id*/ 
            var dataset_comments = [];
            for (var i=0; i < comments.length; i++) {
                if (<?php echo $_GET["id"]?> == comments[i].id ) {
                    dataset_comments.push(comments[i]);
                }
            }

            /*Depending whether the dataset is open source of a marketplace dataset, you can either download
            or apply to use the dataset*/
            var button = "Download";
            if (dataset.access == "Marketplace") {
                button = "Apply";
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column">
                    
                
                    <div class="d-flex justify-content-between ">
                        <button class="btn btn-success" type="button" onclick="history.back()">&#8592 Back to search results</button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-bookmarks"></i></button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-gear"></i> Manage record</button>
                    </div>  

                    <hr>
                    
                
                    <div class="d-flex justify-content-between">
                        <h3>${dataset.title}</h1> 
                        <div>
                           <!-- <label class="bg-primary" style="font-size: large; color: white">&nbsp${dataset.date}&nbsp</label> -->
                            <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                            <label class="bg-danger" style="font-size: large; color: white">&nbsp${dataset.access}&nbsp</label>
                        </div>  
                    </div>
                    <div class="mt-2">
                        
                        <label class="mt-1">${dataset.authors.join(", ")}</label>
                    </div>
                    <div class="mt-1">
                        Published ${dataset.published}
                    </div>
                    
                    <hr>

                    <div class="mt-3">
                        <h3>Description</h3>
                        
                        <p class="mt-1">${dataset.text}</p>
                        
                    </div>
                    
                    
                    <hr>

                    <div class="d-flex justify-content-between">
                        <h3>Citation</h3> 
                        <div>
                          <!--  <label class="mr-5" color: Black">Style</label> -->
                          <span class = "label label-default m-3">Style </span>
                            <button class="btn btn-light dropdown-toggle" type="button" >APA</button>
                        </div> 
                    </div>
                    <div class="mt-2">
                        <p>Jane Smith, & John Doe. (2022). Conference Paper X. Proceedings of the XYZ Conference - 2022, 424-428. <a href="">https://doi.org/12.3456/berd.78910</a>.</p>
                    </div>
                    

                    <!-- <div class="d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <button type="button" class="btn btn-light" onclick="checkValue()">${button}</button>
                            <label class="ps-2"><i>&copy ${dataset.copyright}</i></label>
                        </div>
                        <div>
                            <button type="button" class="btn btn-light mx-2 mt-2">&nbspSave</button>
                        </div> 
                    </div> -->

                    <hr>



                    <div class="mt-3">
                        <h3>Files</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th class="ui berd-green header">Size</th>
                                    <th class="ui berd-green header">Actions</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>${dataset.title}.pdf</th>
                                    <th>430 kB </th>
                                    <th>
                                        <button class="btn btn-success">Preview</button>
                                        <button class="btn btn-success">Download</button>
                                        
                                    </th>

                                </tr>
                                

                            </tbody>
                        </table>
                    </div>

                    <hr>

                    <div class="mt-3">
                        <h3>Details</h3>
                        <table class="table table-bordered">
                            
                            <tbody>
                                <tr>
                                    <th>Author(s):</th>
                                    <th>${dataset.authors.join(", ")}</th>

                                </tr>
                                <tr>
                                    <th>Publisher in:</th>
                                    <th>${dataset.publisher}</th>

                                </tr>
                                
                                <tr>
                                    <th>Keyword(s):</th>
                                    <th>${dataset.keywords.join(", ")}</th>

                                </tr>
                                <tr>
                                    <th>ISBN:</th>
                                    <th>${dataset.ISBN}</th>

                                </tr>
                                
                                
                                <tr>
                                    <th>DOI:</th>
                                    <th>${dataset.doi}</th>

                                </tr>
                                <tr>
                                    <th>License (for files)</th>
                                    <th>${dataset.license}</th>

                                </tr>
                               

                            </tbody>
                        </table>
                    </div>
                    
                    

                    <hr>

                    <div class="mt-3">
                        <h3>Related resources
                        </h3>
                        <div style="border:1px solid black;">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Citations</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">ML Approaches</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Tasks</button>
                                </li>
                            </ul>
                            <div class="tab-content m-3" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <b>Cites:</b>
                                    <ul>
                                        <li> Conference Paper Y (URL) </li>
                                        <li> Conference Paper Z (URL) </li>
                                    </ul>
                                    <b>Cited by:</b>
                                    <ul>
                                        <li> Conference Paper W (URL) </li>
                                        
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <ul>
                                        <li> Random Forest </li>
                                        <li> Naive Bayes </li>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <ul>
                                        <li> data curation </li>
                                        <li> data management </li>
                                    </ul>
                                </div>
                            </div>
                        </div>    
                    </div>

                    

                        

                </div>
                
                `

       

         </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>