<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edu Module</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Semantic UI -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"> -->
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>

        <script>

            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Since there isn't a connected database yet, we need to import the comments for the datasets*/
            <?php include "./js/comments.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Find the comments for the dataset over it's id*/ 
            var dataset_comments = [];
            for (var i=0; i < comments.length; i++) {
                if (<?php echo $_GET["id"]?> == comments[i].id ) {
                    dataset_comments.push(comments[i]);
                }
            }

            /*Depending whether the dataset is open source of a marketplace dataset, you can either download
            or apply to use the dataset*/
            var button = "Download";
            if (dataset.access == "Marketplace") {
                button = "Apply";
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
            <div class="container record-detail">

                    <div class="d-flex justify-content-between ">
                        <button class="btn btn-success" type="button" onclick="history.back()">&#8592 Back to search results</button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-bookmarks"></i></button>
                        <button class="btn" type="button" > </button>
                    </div>  

                    <hr>

                    <div class="d-flex justify-content-between">
                        <h3>${dataset.title}</h1> 
                        <div>
                            <label class="bg-primary" style="font-size: large; color: white">&nbspSelf-learning module&nbsp</label>
                            <label class="bg-secondary" style="font-size: large; color: white">&nbspOpen registration&nbsp</label>
                        </div>  
                    </div>
                    <div class="d-flex justify-content-between">
                        <div>
                            <div class="mt-2">
                                    <b>Author(s)</b>:
                                    <label class="mt-1">${dataset.authors.join(", ")}</label>
                            </div>
                            <div class="mt-1">
                                <p>Published ${dataset.published} | Version v3</p>
                            </div>
                        </div>
                        
                        <div>
                            <div class="d-flex justify-content-end mt-2">
                                <h5>Overall rating (72 votes):</h4>
                                <div class="m-1">
                                    <span class="fa fa-star" style="color : orange"></span>
                                    <span class="fa fa-star" style="color : orange"></span>
                                    <span class="fa fa-star" style="color : orange"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>    
                            <div class="mt-1">
                               <a href="#"> <p>Rate this module</p> </a>
                            </div>
                        </div> 
                        
                    </div>    
                        
                    
                    <hr>

                    <div class="row">
                        <div class="col-sm-8 col-md-8 col-left">
                            <h4>Short description / abstract</h4>
                            <p>${dataset.short_description}</p>

                            <hr>

                            <h4>Long description</h4>
                            <p>${dataset.long_description}</p>

                            <hr>

                            <h4>What you will learn </h4>
                                <ul>
                                    <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li>
                                    <li>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </li>
                                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
                                    <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</li>
                                </ul>

                            <h4>Prerequisites</h4>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. </p>
                            <ul>
                                <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </li>
                                <li>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </li>
                                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
                                <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</li>
                            </ul>

                            <hr>

                            <h4>Further Information & Contact </h4>

                            <b>Module home page</b>
                            <p><a href="">https://example.org/moodle/moodlexyz</a></p>
                            <b>External links</b>
                            <p><a href="">https://www.github/example/repo</a></p>
                            <button class="btn btn-success m-3">Contact</button>

                            <hr>

                            <div class="mt-3">
                                <h3>Related resources
                                </h3>
                                <div style="border:1px solid black;">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Modules</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Papers</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">ML Tasks</button>
                                        </li>
                                    </ul>
                                    <div class="tab-content m-3" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <b>Parent module</b>
                                            <ul>
                                                <li> T&E Module ABC (URL) </li>
                                            </ul>
                                            
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <b> Original Paper</b>
                                            <ul>
                                                <li> Paper X </li>
                                                
                                            </ul>
                                        </div>
                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <b>Related Tasks </b>
                                            <ul>
                                                <li> Sentiment Analysis </li>
                                            </ul>
                                        </div>
                                        
                                    </div>
                                </div>    
                            </div>

                        </div>

                        <div class="col-sm-4 col-md-4 col-right">
                            <div class="d-flex flex-column ps-2" style="border: black 1px solid; height: 400px;">
                                <h5 class="mt-4">Access</h5>
                                <hr>
                                <u><b>Type</b></u>
                                Free online access (registration required)
                                <u class="mt-2"><b>Fee</b></u>
                                None
                                <u class="mt-2"><b>Participation mode</b></u>
                                Asynchronous/online
                                <u class="mt-2"><b>External home page</b></u>
                                https://example.org/moodle/modulexyz

                                <button class="btn btn-success m-3" > <a href="Registration_module.php" style="color : white"> Register for this module </a> </button>
                                
                            </div>

                            <div class="d-flex flex-column ps-2 mt-3" style="border: black 1px solid; height: 750px;">
                                <h5 class="mt-4">Details</h5>
                                <hr>
                                <u><b>Author(s)</b></u>
                                ${dataset.authors.join(", ")}
                                <u class="mt-2"><b>Instructor(s)</b></u>
                                <p>${dataset.instructors.join(", ")}</p>
                                <u class="mt-2"><b>Module type</b></u>
                                ${dataset.module_type}
                                <u class="mt-2"><b>Duration</b></u>
                                ${dataset.duration}
                                <u class="mt-2"><b>Organization</b></u>
                                <p>${dataset.organization}</p>
                                <u class="mt-2"><b>Publication date</b></u>
                                ${dataset.published}
                                <u class="mt-2"><b>Language(s)</b></u>
                                <p>${dataset.languages.join(", ")}</p>
                                <u class="mt-2"><b>License</b></u>
                                <p>${dataset.license}</p>
                                <u class="mt-2"><b>DOI</b></u>
                                <p>${dataset.doi}</p>
                                <u class="mt-2"><b>Funded/Offered by</b></u>
                                <p>${dataset.funded_by}</p>
                                
                            </div>

                            <div class="d-flex flex-column ps-2 mt-3" style="border: black 1px solid; height: 200px;">
                                <h5 class="mt-4">Keywords and Subjects</h5>
                                <hr>
                                <div>
                                <label class="bg-secondary m-2" style="color: white">&nbspMachine Learning&nbsp</label>
                                <label class="bg-secondary m-2" style="color: white">&nbspNLP&nbsp</label>
                            
                                <label class="bg-secondary m-2" style="color: white">&nbspSentimment Analysis&nbsp</label>
                                </div>
                            </div>

                            <div class="d-flex flex-column ps-2 mt-3" style="border: black 1px solid; height: 350px;">
                                <h5 class="mt-4">Recommended Modules</h5>
                                <hr>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>T&E Module X</th>
                                            <th><label class="bg-secondary m-2" style="color: white">&nbspComm. Workshop&nbsp</label></th>
                                        </tr>
                                        <tr>
                                            <th>T&E Module Y</th>
                                            <th><label class="bg-secondary m-2" style="color: white">&nbspCourse&nbsp</label></th>
                                        </tr>
                                        <tr>
                                            <th>T&E Module Z</th>
                                            <th><label class="bg-secondary m-2" style="color: white">&nbspCourse&nbsp</label></th>

                                        </tr>
                                    </tbody>
                                </table>

                                <button class="btn btn-success m-3">Show more</button>

                            </div>


                            <div class="d-flex flex-column ps-2 mt-3" style="border: black 1px solid; height: 250px;">
                                <h5 class="mt-4">Versions</h5>
                                <hr>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Version v3</th>
                                            <th>${dataset.published}</th>
                                        </tr>
                                        <tr>
                                            <th>Version v2</th>
                                            <th>May 6, 2021</th>
                                        </tr>
                                        <tr>
                                            <th>Version v1</th>
                                            <th>Nov 18,2020</th>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            
                            <div class="d-flex flex-column ps-2 mt-3" style="border: black 1px solid; height: 300px;">
                                <h5 class="mt-4">Usage Statistics</h5>
                                <hr>
                                <table class="table">
                            
                                    <thead>
                                            <tr>
                                                <th></th>
                                                <th>All versions</th>
                                                <th>This version</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Views</th>
                                            <th>2500</th>
                                            <th>1200</th>

                                        </tr>

                                        <tr>
                                            <th>Registerations</th>
                                            <th>311</th>
                                            <th>247</th>

                                        </tr>

                                        <tr>
                                            <th>Module</th>
                                            <th>232</th>
                                            <th>199</th>

                                        </tr>
                                        

                                    </tbody>
                                </table>
                            </div>
                        
                        
                        </div>
                    </div>    
                </div>    

                        `
        </script>                
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>