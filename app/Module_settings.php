<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Module settings</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
   <div class="invenio-page-body">
      <div class="ui container fluid page-subheader-outer with-submenu rel-pt-2 ml-0-mobile mr-0-mobile">
         <div class="ui container relaxed grid page-subheader mr-0-mobile ml-0-mobile">
            <div class="row pb-0 m-3">
               <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                  <div class="ui rounded image community-image mt-5 rel-mr-2 left-floated">
                     <img src="/static/images/square-placeholder.png" alt="" onerror="this.onerror=null;this.src='/static/images/square-placeholder.png'">
                  </div>
                  <div class="flex rel-mb-1">
                     <h2 class="ui header mb-0">Training & Education Module XYZ</h2>
                  </div>
                  <div></div>
               </div>
            </div>
         </div>
         <div class="ui container secondary pointing stackable menu page-subheader pl-0 pr-0">
            <a class="item active"><i aria-hidden="true" class="settings icon"></i>Settings</a>
            
         </div>
         <!--  -->
         <div class="container d-flex">
            <div class="d-flex col-1"></div>
            <div class="d-flex flex-column col-8">
               <!-- Publisher -->
               <label class="form-check-label mt-2" for="flexCheckDefault">
               Module Name *
               </label>
               <div>
                  <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                  <input type="text" class="form-control mt-2" id="inputName">
               </div>
               <!-- creators -->
               <label class="form-check-label mt-2" for="flexCheckDefault">
               Type *
               </label>
               <div>
                  <button class="form-control btn btn-light dropdown-toggle mt-2" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                  Dataset
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Publication" onclick="updateDataFormat(this.value)">Publication</a></li>
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Dataset" onclick="updateDataFormat(this.value)">Dataset</a></li>
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Image" onclick="updateDataFormat(this.value)">Image</a></li>
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Video" onclick="updateDataFormat(this.value)">Video</a></li>
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Audio" onclick="updateDataFormat(this.value)">Audio</a></li>
                     <li><a class="dropdown-item" href="javascript:void(0)" value="Other" onclick="updateDataFormat(this.value)">Other</a></li>
                  </ul>
               </div>
               <hr>
               <!--General information form  -->
               <div>
                  <h4 class="mt-3">General information</h4>
                  <!-- <p style="font-size: 14px">(Boxes with a * are obligatory)</p>  -->
                  <div class="m-3">
                     <!-- <form style="width: 550px;"> -->
                        <!-- creators -->
                        <label class="form-check-label mt-2" for="flexCheckDefault">
                        Creators *
                        </label>
                        <div>
                           <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                           <input type="text" class="form-control mt-2" id="inputName" placeholder="John Doe">
                        </div>
                        <div>
                           <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add creator</button>
                        </div>
                        <!-- Instructors -->
                        <label class="form-check-label mt-2" for="flexCheckDefault">
                        Instructors *
                        </label>
                        <div>
                           <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                           <input type="text" class="form-control mt-2" id="inputName" placeholder="John Doe">
                        </div>
                        <div>
                           <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add creator</button>
                        </div>
                        <!-- Short description -->
                        <h4>Short description *</h4>
                        <div>
                           <input class="form-control mt-2" style="height:200px; font-size:14pt;" .....>
                        </div>
                        <h4>Long description *</h4>
                        <div>
                           <input class="form-control mt-2"style="height:200px; font-size:14pt;" .....>
                        </div>
                        <!-- Subjects -->
                        <label class="form-check-label mt-2" for="flexCheckDefault">
                        Subjects
                        </label>
                        <div>
                           <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                           <input type="text" class="form-control mt-2" id="inputName" placeholder="Chemistry, Industry">
                        </div>
                        <hr>
                        <div class="mt-3">
                           <h3>Learning material</h3>
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th>Name</th>
                                    <th class="ui berd-green header">Size</th>
                                    <!-- <th class="ui berd-green header">Actions</th> -->
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <th>slides_01.pdf</th>
                                    <th>400 kB </th>
                                    <th>
                                       <button class="btn btn-success">Remove</button>
                                    </th>
                                 </tr>
                                 <tr>
                                    <th>slides_02.pdf</th>
                                    <th>400 kB</th>
                                    <th>
                                       <button class="btn btn-success">Remove</button>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                           <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add file</button> 
                        </div>
                        <hr>

                        <!-- Access -->

                        
                        <!-- Licsense -->
                        <label class="form-check-label mt-2" for="flexCheckDefault">
                        Licenses
                        </label>
                        <div>
                           <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add license</button>
                        </div>
                  </div>
               </div>
               <hr>
               <div class="row danger-zone mt-3 mb-5">
                  <div class="sixteen wide column">
                     <div class="ui segment negative rel-mt-2">
                        <h2 class="ui header negative">Danger zone</h2>
                        <div class="ui grid">
                           <div class="twelve wide computer sixteen wide mobile ten wide tablet column">
                              <h3 class="ui small header">Delete Module</h3>
                              <p>Please make sure to inform all participants of this course.</p>
                           </div>
                           <div class="right floated four wide computer sixteen wide mobile six wide tablet column">
                              <button class="ui compact fluid icon negative left labeled button" type="button">
                              <i aria-hidden="true" class="pencil icon"></i>Delete Module </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- </form> -->
            </div>
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column ps-2 col-3 m-3 justify-content-evenly" style="border: black 1px solid; height: 250px; width: 300px; position: sticky; top: 50px;">
               <div>
                  <h4 style="text-align:center;">
                  Draft</h5>
                  <div class="mt-2 row " style="text-align:center;">
                     <div class="col-sm-6">
                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Save draft</button>
                     </div>
                     <div class="col-sm-6">
                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Preview</button>
                     </div>
                  </div>
                  <div style="text-align:center;">
                     <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Publish</button>
                  </div>
                  <div style="text-align:center;">
                     <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Assign to user ..</button>
                  </div>
                  <div style="text-align:center;">
                     <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Discard</button>
                  </div>
               </div>
            </div>
         </div>
         <!--  -->
      </div>
   </div>
</main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>