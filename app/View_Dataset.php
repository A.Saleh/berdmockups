<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dataset</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Semantic UI -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"> -->
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>
        
        
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Since there isn't a connected database yet, we need to import the comments for the datasets*/
            <?php include "./js/comments.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Find the comments for the dataset over it's id*/ 
            var dataset_comments = [];
            for (var i=0; i < comments.length; i++) {
                if (<?php echo $_GET["id"]?> == comments[i].id ) {
                    dataset_comments.push(comments[i]);
                }
            }

            /*Depending whether the dataset is open source of a marketplace dataset, you can either download
            or apply to use the dataset*/
            var button = "Download";
            if (dataset.access == "Marketplace") {
                button = "Apply";
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column">
                    
                    <div class="d-flex justify-content-between ">
                        <button class="btn btn-success" type="button" onclick="history.back()">&#8592 Back to search results</button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-bookmarks"></i></button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-gear"></i> Manage record</button>
                    </div>  

                    <hr>
                    
                
                    <div class="d-flex">
                        <h3>${dataset.title}</h1> 
                         
                    </div>
                    <div class="d-flex mt-1 justify-content-between">
                        <p>Data Source (Open data vs Exclusive Corporate Data) <br>
                        Published : ${dataset.uploaded} <br>
                        Number of current version: v2
                        </p>
                        
                        <div>
                            <a href= "Dataset_application.php"> <button class="btn btn-success btn-block" style="color: white; white-space: nowrap" >Get dataset </button> </a>
                        </div> 
                    </div>
                    
                    <div class=" d-flex justify-content-between">
                        <div>
                            <b>TAG(s)</b>:
                            <label class="bg-primary" style="font-size: large; color: white">&nbspCustomer interactions&nbsp</label>
                            <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                            <label class="bg-danger" style="font-size: large; color: white">&nbsp${dataset.access}&nbsp</label>
                        </div>    

                        <div>
                            <button class="btn btn-success btn-block" style="color: white; white-space: nowrap">Discuss dataset </button>
                        </div>    
                    </div>
                    
                    
                    
                    <hr>

                    <div class="d-flex justify-content-between">
                        <h3>Citation</h3> 
                        <div>
                          <!--  <label class="mr-5" color: Black">Style</label> -->
                          <span class = "label label-default m-3">Style </span>
                            <button class="btn btn-light dropdown-toggle" type="button" >APA</button>
                        </div> 
                    </div>
                    <div class="mt-2">
                        <p>${dataset.cite} ${dataset.doi}</p>
                    </div>
                    <hr>

                    <div class="mt-3">
                        <h3>Description</h3>
                        
                        <p class="mt-1">${dataset.text}</p>
                        
                    </div>

                    <!-- <div class="d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <button type="button" class="btn btn-light" onclick="checkValue()">${button}</button>
                            <label class="ps-2"><i>&copy ${dataset.copyright}</i></label>
                        </div>
                        <div>
                            <button type="button" class="btn btn-light mx-2 mt-2">&nbspSave</button>
                        </div> 
                    </div> -->

                    <hr>

                    <div class="mt-3">
                        <h3>Variables
                        </h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Variable 1</th>
                                    <th>Description of variable 1</th>

                                </tr>
                                <tr>
                                    <th>Variable 2</th>
                                    <th>Description of variable 2</th>

                                </tr>
                                <tr>
                                    <th>Variable 3</th>
                                    <th>Description of variable 3</th>

                                </tr>
                                

                            </tbody>
                        </table>
                    </div>
                    
                    <hr>

                    <div class="mt-3">
                        <div class="d-flex justify-content-start">
                            <h3>Quality Assessment</h3>
                            <div class="m-2">
                            <span class="fa fa-star" style="color : orange"></span>
                            <span class="fa fa-star" style="color : orange"></span>
                            <span class="fa fa-star" style="color : orange"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            </div>
                        </div>   
                        
                        <table class="table table-bordered">
                            
                            <tbody>
                                <tr>
                                    <th>Criterion 1:</th>
                                    <th>[ ... ]</th>

                                </tr>
                                <tr>
                                    <th>Criterion 2:</th>
                                    <th>[ ... ]</th>

                                </tr>
                                <tr>
                                    <th>Criterion 3:</th>
                                    <th>[ ... ]</th>

                                </tr>

                            </tbody>
                        </table>
                        
                    </div>

                    <hr>

                    <div class="mt-3">
                        <h3>Related resources
                        </h3>
                        <div style="border:1px solid black;">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="article-tab" data-bs-toggle="tab" data-bs-target="#article" type="button" role="tab" aria-controls="article" aria-selected="true">Cited By</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="relatedDatasets-tab" data-bs-toggle="tab" data-bs-target="#relatedDatasets" type="button" role="tab" aria-controls="relatedDatasets" aria-selected="false">Related Datasets</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Applicable ML Approaches</button>
                                </li>
                            </ul>
                            <div class="tab-content m-3" id="myTabContent">
                                <div class="tab-pane fade show active" id="article" role="tabpanel" aria-labelledby="article-tab">
                                    <div>
                                        <h4> Articles citing this dataset </h4>
                                        <div class="container d-flex justify-content-between">
                                            <div>
                                                <p><b>Volescie ndebit mo maximus molupit autem alitaqui ius. </b> <br>
                                                <u>Max Mustermann, Max Mustermann, Max Mustermann. </u> <br>
                                                Volescie ndebit mo maximus molupit autem alitaqui ius. Cimintibus et vendae voluptam samet entiis explit ipsus ad ese ne et
                                                eatem. Aquuntur? Qui rate simporeped quam, cus.</p>
                                                        
                                            </div>    
                                            <div>
                                                <button class="btn btn-light" style="color: green; white-space: nowrap">Find article </button>
                                            </div>  
                                             
                                        </div>
                                        
                                    </div>

                                    <div class="mt-2">
                                        
                                        <div class="container d-flex justify-content-between">
                                            <div>
                                                <p><b>Volescie ndebit mo maximus molupit autem alitaqui ius. </b> <br>
                                                <u>Max Mustermann, Max Mustermann, Max Mustermann. </u> <br>
                                                Volescie ndebit mo maximus molupit autem alitaqui ius. Cimintibus et vendae voluptam samet entiis explit ipsus ad ese ne et
                                                eatem. Aquuntur? Qui rate simporeped quam, cus.</p>
                                                        
                                            </div>    
                                            <div>
                                                <button class="btn btn-light" style="color: green; white-space: nowrap">Find article </button>
                                            </div>  
                                             
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="relatedDatasets" role="tabpanel" aria-labelledby="relatedDatasets-tab">
                                    <div class="d-flex justify-content-evenly">
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">Data set 1</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">Data set 2</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">Data set 3</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light mt-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">Data set 4</h3>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="d-flex justify-content-evenly">
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">ML Approach 1</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">ML Approach 2</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">ML Approach 3</h3>
                                            
                                        </div>
                                        <div class="d-flex flex-column align-items-center bg-light mt-3" style="width: 235px; height: 237px;">
                                            <h3 class="mt-4">ML Approach 4</h3>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>

                    <hr>

                    <div class="mt-3">
                        <h3>Obtaining this Dataset</h3>

                        <p> Volescie ndebit mo maximus molupit autem alitaqui ius. Cimintibus et vendae voluptam samet entiis explit ipsus ad ese ne et
                            eatem. Aquuntur? Qui rate simporeped quam, cus. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                            sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

                    </div>    

                    <hr>

                    <div class="mt-3">
                        <h3>Files</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th class="ui berd-green header">Size</th>
                                    <th class="ui berd-green header">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>filename1.csv</th>
                                    <th>2.5 TB </th>
                                    <th>
                                        <button class="btn btn-success">Preview</button>
                                        <button class="btn btn-success">Download</button>
                                        <button class="btn btn-success">Open file in Jupyter</button>
                                    </th>

                                </tr>
                                <tr>
                                    <th>filename2.csv</th>
                                    <th>2.5 TB </th>
                                    
                                    <th>
                                        <button class="btn btn-success">Preview</button>
                                        <button class="btn btn-success">Download</button>
                                        <button class="btn btn-success">Open file in Jupyter</button>
                                    </th>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    
                    <hr>

                    <div class="mt-3">
                        <h3>Details</h3>
                        <table class="table table-bordered">
                            
                            <tbody>
                                <tr>
                                    <th>Resource Type:</th>
                                    <th>Dataset</th>

                                </tr>
                                <tr>
                                    <th>Creator:</th>
                                    <th>Creator XYZ</th>

                                </tr>
                                <tr>
                                    <th>Size</th>
                                    <th>5 TB</th>

                                </tr>
                                <tr>
                                    <th>Format(s):</th>
                                    <th>CSV</th>

                                </tr>
                                <tr>
                                    <th>License:</th>
                                    <th>CC-BY 4.0</th>

                                </tr>
                                <tr>
                                    <th>Download:</th>
                                    <th>httmps://example.org/abc/123</th>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                    
                    <hr>

                    <div class="mt-3">
                        <h3>Additional Details
                        </h3>
                        <table class="table table-bordered">
                            
                            <tbody>
                                <tr>
                                    <th>Subjects/ Key Words:</th>
                                    <th>Text, Chemistry, Customer Interactions</th>

                                </tr>
                                <tr>
                                    <th>Companies:</th>
                                    <th>Evonik</th>

                                </tr>

                            </tbody>
                        </table>
                    </div>

                    <hr>
                    
                    <div class="mt-3">
                        <h3>Export Metadata</h3>
                        
                        <div class="d-flex justify-content-start m-3">
                            <button class="btn btn-success m-3" type="button" >Export</button>
                            <button class="btn btn-light dropdown-toggle" type="button" >Data Cite JSON</button>
                        </div> 
                    </div>

                        

                </div>
                
                `

       

         </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>