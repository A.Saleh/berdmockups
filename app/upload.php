<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex mt-4">
            <div class="d-flex flex-column col-8">
                <button class="btn btn-success m-3">Bulk upload metadata from file ... </button>
                <hr>

                <!-- Files -->
                <div>
                    <h4 class="mt-3">Files</h4>
                        <div class="m-3">
                            <div class="mt-2">
                                <input class="form-check-input access" type="checkbox" value="Metadata_only" id="flexCheckDefault" onchange="update()">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Metadata-only record
                                </label>
                            </div>    
                            <button class="btn btn-success bi bi-upload mt-2">Upload files</button>
                            <div class="mt-2">
                                <input type="text" placeholder="URL .." >
                                <button class="btn btn-success m-3 bi bi-upload">Load from URL</button>
                            </div>
                        </div>
                </div>

                    <!--General information form  -->
                <div>
                        <h4 class="mt-3">General information</h4>
                        <!-- <p style="font-size: 14px">(Boxes with a * are obligatory)</p>  -->

                    <div class="m-3">

                        <!-- <form style="width: 550px;"> -->
                            <div class="dropdown mb-3">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Resource Type
                                </label>
                                <div>
                                    <button class="btn btn-light dropdown-toggle mt-2" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dataset
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="ML Approach" onclick="updateDataFormat(this.value)">ML Approach</a></li>
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="Dataset" onclick="updateDataFormat(this.value)">Dataset</a></li>
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="Preprocessing Method" onclick="updateDataFormat(this.value)">Preprocessing Method</a></li>
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="Publication" onclick="updateDataFormat(this.value)">Publication</a></li>
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="Book" onclick="updateDataFormat(this.value)">Book</a></li>
                                        <li><a class="dropdown-item" href="javascript:void(0)" value="Task" onclick="updateDataFormat(this.value)">Task</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!-- title -->
                            
                            <label class="form-check-label" for="flexCheckDefault">
                                Title *
                            </label>

                            <div>
                                <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                                <input type="text" class="form-control mt-2" id="inputName" placeholder="Dataset X">
                            </div>    
                            
                            <div>
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add titles</button>
                            </div>

                            <!-- Publication date -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Publication date *
                            </label>

                            <div>
                                <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                                <input type="text" class="form-control mt-2" id="inputName" placeholder="2022-07-07">
                            </div>  
                            
                            <!-- Publisher -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Publisher
                            </label>

                            <div>
                                <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                                <input type="text" class="form-control mt-2" id="inputName">
                            </div>

                            <!-- creators -->
                            
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Creators *
                            </label>

                            <div>
                                <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                                <input type="text" class="form-control mt-2" id="inputName" placeholder="John Doe">
                            </div>    
                            
                            
                            <div>
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add creator</button>
                            </div>

                            <!-- Contributors -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Contributors
                            </label>

                            <div>
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add contributor</button>
                            </div>

                            <!-- Subjects -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Subjects
                            </label>

                            <div>
                                <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                                <input type="text" class="form-control mt-2" id="inputName" placeholder="Chemistry, Industry">
                            </div>

                            <!-- Description -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Description
                            </label>

                            <div>

                                <input class="form-control mt-2"style="height:200px; font-size:14pt;" .....>

                            </div>   
                            
                            <!-- Licsense -->
                            <label class="form-check-label mt-2" for="flexCheckDefault">
                                Licenses
                            </label>

                            <div>
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add license</button>
                            </div>
                        </div>    
                </div> 
                
                <!-- Dataset-specific information -->
                <div>
                    <h4>Dataset-specific information </h4>

                    <div class="m-2">

                        <label class="form-check-label mt-2" for="flexCheckDefault">Companies</label>


                        <div class="mt-2 row">
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="inputPassword" placeholder="Degussa, Evonik">
                            </div>
                            <button class="col-sm-6 col-form-label btn btn-success" type="button"><i aria-hidden="true" class="add icon"></i>Recognize companies in my dataset</button>
                        </div>

                        <label class="form-check-label mt-2" for="flexCheckDefault">Variables</label>

                        <div class="mt-2 row">
                            <div class="col-sm-4">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Variable name ...">
                            </div>
                            <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Description ...">
                            </div>
                        </div>    

                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add variable</button>

                    </div>

                </div>

                <!-- Related resources -->

                <div class="mt-3">

                    <h4>Related resources </h4>

                    <div class="m-2 row">
                        
                        <div class="col-sm-3">
                                <label class="form-check-label mt-2" for="flexCheckDefault">Relation</label>
                              
                                <div class="dropdown mb-3 mt-3">
                                    <button class="btn btn-light dropdown-toggle" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                    is continued by
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="relation">
                                        <li><a class="dropdown-item" >is described by</a></li>
                                        <li><a class="dropdown-item" >Complies</a></li>
                                        <li><a class="dropdown-item" >is cited by</a></li>
                                        <li><a class="dropdown-item" >is continued by</a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="col-sm-3">
                                <label class="form-check-label mt-2" for="flexCheckDefault">Identifier </label>
                                    
                                <input type="text" class="form-control mb-3 mt-3" id="inputPassword" placeholder="10.1234/berd/abc123 ...">                          
                        </div>
                        <div class="col-sm-3">
                                <label class="form-check-label mt-2" for="flexCheckDefault">Scheme</label>
                                    
                                <div class="dropdown mb-3 mt-3">
                                    <button class="btn btn-light dropdown-toggle" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                    DOI
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="relation">
                                        <li><a class="dropdown-item" >Bibcode</a></li>
                                        <li><a class="dropdown-item" >Handle</a></li>
                                        <li><a class="dropdown-item" >ISBN</a></li>
                                        <li><a class="dropdown-item" >PMID</a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="form-check-label mt-2" for="flexCheckDefault">Resource type</label>
                              
                            <div class="dropdown mb-3 mt-3">
                                <button class="btn btn-light dropdown-toggle" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                Dataset
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="relation">
                                    <li><a class="dropdown-item" >Dataset</a></li>
                                    <li><a class="dropdown-item" >Publication</a></li>
                                    <li><a class="dropdown-item" >Presentation</a></li>
                                    <li><a class="dropdown-item" >Software</a></li>
                                </ul>
                            </div>
                        </div>

                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Add related resource</button>
                    </div>

                </div>




                    <!-- </form> -->
            </div>
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column ps-2 col-3 m-3 justify-content-evenly" style="border: black 1px solid; height: 200px; width: 300px; position: sticky; top: 50px;">
                <div>
                    <h4 style="text-align:center;">Draft</h5>
                    <div class="mt-2 row " style="text-align:center;">
                            <div class="col-sm-6">
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Save draft</button>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Preview</button>
                            </div>
                    </div>  
                    <div style="text-align:center;">
                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Submit for review</button>
                    </div>
                    <div style="text-align:center;">
                        <button class="btn btn-success mt-2" type="button"><i aria-hidden="true" class="add icon"></i>Discard</button>
                    </div>
                </div>
                
            </div>
        </div>

        <script>

        /*Displays selected item as "title" of the dropdown button*/ 
        $(".dropdown-menu li a").click(function(){
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });

        /*Variable to hide and show marketplace related content (default true)*/
        var hide = true;

        function openAccess() {
            hide = true;
            update();
        };

        function marketplaceAccess() {
            hide = false;
            update();
        };

        function update() {
            if (!hide) {
                document.getElementById("marketplace").innerHTML = `
                    <h5 class="mt-4"><nobr>Please specify the criteria that need to be met to access the dataset*</nobr></h5>
                    <p style="font-size: 14px">(e.g. specific research question to be answered, license agreements to be signed, data protection rules to be complied with etc.)</font size>
                    <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                `
            } else {
                document.getElementById("marketplace").innerHTML = `
                    <h5 class="mt-4"><nobr>Please enter your contact information*</nobr></h5>
                    <div class="mb-3 input-group mt-3">
                        <div class="input-group-text"><i class="bi bi-person"></i></div>
                        <input type="text" class="form-control" id="inputName" placeholder="Full name">
                    </div>
                    <div class="mb-3 input-group">
                        <div class="input-group-text"><i class="bi bi-envelope"></i></div>
                        <input type="email" class="form-control" id="inputEmail" placeholder="E-Mail">
                    </div>
                `
            } 
        };

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>