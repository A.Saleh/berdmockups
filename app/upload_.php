<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
    <div id="deposit-form">
        <form class="ui form">
            <div class="ui fluid container page-subheader-outer compact ml-0-mobile mr-0-mobile">
                <div class="ui container page-subheader">
                    <div class="page-subheader-element">Select the community where you want to submit your record.</div>
                    <div class="community-header-element rel-ml-1"><button name="setting" aria-haspopup="dialog" aria-expanded="false" class="ui mini primary button community-header-button" type="button">Select a community</button></div>
                </div>
            </div>
            <div id="rdm-deposit-form" class="ui container rel-mt-1">
                <div class="ui grid mt-25">
                    <div class="eleven wide computer sixteen wide mobile sixteen wide tablet column">
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Files</div>
                        <div class="content active">
                            <div class="ui container">
                                <div class="ui grid">
                                <div class="row pt-10 pb-5">
                                    <div class="left floated middle aligned six wide computer sixteen wide mobile six wide tablet column">
                                        <div role="list" class="ui horizontal list">
                                            <div role="listitem" class="item">
                                            <div class="ui checkbox"><input class="hidden" readonly="" tabindex="0" type="checkbox" value=""><label>Metadata-only record</label></div>
                                            </div>
                                            <div role="listitem" class="item"><i aria-hidden="true" class="question circle outline icon neutral"></i></div>
                                        </div>
                                    </div>
                                    <div class="ten wide computer sixteen wide mobile ten wide tablet column storage-col">
                                        <div class="ui tiny header mr-10">Storage available</div>
                                        <div role="list" class="ui horizontal right floated list">
                                            <div role="listitem" class="item">
                                            <div class="ui label">0 out of 100 files</div>
                                            </div>
                                            <div role="listitem" class="item">
                                            <div class="ui label">0 bytes out of 10.00 GB</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-0 pb-0">
                                    <div class="sixteen wide column">
                                        <span role="button">
                                            <input multiple="" type="file" autocomplete="off" tabindex="-1" style="display: none;">
                                            <div class="ui basic very padded segment file-upload-area no-files">
                                            <div class="ui center aligned three column grid">
                                                <div class="middle aligned row">
                                                    <div class="seven wide computer sixteen wide mobile seven wide tablet column">
                                                        <div class="ui small header">Drag and drop files</div>
                                                    </div>
                                                    <div class="two wide computer sixteen wide mobile two wide tablet column mt-10 mb-10">- or -</div>
                                                    <div class="seven wide computer sixteen wide mobile seven wide tablet column"><button class="ui icon primary left labeled button" type="button"><i aria-hidden="true" class="upload icon"></i>Upload files</button></div>
                                                </div>
                                            </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div class="row file-upload-note pt-5">
                                    <div class="sixteen wide column">
                                        <div class="ui visible warning message">
                                            <p><i aria-hidden="true" class="warning sign icon"></i>File addition, removal or modification are not allowed after you have published your upload.</p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Basic information</div>
                        <div class="content active">
                            <div class="ui container">
                                <div class="required field"><label for="pids.doi" class="field-label-class invenio-field-label"><i aria-hidden="true" class="barcode icon"></i>Digital Object Identifier</label></div>
                                <div class="inline fields">
                                <div class="field">Do you already have a DOI for this upload?</div>
                                <div class="two wide field">
                                    <div class="ui checked radio checkbox"><input class="hidden" name="radioGroup" readonly="" tabindex="0" type="radio" value="unmanaged" checked=""><label>Yes</label></div>
                                </div>
                                <div class="two wide field">
                                    <div class="ui radio checkbox"><input class="hidden" name="radioGroup" readonly="" tabindex="0" type="radio" value="managed"><label>No</label></div>
                                </div>
                                </div>
                                <div class="eight wide field">
                                <div class="sixteen wide field">
                                    <div class="ui input"><input placeholder="Copy/paste your existing DOI here..." type="text" value=""></div>
                                </div>
                                </div>
                                <label class="helptext">A DOI allows your upload to be easily and unambiguously cited. Example: 10.1234/foo.bar</label>
                                <div class="required field invenio-select-field">
                                <label for="metadata.resource_type"><label for="metadata.resource_type" class="field-label-class invenio-field-label"><i aria-hidden="true" class="tag icon"></i>Resource type</label></label>
                                <div name="metadata.resource_type" field="[object Object]" labelclassname="field-label-class" required="" id="metadata.resource_type" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                    <i aria-hidden="true" class="dropdown icon"></i>
                                    <div class="menu transition">
                                        <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><i aria-hidden="true" class="table icon"></i><span class="text">Dataset</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Diagram</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Drawing</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Figure</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Other</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Photo</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Plot</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="graduation icon"></i><span class="text">Lesson</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="asterisk icon"></i><span class="text">Other</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="columns icon"></i><span class="text">Poster</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="group icon"></i><span class="text">Presentation</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Annotation collection</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Book</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Book section</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Conference paper</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Data management plan</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Journal article</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Other</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Patent</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Preprint</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Project deliverable</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Project milestone</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Proposal</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Report</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Software documentation</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Taxonomic treatment</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Technical note</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Thesis</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Working paper</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="code icon"></i><span class="text">Software</span></div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="film icon"></i><span class="text">Video/Audio</span></div>
                                    </div>
                                </div>
                                </div>
                                <div class="required field title-field">
                                <label for="metadata.title" class="field-label-class invenio-field-label"><i aria-hidden="true" class="book icon"></i>Title</label>
                                <div class="ui input"><input name="metadata.title" required="" id="metadata.title" type="text" value=""></div>
                                </div>
                                <div class="field additional-titles">
                                <label for="metadata.additional_titles" class="field-label-class invenio-field-label"></label><label class="helptext"></label>
                                <div class="fields">
                                    <div class="field align-self-end"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add titles</button></div>
                                </div>
                                </div>
                                <div class="required field invenio-text-input-field">
                                <label for="metadata.publication_date" class="field-label-class invenio-field-label"><i aria-hidden="true" class="calendar icon"></i>Publication date</label>
                                <div class="ui input"><input name="metadata.publication_date" placeholder="YYYY-MM-DD or YYYY-MM-DD/YYYY-MM-DD for intervals. MM and DD are optional." required="" id="metadata.publication_date" type="text" value="2022-08-22"></div>
                                </div>
                                <label class="helptext">In case your upload was already published elsewhere, please use the date of the first publication. Format: YYYY-MM-DD, YYYY-MM, or YYYY. For intervals use DATE/DATE, e.g. 1939/1945.</label>
                                <div class="required field">
                                <label for="metadata.creators" class="field-label-class invenio-field-label"><i aria-hidden="true" class="user icon"></i>Creators</label>
                                <div role="list" class="ui list"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add creator</button></div>
                                </div>
                                <div class="field description-field rel-mb-1" id="metadata.description">
                                <label for="metadata.description" class="field-label-class invenio-field-label"><i aria-hidden="true" class="pencil icon"></i>Description</label>
                                <div style="display: none;"></div>
                                <div class="ck ck-reset ck-editor ck-rounded-corners" role="application" dir="ltr" lang="en" aria-labelledby="ck-editor__aria-label_e14d7873ab3302faa2194fb54bd93b21e">
                                    <label class="ck ck-label ck-voice-label" id="ck-editor__aria-label_e14d7873ab3302faa2194fb54bd93b21e">Rich Text Editor</label>
                                    <div class="ck ck-editor__top ck-reset_all" role="presentation">
                                        <div class="ck ck-sticky-panel">
                                            <div class="ck ck-sticky-panel__placeholder" style="display: none;"></div>
                                            <div class="ck ck-sticky-panel__content">
                                            <div class="ck ck-toolbar ck-toolbar_grouping" role="toolbar" aria-label="Editor toolbar">
                                                <div class="ck ck-toolbar__items">
                                                    <div class="ck ck-dropdown ck-heading-dropdown">
                                                        <button class="ck ck-button ck-off ck-button_with-text ck-dropdown__button" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e98de733889dbe9e570ce7e8f53792422" aria-haspopup="true">
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Heading</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e98de733889dbe9e570ce7e8f53792422">Paragraph</span>
                                                        <svg class="ck ck-icon ck-dropdown__arrow" viewBox="0 0 10 10">
                                                            <path d="M.941 4.523a.75.75 0 1 1 1.06-1.06l3.006 3.005 3.005-3.005a.75.75 0 1 1 1.06 1.06l-3.549 3.55a.75.75 0 0 1-1.168-.136L.941 4.523z"></path>
                                                        </svg>
                                                        </button>
                                                        <div class="ck ck-reset ck-dropdown__panel ck-dropdown__panel_se">
                                                        <ul class="ck ck-reset ck-list">
                                                            <li class="ck ck-list__item"><button class="ck ck-button ck-heading_paragraph ck-on ck-button_with-text" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_ee1a33c30482372db9f1008a0151c0588"><span class="ck ck-tooltip ck-tooltip_s ck-hidden"><span class="ck ck-tooltip__text"></span></span><span class="ck ck-button__label" id="ck-editor__aria-label_ee1a33c30482372db9f1008a0151c0588">Paragraph</span></button></li>
                                                            <li class="ck ck-list__item"><button class="ck ck-button ck-heading_heading1 ck-off ck-button_with-text" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e53de5a50662d92014e2f0321a94e60e8"><span class="ck ck-tooltip ck-tooltip_s ck-hidden"><span class="ck ck-tooltip__text"></span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e53de5a50662d92014e2f0321a94e60e8">Heading 1</span></button></li>
                                                            <li class="ck ck-list__item"><button class="ck ck-button ck-heading_heading2 ck-off ck-button_with-text" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_ea95ec2710134999d92750363ad53f7d9"><span class="ck ck-tooltip ck-tooltip_s ck-hidden"><span class="ck ck-tooltip__text"></span></span><span class="ck ck-button__label" id="ck-editor__aria-label_ea95ec2710134999d92750363ad53f7d9">Heading 2</span></button></li>
                                                            <li class="ck ck-list__item"><button class="ck ck-button ck-heading_heading3 ck-off ck-button_with-text" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e6f53015bd3718cc127ef923bc5197a8c"><span class="ck ck-tooltip ck-tooltip_s ck-hidden"><span class="ck ck-tooltip__text"></span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e6f53015bd3718cc127ef923bc5197a8c">Heading 3</span></button></li>
                                                        </ul>
                                                        </div>
                                                    </div>
                                                    <span class="ck ck-toolbar__separator"></span>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e6c7575ad26c70bcdf90eed2d0a88e66e" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M10.187 17H5.773c-.637 0-1.092-.138-1.364-.415-.273-.277-.409-.718-.409-1.323V4.738c0-.617.14-1.062.419-1.332.279-.27.73-.406 1.354-.406h4.68c.69 0 1.288.041 1.793.124.506.083.96.242 1.36.478.341.197.644.447.906.75a3.262 3.262 0 0 1 .808 2.162c0 1.401-.722 2.426-2.167 3.075C15.05 10.175 16 11.315 16 13.01a3.756 3.756 0 0 1-2.296 3.504 6.1 6.1 0 0 1-1.517.377c-.571.073-1.238.11-2 .11zm-.217-6.217H7v4.087h3.069c1.977 0 2.965-.69 2.965-2.072 0-.707-.256-1.22-.768-1.537-.512-.319-1.277-.478-2.296-.478zM7 5.13v3.619h2.606c.729 0 1.292-.067 1.69-.2a1.6 1.6 0 0 0 .91-.765c.165-.267.247-.566.247-.897 0-.707-.26-1.176-.778-1.409-.519-.232-1.31-.348-2.375-.348H7z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Bold (CTRL+B)</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e6c7575ad26c70bcdf90eed2d0a88e66e">Bold</span>
                                                    </button>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_eb02cbbf7a575de3040b0dd80223b688b" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M9.586 14.633l.021.004c-.036.335.095.655.393.962.082.083.173.15.274.201h1.474a.6.6 0 1 1 0 1.2H5.304a.6.6 0 0 1 0-1.2h1.15c.474-.07.809-.182 1.005-.334.157-.122.291-.32.404-.597l2.416-9.55a1.053 1.053 0 0 0-.281-.823 1.12 1.12 0 0 0-.442-.296H8.15a.6.6 0 0 1 0-1.2h6.443a.6.6 0 1 1 0 1.2h-1.195c-.376.056-.65.155-.823.296-.215.175-.423.439-.623.79l-2.366 9.347z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Italic (CTRL+I)</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_eb02cbbf7a575de3040b0dd80223b688b">Italic</span>
                                                    </button>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e0bb1d0c3a872d4ce66132677d92a5038" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M11.077 15l.991-1.416a.75.75 0 1 1 1.229.86l-1.148 1.64a.748.748 0 0 1-.217.206 5.251 5.251 0 0 1-8.503-5.955.741.741 0 0 1 .12-.274l1.147-1.639a.75.75 0 1 1 1.228.86L4.933 10.7l.006.003a3.75 3.75 0 0 0 6.132 4.294l.006.004zm5.494-5.335a.748.748 0 0 1-.12.274l-1.147 1.639a.75.75 0 1 1-1.228-.86l.86-1.23a3.75 3.75 0 0 0-6.144-4.301l-.86 1.229a.75.75 0 0 1-1.229-.86l1.148-1.64a.748.748 0 0 1 .217-.206 5.251 5.251 0 0 1 8.503 5.955zm-4.563-2.532a.75.75 0 0 1 .184 1.045l-3.155 4.505a.75.75 0 1 1-1.229-.86l3.155-4.506a.75.75 0 0 1 1.045-.184z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Link (Ctrl+K)</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e0bb1d0c3a872d4ce66132677d92a5038">Link</span>
                                                    </button>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e2713c6fc296229c79e10bf56f96effda" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M7 5.75c0 .414.336.75.75.75h9.5a.75.75 0 1 0 0-1.5h-9.5a.75.75 0 0 0-.75.75zm-6 0C1 4.784 1.777 4 2.75 4c.966 0 1.75.777 1.75 1.75 0 .966-.777 1.75-1.75 1.75C1.784 7.5 1 6.723 1 5.75zm6 9c0 .414.336.75.75.75h9.5a.75.75 0 1 0 0-1.5h-9.5a.75.75 0 0 0-.75.75zm-6 0c0-.966.777-1.75 1.75-1.75.966 0 1.75.777 1.75 1.75 0 .966-.777 1.75-1.75 1.75-.966 0-1.75-.777-1.75-1.75z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Bulleted List</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e2713c6fc296229c79e10bf56f96effda">Bulleted List</span>
                                                    </button>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e31b4b03edfc182504446a0baafe4dc00" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M7 5.75c0 .414.336.75.75.75h9.5a.75.75 0 1 0 0-1.5h-9.5a.75.75 0 0 0-.75.75zM3.5 3v5H2V3.7H1v-1h2.5V3zM.343 17.857l2.59-3.257H2.92a.6.6 0 1 0-1.04 0H.302a2 2 0 1 1 3.995 0h-.001c-.048.405-.16.734-.333.988-.175.254-.59.692-1.244 1.312H4.3v1h-4l.043-.043zM7 14.75a.75.75 0 0 1 .75-.75h9.5a.75.75 0 1 1 0 1.5h-9.5a.75.75 0 0 1-.75-.75z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Numbered List</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e31b4b03edfc182504446a0baafe4dc00">Numbered List</span>
                                                    </button>
                                                    <span class="ck ck-toolbar__separator"></span>
                                                    <button class="ck ck-button ck-disabled ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e760c35dfe4e3fe34dcc208743735bb07" aria-disabled="true">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M2 3.75c0 .414.336.75.75.75h14.5a.75.75 0 1 0 0-1.5H2.75a.75.75 0 0 0-.75.75zm5 6c0 .414.336.75.75.75h9.5a.75.75 0 1 0 0-1.5h-9.5a.75.75 0 0 0-.75.75zM2.75 16.5h14.5a.75.75 0 1 0 0-1.5H2.75a.75.75 0 1 0 0 1.5z"></path>
                                                        <path d="M1.632 6.95L5.02 9.358a.4.4 0 0 1-.013.661l-3.39 2.207A.4.4 0 0 1 1 11.892V7.275a.4.4 0 0 1 .632-.326z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Increase indent</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e760c35dfe4e3fe34dcc208743735bb07">Increase indent</span>
                                                    </button>
                                                    <button class="ck ck-button ck-disabled ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e37c023f2a405d8af81aa0e80fd632a45" aria-disabled="true">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M2 3.75c0 .414.336.75.75.75h14.5a.75.75 0 1 0 0-1.5H2.75a.75.75 0 0 0-.75.75zm5 6c0 .414.336.75.75.75h9.5a.75.75 0 1 0 0-1.5h-9.5a.75.75 0 0 0-.75.75zM2.75 16.5h14.5a.75.75 0 1 0 0-1.5H2.75a.75.75 0 1 0 0 1.5z"></path>
                                                        <path d="M4.368 6.95L.98 9.358a.4.4 0 0 0 .013.661l3.39 2.207A.4.4 0 0 0 5 11.892V7.275a.4.4 0 0 0-.632-.326z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Decrease indent</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e37c023f2a405d8af81aa0e80fd632a45">Decrease indent</span>
                                                    </button>
                                                    <span class="ck ck-toolbar__separator"></span>
                                                    <button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e518cfbf2ebd5a8eddcde46bb6aa5f2a9" aria-pressed="false">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M3 10.423a6.5 6.5 0 0 1 6.056-6.408l.038.67C6.448 5.423 5.354 7.663 5.22 10H9c.552 0 .5.432.5.986v4.511c0 .554-.448.503-1 .503h-5c-.552 0-.5-.449-.5-1.003v-4.574zm8 0a6.5 6.5 0 0 1 6.056-6.408l.038.67c-2.646.739-3.74 2.979-3.873 5.315H17c.552 0 .5.432.5.986v4.511c0 .554-.448.503-1 .503h-5c-.552 0-.5-.449-.5-1.003v-4.574z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Block quote</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_e518cfbf2ebd5a8eddcde46bb6aa5f2a9">Block quote</span>
                                                    </button>
                                                    <button class="ck ck-button ck-disabled ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_ea2f6ff5ae73b6278ed8666bd5dcb251a" aria-disabled="true">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M5.042 9.367l2.189 1.837a.75.75 0 0 1-.965 1.149l-3.788-3.18a.747.747 0 0 1-.21-.284.75.75 0 0 1 .17-.945L6.23 4.762a.75.75 0 1 1 .964 1.15L4.863 7.866h8.917A.75.75 0 0 1 14 7.9a4 4 0 1 1-1.477 7.718l.344-1.489a2.5 2.5 0 1 0 1.094-4.73l.008-.032H5.042z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Undo (CTRL+Z)</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_ea2f6ff5ae73b6278ed8666bd5dcb251a">Undo</span>
                                                    </button>
                                                    <button class="ck ck-button ck-disabled ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_ee74bd706fc68be83c9552b791b8fedda" aria-disabled="true">
                                                        <svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                                        <path d="M14.958 9.367l-2.189 1.837a.75.75 0 0 0 .965 1.149l3.788-3.18a.747.747 0 0 0 .21-.284.75.75 0 0 0-.17-.945L13.77 4.762a.75.75 0 1 0-.964 1.15l2.331 1.955H6.22A.75.75 0 0 0 6 7.9a4 4 0 1 0 1.477 7.718l-.344-1.489A2.5 2.5 0 1 1 6.039 9.4l-.008-.032h8.927z"></path>
                                                        </svg>
                                                        <span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Redo (CTRL+Y)</span></span><span class="ck ck-button__label" id="ck-editor__aria-label_ee74bd706fc68be83c9552b791b8fedda">Redo</span>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ck ck-editor__main" role="presentation">
                                        <div class="ck-blurred ck ck-content ck-editor__editable ck-rounded-corners ck-editor__editable_inline" lang="en" dir="ltr" role="textbox" aria-label="Rich Text Editor, main" contenteditable="true">
                                            <p><br data-cke-filler="true"></p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="field additional-descriptions">
                                <label for="metadata.additional_descriptions" class="field-label-class invenio-field-label"></label><label class="helptext"></label>
                                <div class="fields">
                                    <div class="field align-self-end"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add description</button></div>
                                </div>
                                </div>
                                <div class="field">
                                <label for="metadata.rights" class="field-label-class invenio-field-label"><i aria-hidden="true" class="drivers license icon"></i>Licenses</label>
                                <div role="list" class="ui list">
                                    <div role="listitem" class="item deposit-drag-listitem">
                                        <div class="right floated content"><button class="ui mini primary button" type="button">Edit</button><button class="ui mini button" type="button">Remove</button></div>
                                        <i aria-hidden="true" class="bars icon drag-anchor" draggable="true"></i>
                                        <div class="content">
                                            <div class="header">Creative Commons Attribution 4.0 International</div>
                                            <div class="description">The Creative Commons Attribution license allows re-distribution and re-use of a licensed work on the condition that the creator is appropriately credited.</div>
                                            <span><a href="https://creativecommons.org/licenses/by/4.0/legalcode" target="_blank" rel="noopener noreferrer"><span>&nbsp;</span>Read more</a></span>
                                        </div>
                                    </div>
                                    <button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add standard</button><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add custom</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Recommended information</div>
                        <div class="content active">
                            <div class="ui container">
                                <div class="field">
                                <label for="metadata.contributors" class="field-label-class invenio-field-label"><i aria-hidden="true" class="user plus icon"></i>Contributors</label>
                                <div role="list" class="ui list"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add contributor</button></div>
                                </div>
                                <div field="[object Object]" form="[object Object]" class="fields main-group-field">
                                <div class="five wide field subjects-field">
                                    <label for="metadata.subjects" class="field-label-class invenio-field-label"><i aria-hidden="true" class="tag icon"></i>Subjects</label>
                                    <div field="[object Object]" form="[object Object]" class="fields invenio-group-field">
                                        <div class="eight wide field p-0" style="margin-bottom: auto; margin-top: auto;">Suggest from</div>
                                        <div class="eight wide field p-0">
                                            <div role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <div aria-atomic="true" aria-live="polite" role="alert" class="divider text">All</div>
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="true" aria-selected="true" class="active selected item" style="pointer-events: all;"><span class="text">All</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">FOS</span></div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="eleven wide field invenio-remote-select-field">
                                    <label for="metadata.subjects"><label class="mobile-hidden">&nbsp;</label></label>
                                    <div name="metadata.subjects" field="[object Object]" id="metadata.subjects" role="combobox" aria-busy="false" aria-expanded="false" class="ui fluid multiple search selection dropdown">
                                        <input aria-autocomplete="list" autocomplete="off" class="search" tabindex="0" type="text" value=""><span class="sizer"></span>
                                        <div aria-atomic="true" aria-live="polite" role="alert" class="divider default text">Search for a subject by name</div>
                                        <i aria-hidden="true" class="dropdown icon"></i>
                                        <div aria-multiselectable="true" role="listbox" class="menu transition"></div>
                                    </div>
                                </div>
                                </div>
                                <div class="field invenio-remote-select-field">
                                <label for="metadata.languages"><label for="metadata.languages" class="field-label-class invenio-field-label"><i aria-hidden="true" class="globe icon"></i>Languages</label></label>
                                <div name="metadata.languages" field="[object Object]" id="metadata.languages" role="combobox" aria-busy="false" aria-expanded="false" class="ui fluid multiple search selection dropdown">
                                    <input aria-autocomplete="list" autocomplete="off" class="search" tabindex="0" type="text" value=""><span class="sizer"></span>
                                    <div aria-atomic="true" aria-live="polite" role="alert" class="divider default text">Search for a language by name (e.g "eng", "fr" or "Polish")</div>
                                    <i aria-hidden="true" class="dropdown icon"></i>
                                    <div aria-multiselectable="true" role="listbox" class="menu transition"></div>
                                </div>
                                </div>
                                <div class="field">
                                <label for="metadata.dates" class="field-label-class invenio-field-label"><i aria-hidden="true" class="calendar icon"></i>Dates</label>
                                <div>
                                    <div field="[object Object]" form="[object Object]" class="fields invenio-group-field">
                                        <div class="required five wide field invenio-text-input-field">
                                            <label for="metadata.dates.0.date">Date</label>
                                            <div class="ui input"><input name="metadata.dates.0.date" placeholder="YYYY-MM-DD or YYYY-MM-DD/YYYY-MM-DD" required="" id="metadata.dates.0.date" type="text" value=""></div>
                                        </div>
                                        <div class="required five wide field invenio-select-field">
                                            <label for="metadata.dates.0.type">Type</label>
                                            <div name="metadata.dates.0.type" field="[object Object]" required="" id="metadata.dates.0.type" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><span class="text">Accepted</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Available</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Collected</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Copyrighted</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Created</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Issued</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Other</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Submitted</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Updated</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Valid</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Withdrawn</span></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="five wide field invenio-text-input-field">
                                            <label for="metadata.dates.0.description">Description</label>
                                            <div class="ui input"><input name="metadata.dates.0.description" id="metadata.dates.0.description" type="text" value=""></div>
                                        </div>
                                        <div class="field"><button aria-label="Remove field" class="ui icon button close-btn" type="button"><i aria-hidden="true" class="close icon"></i></button></div>
                                    </div>
                                </div>
                                <label class="helptext">Format: DATE or DATE/DATE where DATE is YYYY or YYYY-MM or YYYY-MM-DD.</label>
                                <div class="fields">
                                    <div class="field align-self-end"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add date</button></div>
                                </div>
                                </div>
                                <div class="field invenio-text-input-field">
                                <label for="metadata.version" class="field-label-class invenio-field-label"><i aria-hidden="true" class="code branch icon"></i>Version</label>
                                <div class="ui input"><input name="metadata.version" placeholder="" id="metadata.version" type="text" value=""></div>
                                </div>
                                <label class="helptext"><span>Mostly relevant for software and dataset uploads. A semantic version string is preferred see<a href="https://semver.org/" target="_blank" rel="noopener noreferrer"> semver.org</a>, but any version string is accepted.</span></label>
                                <div class="field invenio-text-input-field">
                                <label for="metadata.publisher" class="field-label-class invenio-field-label"><i aria-hidden="true" class="building outline icon"></i>Publisher</label>
                                <div class="ui input"><input name="metadata.publisher" placeholder="Publisher" id="metadata.publisher" type="text" value="CERN"></div>
                                </div>
                                <label class="helptext">The publisher is used to formulate the citation, so consider the prominence of the role.</label>
                            </div>
                        </div>
                    </div>
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Funding</div>
                        <div class="content active">
                            <div class="ui container">
                                <div class="field">
                                <label for="metadata.funding" class="field-label-class invenio-field-label"><i aria-hidden="true" class="money bill alternate outline icon"></i>Awards</label>
                                <div role="list" class="ui list"><button aria-expanded="false" aria-haspopup="dialog" class="ui icon left labeled button mb-5" type="button"><i aria-hidden="true" class="add icon"></i>Add award</button><button aria-expanded="false" aria-haspopup="dialog" class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add custom</button></div>
                                </div>
                                <div class="ui divider"></div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Alternate identifiers</div>
                        <div class="content active">
                            <div class="ui container">
                                <div class="field">
                                <label for="metadata.identifiers" class="field-label-class invenio-field-label"><label for="metadata.identifiers" class="field-label-class invenio-field-label"><i aria-hidden="true" class="barcode icon"></i>Alternate identifiers</label></label>
                                <div>
                                    <div field="[object Object]" form="[object Object]" class="fields invenio-group-field">
                                        <div class="required eleven wide field invenio-text-input-field">
                                            <label for="metadata.identifiers.0.identifier">Identifier</label>
                                            <div class="ui input"><input name="metadata.identifiers.0.identifier" required="" id="metadata.identifiers.0.identifier" type="text" value=""></div>
                                        </div>
                                        <div class="required five wide field invenio-select-field">
                                            <label for="metadata.identifiers.0.scheme">Scheme</label>
                                            <div name="metadata.identifiers.0.scheme" field="[object Object]" required="" id="metadata.identifiers.0.scheme" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><span class="text">ARK</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">arXiv</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Bibcode</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">DOI</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">EAN13</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">EISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Handle</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">IGSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISBN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISTC</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">LISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">LSID</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">PMID</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">PURL</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">UPC</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">URL</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">URN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">W3ID</span></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="field"><button aria-label="Remove field" class="ui icon button close-btn"><i aria-hidden="true" class="close icon"></i></button></div>
                                    </div>
                                </div>
                                <label class="helptext"></label>
                                <div class="fields">
                                    <div class="field align-self-end"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add identifier</button></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion ui inverted invenio-accordion-field ">
                        <div class="active title"><i aria-hidden="true" class="angle right icon"></i>Related works</div>
                        <div class="content active">
                            <div class="ui container">
                                <label class="helptext" style="margin-bottom: 10px;">Specify identifiers of related works. Supported identifiers include DOI, Handle, ARK, PURL, ISSN, ISBN, PubMed ID, PubMed Central ID, ADS Bibliographic Code, arXiv, Life Science Identifiers (LSID), EAN-13, ISTC, URNs, and URLs.</label>
                                <div class="field">
                                <label for="metadata.related_identifiers" class="field-label-class invenio-field-label"><label for="metadata.related_identifiers" class="field-label-class invenio-field-label"><i aria-hidden="true" class="barcode icon"></i>Related works</label></label>
                                <div>
                                    <div field="[object Object]" form="[object Object]" class="fields invenio-group-field">
                                        <div class="required three wide field invenio-select-field">
                                            <label for="metadata.related_identifiers.0.relation_type">Relation</label>
                                            <div name="metadata.related_identifiers.0.relation_type" field="[object Object]" required="" id="metadata.related_identifiers.0.relation_type" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <div aria-atomic="true" aria-live="polite" role="alert" class="divider default text">Select relation...</div>
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><span class="text">Is continued by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is described by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Continues</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Compiles</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Has version</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">HasPart</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is variant form of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is cited by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is identical to</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Reviews</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is supplement to</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is supplemented by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is original form of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is new version of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Describes</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Cites</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is part of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is previous version of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is version of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is referenced by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">References</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is documented by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is compiled by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Documents</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is source of</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is reviewed by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is derived from</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is required by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Obsoletes</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Is obsoleted by</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Requires</span></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="required four wide field invenio-text-input-field">
                                            <label for="metadata.related_identifiers.0.identifier">Identifier</label>
                                            <div class="ui input"><input name="metadata.related_identifiers.0.identifier" required="" id="metadata.related_identifiers.0.identifier" type="text" value=""></div>
                                        </div>
                                        <div class="required two wide field invenio-select-field">
                                            <label for="metadata.related_identifiers.0.scheme">Scheme</label>
                                            <div name="metadata.related_identifiers.0.scheme" field="[object Object]" required="" id="metadata.related_identifiers.0.scheme" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><span class="text">ARK</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">arXiv</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Bibcode</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">DOI</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">EAN13</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">EISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">Handle</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">IGSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISBN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">ISTC</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">LISSN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">LSID</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">PMID</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">PURL</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">UPC</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">URL</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">URN</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><span class="text">W3ID</span></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="seven wide field invenio-select-field">
                                            <label for="metadata.related_identifiers.0.resource_type"><label for="metadata.related_identifiers.0.resource_type" class="field-label-class invenio-field-label">Resource type</label></label>
                                            <div name="metadata.related_identifiers.0.resource_type" field="[object Object]" labelclassname="small field-label-class" id="metadata.related_identifiers.0.resource_type" role="listbox" aria-expanded="false" class="ui fluid selection dropdown" tabindex="0">
                                            <i aria-hidden="true" class="dropdown icon"></i>
                                            <div class="menu transition">
                                                <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;"><i aria-hidden="true" class="table icon"></i><span class="text">Dataset</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Diagram</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Drawing</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Figure</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Other</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Photo</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="chart bar outline icon"></i><span class="text">Image / Plot</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="graduation icon"></i><span class="text">Lesson</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="asterisk icon"></i><span class="text">Other</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="columns icon"></i><span class="text">Poster</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="group icon"></i><span class="text">Presentation</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Annotation collection</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Book</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Book section</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Conference paper</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Data management plan</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Journal article</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Other</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Patent</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Preprint</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Project deliverable</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Project milestone</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Proposal</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Report</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Software documentation</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Taxonomic treatment</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Technical note</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Thesis</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="file alternate icon"></i><span class="text">Publication / Working paper</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="code icon"></i><span class="text">Software</span></div>
                                                <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;"><i aria-hidden="true" class="film icon"></i><span class="text">Video/Audio</span></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="field"><button aria-label="Remove field" class="ui icon button close-btn"><i aria-hidden="true" class="close icon"></i></button></div>
                                    </div>
                                </div>
                                <label class="helptext"></label>
                                <div class="fields">
                                    <div class="field align-self-end"><button class="ui icon left labeled button" type="button"><i aria-hidden="true" class="add icon"></i>Add related work</button></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="five wide computer sixteen wide mobile sixteen wide tablet column deposit-sidebar">
                    <div class="">
                        <div></div>
                        <div class="ui sticky">
                            <div class="ui card">
                                <div class="content">
                                <div class="ui middle aligned grid">
                                    <div class="centered row pt-5 pb-5 neutral">
                                        <div class="center aligned sixteen wide column"><span>Draft</span><i aria-hidden="true" class="info circle icon ml-10"></i></div>
                                    </div>
                                </div>
                                </div>
                                <div class="content">
                                <div class="ui relaxed grid">
                                    <div class="eight wide computer sixteen wide mobile column pb-0 left-btn-col"><button name="save" class="ui fluid icon left labeled button"><i aria-hidden="true" class="save icon"></i>Save draft</button></div>
                                    <div class="eight wide computer sixteen wide mobile column pb-0 right-btn-col"><button name="preview" class="ui fluid icon left labeled button"><i aria-hidden="true" class="eye icon"></i>Preview</button></div>
                                    <div class="sixteen wide column pt-10"><button name="publish" class="ui fluid icon positive disabled left labeled button" disabled="" tabindex="-1"><i aria-hidden="true" class="upload icon"></i>Publish</button></div>
                                </div>
                                </div>
                            </div>
                            <div class="ui card access-right">
                                <div class="required field">
                                <div class="content">
                                    <div class="header"><label for="access" class="field-label-class invenio-field-label"><i aria-hidden="true" class="shield icon"></i>Visibility</label></div>
                                </div>
                                <div class="content">
                                    Full record
                                    <div class="ui two buttons"><button data-testid="protection-buttons-component-public" class="ui active button positive">Public</button><button data-testid="protection-buttons-component-restricted" class="ui button">Restricted</button></div>
                                    <div class="ui hidden divider"></div>
                                    <div data-testid="access-files">
                                        Files only
                                        <div class="ui two buttons"><button data-testid="protection-buttons-component-public" class="ui active button positive">Public</button><button data-testid="protection-buttons-component-restricted" class="ui button">Restricted</button></div>
                                    </div>
                                    <div class="ui hidden divider"></div>
                                    <div data-testid="access-message" class="ui icon positive visible message">
                                        <i aria-hidden="true" class="lock open icon"></i>
                                        <div class="content">
                                            <div class="header">Public</div>
                                            The record and files are publicly accessible.
                                        </div>
                                    </div>
                                    <div class="ui hidden divider"></div>
                                </div>
                                <div class="content">
                                    <div class="ui tiny header header">Options</div>
                                </div>
                                <div class="extra content">
                                    <div role="list" class="ui list">
                                        <div role="listitem" class="disabled item" data-testid="option-list-embargo">
                                            <i aria-hidden="true" class="icon">
                                            <div data-testid="embargo-checkbox-component" class="ui disabled fitted checkbox"><input class="hidden" disabled="" id="access.embargo.active" readonly="" tabindex="-1" type="checkbox" value=""><label for="access.embargo.active"></label></div>
                                            </i>
                                            <div class="content">
                                            <div class="header"><label for="access.embargo.active">Apply an embargo <i aria-hidden="true" class="clock outline icon"></i></label></div>
                                            <div class="description">Record or files protection must be <b>restricted</b> to apply an embargo.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </form>
        </div>


            <script>

            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            /*Variable to hide and show marketplace related content (default true)*/
            var hide = true;

            function openAccess() {
                hide = true;
                update();
            };

            function marketplaceAccess() {
                hide = false;
                update();
            };

            function update() {
                if (!hide) {
                    document.getElementById("marketplace").innerHTML = `
                        <h5 class="mt-4"><nobr>Please specify the criteria that need to be met to access the dataset*</nobr></h5>
                        <p style="font-size: 14px">(e.g. specific research question to be answered, license agreements to be signed, data protection rules to be complied with etc.)</font size>
                        <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                    `
                } else {
                    document.getElementById("marketplace").innerHTML = `
                        <h5 class="mt-4"><nobr>Please enter your contact information*</nobr></h5>
                        <div class="mb-3 input-group mt-3">
                            <div class="input-group-text"><i class="bi bi-person"></i></div>
                            <input type="text" class="form-control" id="inputName" placeholder="Full name">
                        </div>
                        <div class="mb-3 input-group">
                            <div class="input-group-text"><i class="bi bi-envelope"></i></div>
                            <input type="email" class="form-control" id="inputEmail" placeholder="E-Mail">
                        </div>
                    `
                } 
            };

            </script>
    </main>

        <footer>
            <!-- Footer -->
            <div class="mt-5" id="footer-placeholder"></div>
            <script>
                $(function(){
                    $("#footer-placeholder").load("/html/footer.html");
                });
            </script>
            <!-- End of footer -->
        </footer>
        
        <!-- Bootstrap JS -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        
    </body>
    </html>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>