<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pre-Processing Method</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Semantic UI -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"> -->
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>
        <!-- <div class="container d-flex flex-column mt-4">
            <div class="d-flex flex-column col-8">
                <div>
                    <h3>Comments</h3>
                    <hr>
                </div>
                <div class="d-flex flex-column" id="comments" style="overflow-y: auto; max-height: 500px;"></div>
                <div class="d-flex flex-column mt-2">
                    <label for="exampleFormControlTextarea1" class="form-label"><h4>Add a comment</h4></label>
                    <div class="d-flex align-items-center">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                        <div>
                            <button class="btn btn-light ms-2" type="submit">Submit</button>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn-light mt-2" type="submit">Add Image</button> 
                    </div>
                </div>
            </div>
        </div> -->
        
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Since there isn't a connected database yet, we need to import the comments for the datasets*/
            <?php include "./js/comments.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Find the comments for the dataset over it's id*/ 
            var dataset_comments = [];
            for (var i=0; i < comments.length; i++) {
                if (<?php echo $_GET["id"]?> == comments[i].id ) {
                    dataset_comments.push(comments[i]);
                }
            }

            /*Depending whether the dataset is open source of a marketplace dataset, you can either download
            or apply to use the dataset*/
            var button = "Download";
            if (dataset.access == "Marketplace") {
                button = "Apply";
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column">
                    
                    <div class="d-flex justify-content-between ">
                        <button class="btn btn-success" type="button" onclick="history.back()">&#8592 Back to search results</button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-bookmarks"></i></button>
                        <button class="btn btn-success" type="button" ><i class="bi bi-gear"></i> Manage record</button>
                    </div>  

                    <hr>
                    
                
                    <div class="d-flex justify-content-between">
                        <h3>${dataset.title}</h1> 
                        <div>
                           <!-- <label class="bg-primary" style="font-size: large; color: white">&nbsp${dataset.date}&nbsp</label> -->
                            <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                            <label class="bg-danger" style="font-size: large; color: white">&nbsp Paradigm &nbsp</label>
                        </div>  
                    </div>
                    <div class="mt-1">
                        Published ${dataset.date_created}
                    </div>

                    <div class="mt-2">
                        
                        <label class="mt-1">Version: ${dataset.version} </label>
                    </div>
                    
                    <hr>


                    <div class="d-flex justify-content-between">
                        <h3>Citation</h3> 
                        <div>
                          <!--  <label class="mr-5" color: Black">Style</label> -->
                          <span class = "label label-default m-3">Style </span>
                            <button class="btn btn-light dropdown-toggle" type="button" >APA</button>
                        </div> 
                    </div>
                    <div class="mt-2">
                        <p>${dataset.creator} (${dataset.year}). ${dataset.title}. ${dataset.publisher}. </p>
                    </div>

                    <hr>

                    <div class="mt-3">
                        <h3>Description</h3>
                        
                        <p class="mt-1">${dataset.text}</p>
                        
                    </div>
                    

                   

                    <hr>

                    

                    <div class="mt-3">
                        <h3>Details</h3>
                        <table class="table table-bordered">
                            
                            <tbody>
                                <tr>
                                    <th>Resource Type:</th>
                                    <th>${dataset.type}</th>

                                </tr>

                                <tr>
                                    <th>Creator:</th>
                                    <th>${dataset.creator}</th>

                                </tr>
                                
                                <tr>
                                    <th>Published by:</th>
                                    <th>${dataset.publisher}</th>

                                </tr>

                                <tr>
                                    <th>License (database):</th>
                                    <th>${dataset.license}</th>

                                </tr>

                                <tr>
                                    <th>Repository:</th>
                                    <th>${dataset.repository}</th>

                                </tr>
                            
                                <tr>
                                    <th>Paradigm:</th>
                                    <th>${dataset.paradigm}</th>

                                </tr>

                                <tr>
                                    <th>Data:</th>
                                    <th>${dataset.data}</th>

                                </tr>

                                <tr>
                                    <th>Categories / Keywords:</th>
                                    <th>${dataset.catgeories.join(", ")}</th>

                                </tr>
                                
                                <tr>
                                    <th>DOI:</th>
                                    <th><a href="https:10.1093/abcde/fgh123">${dataset.doi}</a></th>

                                </tr>

                                <tr>
                                    <th>Dates:</th>
                                    <th>Created: ${dataset.date_created} <br> Modified: ${dataset.date_modified}</th>

                                </tr>
                                

                            </tbody>
                        </table>
                    </div>
                    
                    

                    <hr>

                    <div class="mt-3">
                        <h3>Related resources
                        </h3>
                        <div style="border:1px solid black;">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Described in</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Used by</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Tasks</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#required" type="button" role="tab" aria-controls="required" aria-selected="false">Pre-processing methods</button>
                                </li>
                            </ul>
                            <div class="tab-content m-3" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    
                                    <ul>
                                        <li> Paper X (URL) </li>
                                        
                                    </ul>
                                    
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <ul>
                                        <li> University of Rostock research team </li>
                                        
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <ul>
                                        <li> Natural language processing </li>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="required" role="tabpanel" aria-labelledby="contact-tab">
                                    <ul>
                                        <li> data curation </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>    
                    </div>
                    
                    <hr>

                    <div class="mt-3">
                        <h3>Export Metadata</h3>
                        
                        <div class="d-flex justify-content-start m-3">
                            <button class="btn btn-success m-3" type="button" >Export</button>
                            <button class="btn btn-light dropdown-toggle" type="button" >Data Cite JSON</button>
                        </div> 
                    </div>

                        

                </div>
                
                `

        //     /*Checks how many comments there are for the dataset. If 0, it will show a default message*/
        //     var count = 0;
            
        //     /*Fill the comment segment*/
        //     document.getElementById("comments").innerHTML = `
        //         ${dataset_comments.map(function(data) {
        //             count++;
        //             return `
        //             <div class="d-flex align-items-center mt-2">
        //                 <b>${data.author}</b>
        //                 <font size="2"><i>&nbsp commented on ${data.date}</i></font size>
        //             </div>
        //             <p class="mt-2">${data.text}</p>
        //             <div class="d-flex mb-4">
        //                 <a href="#"><i class="bi bi-hand-thumbs-up"></i></a>
        //                 <font size="3">&nbsp${data.likes}</font size>
        //             </div>
        //             `
        //         }).join("")}
        //     `

        //     if (count==0) {
        //         document.getElementById("comments").innerHTML = `<h4 class="mt-2 mb-5">No comments added yet.</h4>`
        //     }

        //     function checkValue() {
        //         if (button == "Apply") {
        //             document.location.href = `/apply_for_dataset.php?id=${dataset.id}`
        //         }
        //     }

         </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>