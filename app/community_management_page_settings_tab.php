<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Community management page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">

</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="index.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="dashboard.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main> 
    
        <div class="invenio-page-body">
            <div class="ui container fluid page-subheader-outer with-submenu rel-pt-2 ml-0-mobile mr-0-mobile">
                <div class="ui container relaxed grid page-subheader mr-0-mobile ml-0-mobile">
                    <div class="row pb-0">
                        <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                            <div class="ui rounded image community-image mt-5 rel-mr-2 left-floated">
                            <img src="/static/images/square-placeholder.png" alt="" onerror="this.onerror=null;this.src='/static/images/square-placeholder.png'">
                            </div>
                            <div class="flex rel-mb-1">
                            <h2 class="ui header mb-0">Community X</h2>
                            </div>
                            <div></div>
                        </div>
                        <div class="sixteen wide mobile sixteen wide tablet three wide computer right aligned middle aligned column">
                            <a onclick="document.location.href = `/upload.php`" class="ui positive button labeled icon rel-mt-1">
                            <i class="plus icon" aria-hidden="true"></i> New upload </a>
                        </div>
                    
                    </div>
                    <!--TODO: Delete this menu to avoid using invenio_app_rdm-->
                    
                </div>
                <!--  -->
                
                <div class="ui container secondary pointing stackable menu page-subheader pl-0 pr-0">

                    <a class="item" onclick="javascript:location.href='community_management_page_home_tab.php'" ><i aria-hidden="true" class="settings icon"></i>Home</a>
                
                    <a class="item active "><i aria-hidden="true" class="settings icon" href=""></i>Settings</a>

                    <a class="item" onclick="javascript:location.href='community_management_page_search_tab.php'" ><i aria-hidden="true" class="settings icon"></i>Search</a>

                    <a class="item" onclick="javascript:location.href='community_management_page_members_tab.php'"><i aria-hidden="true" class="settings icon"></i>Members</a>
                
                </div>

            

                <!--  -->
                <div class="ui container grid communities-settings rel-mt-2">
                <!-- <div class="sixteen wide mobile sixteen wide tablet three wide computer column">
                    <div class="ui vertical computer horizontal tablet horizontal mobile menu">
                        <a href="/communities/testthis/settings" class="brand item active  item">Profile</a>
                        <a href="/communities/testthis/settings/privileges" class="  item">Privileges</a>
                    </div>
                </div> -->
                <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                    <div id="app" data-community="{&quot;access&quot;: {&quot;member_policy&quot;: &quot;open&quot;, &quot;record_policy&quot;: &quot;open&quot;, &quot;visibility&quot;: &quot;public&quot;}, &quot;created&quot;: &quot;2022-07-27T22:20:15.986246+00:00&quot;, &quot;id&quot;: &quot;62298ca9-6946-4ab9-a5f5-edfdd1e02bce&quot;, &quot;links&quot;: {&quot;invitations&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/invitations&quot;, &quot;logo&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/logo&quot;, &quot;members&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/members&quot;, &quot;public_members&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/members/public&quot;, &quot;rename&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/rename&quot;, &quot;requests&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce/requests&quot;, &quot;self&quot;: &quot;https://inveniordm.web.cern.ch/api/communities/62298ca9-6946-4ab9-a5f5-edfdd1e02bce&quot;, &quot;self_html&quot;: &quot;https://inveniordm.web.cern.ch/communities/testthis&quot;, &quot;settings_html&quot;: &quot;https://inveniordm.web.cern.ch/communities/testthis/settings&quot;}, &quot;metadata&quot;: {&quot;title&quot;: &quot;test this&quot;}, &quot;revision_id&quot;: 2, &quot;slug&quot;: &quot;testthis&quot;, &quot;ui&quot;: {}, &quot;updated&quot;: &quot;2022-07-27T22:20:16.812245+00:00&quot;}" data-has-logo="false" data-types="[{&quot;id&quot;: &quot;organization&quot;, &quot;title_l10n&quot;: &quot;Organization&quot;}, {&quot;id&quot;: &quot;event&quot;, &quot;title_l10n&quot;: &quot;Event&quot;}, {&quot;id&quot;: &quot;project&quot;, &quot;title_l10n&quot;: &quot;Project&quot;}, {&quot;id&quot;: &quot;topic&quot;, &quot;title_l10n&quot;: &quot;Topic&quot;}]" data-logo-max-size="1000000">
                    <form class="ui form communities-profile">
                            <div class="ui hidden negative message flashed">
                                <div class="ui container grid">
                                    <div class="left aligned fifteen wide column">
                                    <strong></strong>
                                    </div>
                                </div>
                            </div>
                        <div class="ui grid">
                        <div class="row pt-10 pb-0">
                            <div class="nine wide computer sixteen wide mobile nine wide tablet column rel-pb-2">
                                <div class="field invenio-text-input-field">
                                    <label for="metadata.title" class="field-label-class invenio-field-label">
                                    <i aria-hidden="true" class="book icon"></i>Community name </label>
                                        <div class="ui fluid input">
                                            <input name="metadata.title" id="metadata.title" type="text" value="Community X">
                                        </div>
                                </div>
                            <div class="field invenio-select-field">
                                <label for="metadata.type.id">
                                <label for="metadata.type.id" class="field-label-class invenio-field-label">
                                    <i aria-hidden="true" class="tag icon"></i>Type </label>
                                </label>
                                <div name="metadata.type.id" field="[object Object]" id="metadata.type.id" role="combobox" aria-expanded="false" class="ui fluid search selection dropdown">
                                    <input aria-autocomplete="list" autocomplete="off" class="search" tabindex="0" type="text" value="">
                                    <i aria-hidden="true" class="dropdown icon"></i>
                                    <div role="listbox" class="menu transition">
                                        <div role="option" aria-checked="false" aria-selected="true" class="selected item" style="pointer-events: all;">
                                            <span class="text">Organization</span>
                                        </div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;">
                                            <span class="text">Event</span>
                                        </div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;">
                                            <span class="text">Project</span>
                                        </div>
                                        <div role="option" aria-checked="false" aria-selected="false" class="item" style="pointer-events: all;">
                                            <span class="text">Topic</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="field invenio-text-input-field">
                                <label for="metadata.description" class="field-label-class invenio-field-label">
                                <i aria-hidden="true" class="pencil icon"></i>Description </label>
                                <div class="ui input">
                                    <input name="metadata.description" id="metadata.description" type="text" value="">
                                </div>
                            </div>
                            
                            <div class="field">
                                <div class="row pt-10 pb-0">
                                    <div class="eight wide computer sixteen wide mobile twelve wide tablet column">
                                        <h2 class="ui tiny header">Community visibility</h2>
                                        <div class="field invenio-radio-field">
                                            <div class="ui checked radio checkbox">
                                                <input class="hidden" name="access.visibility" readonly="" tabindex="0" type="radio" value="public" checked="">
                                                <label for="access.visibility" class="field-label-class invenio-field-label">
                                                    <i aria-hidden="true" class="group icon"></i>Public </label>
                                            </div>
                                        </div>
                                    
                                        <div class="field invenio-radio-field">
                                            <div class="ui radio checkbox">
                                                <input class="hidden" name="access.visibility" readonly="" tabindex="0" type="radio" value="public">
                                                <label for="access.visibility" class="field-label-class invenio-field-label">
                                                    <i aria-hidden="true" class="lock icon"></i>Restricted </label>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>

                            <div class="field">
                                <div class="row pt-10 pb-0">
                                    <div class="eight wide computer sixteen wide mobile twelve wide tablet column">
                                        <h2 class="ui tiny header">Community Subsections</h2>
                                        
                                        <div class="field invenio-radio-field">
                        
                                                <label for="access.visibility" class="field-label-class invenio-field-label">
                                                    <i aria-hidden="true" class="dropdown icon"></i>Subsections 1 </label>
                                            
                                        </div>
                                    
                                        <div class="field invenio-radio-field">
                                            
                                                <label for="access.visibility" class="field-label-class invenio-field-label">
                                                    <i aria-hidden="true" class="dropdown icon"></i>Subsections 2 </label>
                                        
                                        </div>

                                        <button aria-expanded="false" aria-haspopup="dialog" class="ui icon left labeled button mb-5" type="button">
                                        <i aria-hidden="true" class="add icon"></i>Add subsection </button>


                                    
                                    </div>
                                </div>
                            </div>

                            
                            <button class="ui icon primary left labeled button" type="submit">
                                <i aria-hidden="true" class="save icon"></i>Save </button>
                            </div>
                            <div class="right floated four wide computer sixteen wide mobile six wide tablet column">
                            <span role="button">
                                <input accept=".jpeg,.jpg,.png" type="file" autocomplete="off" tabindex="-1" style="display: none;">
                                <div class="ui header mt-0">Profile picture</div>
                                    <div class="ui fluid rounded image community-logo settings fallback_image">
                                        <img alt="No image found" src="/static/images/square-placeholder.png">
                                    </div>
                                <div class="ui hidden divider"></div>
                            </span>
                            <button class="ui fluid icon left labeled button rel-mt-1 rel-mb-1" type="button">
                                <i aria-hidden="true" class="upload icon"></i>Upload new picture </button>
                            <label class="helptext">File must be smaller than 1.00 MB</label>
                            </div>
                        </div>
                        <div class="row danger-zone mb-5">
                            <div class="sixteen wide column">
                            <div class="ui segment negative rel-mt-2">
                                <h2 class="ui header negative">Danger zone</h2>
                                <div class="ui grid">
                                <div class="twelve wide computer sixteen wide mobile ten wide tablet column">
                                    <h3 class="ui small header">Change identifier</h3>
                                    <p>Changing your community's unique identifier can have unintended side effects.</p>
                                </div>
                                <div class="right floated four wide computer sixteen wide mobile six wide tablet column">
                                    <button class="ui compact fluid icon negative left labeled button" type="button">
                                    <i aria-hidden="true" class="pencil icon"></i>Change identifier </button>
                                </div>
                                <div class="left floated twelve wide computer sixteen wide mobile ten wide tablet column">
                                    <h3 class="ui small header">Delete community</h3>
                                    <p>Once deleted, it will be gone forever. Please be certain.</p>
                                </div>
                                <div class="right floated four wide computer sixteen wide mobile six wide tablet column">
                                    <button aria-haspopup="dialog" aria-controls="warning-modal" aria-expanded="false" id="delete-button" class="ui compact fluid icon negative left labeled button" type="button">
                                    <i aria-hidden="true" class="trash icon"></i>Delete community </button>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>

</html>