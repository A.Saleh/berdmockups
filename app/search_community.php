<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search community</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>


</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container full-height-container d-flex flex-column">

            <!-- <div class="d-flex mt-5">
                <div class="col-3">    
                </div>
                <div class="d-flex justify-content-between col-9">
                    <button class="btn btn-light" type="button" onclick="document.location.href = `/search.php?search=+`">Save search results</button>
                    <button class="btn btn-light" type="button" onclick="document.location.href = `/search.php?search=+`">Notify me about new resources</button>
                    <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"> Weekly</button>
                </div>    
            </div> -->

            <div class="d-flex mt-5">
                
                <div class="col-3">
                    
                </div>

                <div class="d-flex justify-content-between col-9">
                    
                    <div id="results-header"></div>
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Sort by
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#" onclick="sortByNewest()">Newest</a></li>
                            <li><a class="dropdown-item" href="#" onclick="sortByOldest()">Oldest</a></li>
                            <li><a class="dropdown-item" href="#">Best Match</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="d-flex">
                <div class="d-flex flex-column col-3 mt-2">
                    <h3>Filter</h3>
                    <div class="d-flex flex-column mt-4">
                        <h5>Visibility</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input access" type="checkbox" value="Metadata_only" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Public
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input access" type="checkbox" value="Open" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Restricted
                            </label>
                        </div>
                    </div>
                    
                    <div class="d-flex flex-column mt-4">
                        <h5>Types</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Dataset" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Organization
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="ML_approach" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Event
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Preprocessing_method" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Topic
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Working_paper" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Project
                            </label>
                        </div>
                    </div>

                </div>
                <!-- <div class="flex-column mt-3" id="results" style="overflow-y: auto; height: 1000px;">  -->
                <div class="flex-column mt-3" id="results" style="overflow-y: auto;"> 
                </div>
            </div>
        </div>

    <!-- JS -->
    <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>
            //communities 
            <?php include "./js/communities.js"; ?>
            
            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            update();

            /*Display datasets*/
            function update() {

                /*Applying the filters*/
                var access = document.querySelectorAll('.access');
                var resourceType = document.querySelectorAll('.resource-type');
                var dataType = document.querySelectorAll('.data-type');
                var accessValues = [];
                var dtValues = [];
                var rtValues = [];
                var newDatasets = [];

                for (var a of access) {
                    if (a.checked) {
                        accessValues.push(a.value);
                    }
                }

                for (var rt of resourceType) {
                    if (rt.checked) {
                        rtValues.push(rt.value);
                    }
                }

                for (var dt of dataType) {
                    if (dt.checked) {
                        dtValues.push(dt.value);
                    }
                }

                if (accessValues.length == 0 && rtValues.length == 0 && dtValues.length == 0) {
                    newDatasets = communities.slice();
                } else {
                    for (var i = 0; i < communities.length; i++) {
                        if (isIncluded(communities[i].access, accessValues) && isIncluded(communities[i].type, rtValues) && isIncluded(communities[i].data, dtValues)) {
                            newDatasets.push(communities[i]);
                        }      
                    }
                }  
                /*Variable needed to display the number of found results*/
                var count = 0;

                /*Fill the results segment*/
                document.getElementById("results").innerHTML = `
                    ${newDatasets.map(function(data) {
                        
                        /*Check if the search term is included within the authors array*/
                        var found = true;
                        // for(var i=0; i<data.authors.length; i++) {
                        //     if (data.authors[i].match(/<?php echo $_GET["search"]?>/i)) {
                        //         found = true;
                        //     }
                        // }

                        /* Check if the search term is included within the title or text*/
                        if (data.name.match(/<?php echo $_GET["search"]?>/i) || data.description.match(/<?php echo $_GET["search"] ?>/i) || found) {
                            count++;
                            return `
                            <div class="d-flex flex-column mt-4" id="results" style="overflow: auto; ">
                                <div>
                                    <label class="bg-primary" style="color: white">&nbsp${data.type}&nbsp</label>
                                    
                                </div>
                                <div class="d-inline-flex">
                                    
                                    <a href="community_management_page_home_tab.php" class="mt-3" style="text-decoration: none;">
                                
                                        <font size="5">${data.name}</font size>
                                    </a>
                                </div>
                              
                                <div class="d-flex pe-3 mt-1">
                                    <p class="mt-2 text-line-limit">${data.description}</p>
                                </div>
                                
                            </div>
                            `
                        } 
                    }).join("")}
                `
                if (count==0) {
                    document.getElementById("results").innerHTML = `
                        <h3 class="mt-4">No datasets were found ... :(</h3>
                        <div class="d-inline-flex flex-column mt-3">
                            <h4>Your options:</h4>
                            <a href="#" class="mt-3" style="font-size: 20px">Notify me, when a suitable dataset is avaliable</a>
                            <a href="request.php" class="mt-3" style="font-size: 20px">Submit Dataset Request for your research project</a>
                            <a href="upload.php" class="mt-3" style="font-size: 20px">Upload your own dataset for the given search</a>
                        </div>
                    `
                }

                /*Display the number of found results for the search term*/
                document.getElementById("results-header").innerHTML = `<h5>${count} results found for "<?php echo $_GET["search"] ?>"</h5>`
            }
                
            /*Function to sort the dataset array by the newest date*/
            function sortByNewest() {
                datasets.sort(function(a,b) {
                    return new Date(b.date) - new Date(a.date);
                });
                update();
            };

            /*Function to sort the dataset array by the oldest date*/
            function sortByOldest() {
                datasets.sort(function(a,b) {
                    return new Date(a.date) - new Date(b.date);
                });
                update();
            };

            function isIncluded(val, array) {
                if (array.includes(val) || array.length == 0) {
                    return true;
                } else {
                    return false;
                }
            }

        </script>

    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>

