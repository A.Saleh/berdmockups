var datasets_data_portal = [
    
    {
        type : "Open",
        type_1: "Dataset",
        id: 1,
        name : "Dataset X",
        publisher: "Test publisher",
        num_of_citations : "31",
        description : "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."
    },
    {
        type : "Marketplace",
        type_1: "Dataset",
        id: 1,
        name : "Dataset Y",
        publisher: "Test publisher",
        num_of_citations : "31",
        description : "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."
    },
    {
        type : "Open",
        type_1: "Dataset",
        id: 1,
        name : "Dataset Z",
        publisher: "Test publisher",
        num_of_citations : "31",
        description : "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."
    },
    
];