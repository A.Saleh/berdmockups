var comments = [
    {
        id : "1" ,
        date : "2022-06-06" ,
        author : "Albert Einstein" ,
        text : "This dataset is revolutionary to science. Couldn't have done it better myself." ,
        likes : "5"
    },
    {
        id : "1" ,
        date : "2022-06-03" ,
        author : "Charlie Chaplin" ,
        text : "I'm speechless!" ,
        likes : "3"
    },
    {
        id : "2" ,
        date : "2022-05-30" ,
        author : "Bugs Bunny" ,
        text : "Nice work!" ,
        likes : "2"  
    }
];