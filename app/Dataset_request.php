<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dataset Request</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        
        <div class="ui container fluid">
            <div class="row pb-0 mt-4">
                <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                    <div class="flex rel-mb-1">
                        <h3 class="ui header mb-0">Make a dataset request</h2>
                    </div>
                </div>
            </div>
        </div>    
        <!-- Application form -->
        <div class="container d-flex">
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column col-11">

               <!-- Question 1 -->
               <label class="form-check-label mt-2" for="flexCheckDefault">
               1- Please provide a paragraph summary describing what dataset you are looking for.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 2 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
               2- Please describe the research project that you would like to conduct and how the request type of dataset would contribute to your research.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 3 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
               3- Why do you consider existing datasets on the BERD platform not sufficient to conduct your research project?
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                
                <h3 class="mt-4" >Contact details </h3>
                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault" style="width: 150px"><b> Name: </b></label>
                    <input class="ms-3" type="text" placeholder="Type something here .."  >
                </div>

                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault" style="width: 150px"><b> Affiliation: </b></label>
                    <input class="ms-3" type="text" placeholder="Type something here .." >
                </div>

                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault" style="width: 150px"><b> Email address: </b></label>
                    <input class="ms-3" type="text" placeholder="Type something here .." >
                </div>

               <div class="d-flex justify-content-between mt-4 ">
                  <button class="btn btn-success">Previous</button>
                  <button class="btn btn-success">Save progress</button>
                  <button class="btn btn-success">Submit</button>
               </div>
            </div>

            
            
         </div>
         <!--  -->
        


    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>