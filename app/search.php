<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
               <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="berd_home.php">Home</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="communities_page.php">Communities</a>
                        </li> 
                     </ul>
                     <a href="#" class = "ms-auto">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="28" height="28"> 
                     </a>
               </div>
            </div>
        </nav>
        <!-- Navigation bar-->
      <!--  <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script> -->
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container full-height-container d-flex flex-column">

            <div class="d-flex mt-5">
                <div class="col-3">    
                </div>
                <div class="d-flex justify-content-between col-9">
                    <button class="btn btn-light" type="button" onclick="document.location.href = `/search.php?search=+`">Save search results</button>
                    <button class="btn btn-light" type="button" onclick="document.location.href = `/search.php?search=+`">Notify me about new resources</button>
                    <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"> Weekly</button>
                </div>    
            </div>

            <div class="d-flex mt-5">
                
                <div class="col-3">
                    
                </div>

                <!-- <div class="d-flex justify-content-between col-9">
                    <button class="btn btn-light" type="button" >Save search results</button>
                    <button class="btn btn-light" type="button" >Notify me about new resources</button>
                    <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">Weekly</button>

                </div>
                 -->
                <div class="d-flex justify-content-between col-9">
                     
                    <div id="results-header"></div>
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Sort by
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#" onclick="sortByNewest()">Newest</a></li>
                            <li><a class="dropdown-item" href="#" onclick="sortByOldest()">Oldest</a></li>
                            <li><a class="dropdown-item" href="#">Best Match</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="d-flex">
                <div class="d-flex flex-column col-3 mt-2">
                    <h3>Filter</h3>
                    <div class="d-flex flex-column mt-4">
                        <h5>Access Status</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input access" type="checkbox" value="Metadata_only" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Metadata-only (4)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input access" type="checkbox" value="Open" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Open (3)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input access" type="checkbox" value="Marketplace" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Marketplace (1)
                            </label>
                        </div>
                    </div>
                    
                    <div class="d-flex flex-column mt-4">
                        <h5>Resource Types</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Dataset" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Dataset (7)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="MLApproach" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                ML Approach (7)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="PreProcessingMethod" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Preprocessing Method (6)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Publication" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Publication (5)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="BookWithData" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Book (4)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="BookWithDataDatabase" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Book digitized (3)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Task" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Task (2)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="EduTrainModule" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Education Module (1)
                            </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column mt-4">
                        <h5>Date of publication</h5>
                        <div class="form-check mt-2">    
                          
                            <div class=" row d-flex ">
                                <input class="m-2" type="text" id="rangePrimary" style="width: 80px " />
                                <input class="m-2" type="text" id="rangePrimary1" style="width: 80px " />
                            </div>
                            <div class="d-flex">
                                <input style="width: 80px" type="range" name="range" step="1" min="1920" max="1980" value="1930" onchange="rangePrimary.value=value">
                                <input style="width: 80px; ; margin-left: -1px; direction: rtl" type="range" name="range" step="1" min="-2022" max="-1980" value="-2000" onchange="rangePrimary1.value=(-1)*value">
                            </div>
                            
                        </div>
                    </div>
                    

                    


                    <div class="d-flex flex-column mt-4">
                        <h5>Creator / Author</h5>
                        <div class="form-check mt-2">
                            <input type="search" name="search" placeholder="Search creator/author" aria-label="Search">
                        </div>
                        
                        <!-- <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Structured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Klaus Tochtermann (11)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Unstructured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                            Anna Maria Höfler (10)
                            </label>
                        </div>
                        
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Unstructured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                            Isabella Peters (9)
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Unstructured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                            Stephanie B. Linek (8)
                            </label>
                        </div> -->

                        
                        
                        
                    </div>

                    <div class="d-flex flex-column mt-4">
                        <h5>Publisher</h5>
                        <div class="form-check mt-2">
                            <input type="search" name="search" placeholder="Search publisher" aria-label="Search">
                        </div>

                        <!-- <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Structured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Klaus Tochtermann (11)
                            </label>
                        </div>
                        
                        
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Unstructured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                            Isabella Peters (9)
                            </label>
                        </div> -->
                          
                    </div>

                    <div class="d-flex flex-column mt-4">
                        <h5>Versions</h5>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Show all versions</label>
                        </div>
                    </div>

                    <div class="d-flex flex-column mt-4">
                        <h5>Help</h5>
                        <a>Search guide </a>
                    </div>

                </div>
                <!-- <div class="flex-column mt-3" id="results" style="overflow-y: auto; height: 1000px;">  -->
                <div class="flex-column mt-3" id="results" style="overflow-y: auto;"> 
                </div>
            </div>
        </div>

        <!-- JS -->
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>
            
            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            update();

            /*Display datasets*/
            function update() {

                /*Applying the filters*/
                var access = document.querySelectorAll('.access');
                var resourceType = document.querySelectorAll('.resource-type');
                var dataType = document.querySelectorAll('.data-type');
                var accessValues = [];
                var dtValues = [];
                var rtValues = [];
                var newDatasets = [];

                for (var a of access) {
                    if (a.checked) {
                        accessValues.push(a.value);
                    }
                }

                for (var rt of resourceType) {
                    if (rt.checked) {
                        rtValues.push(rt.value);
                    }
                }

                for (var dt of dataType) {
                    if (dt.checked) {
                        dtValues.push(dt.value);
                    }
                }

                if (accessValues.length == 0 && rtValues.length == 0 && dtValues.length == 0) {
                    newDatasets = datasets.slice();
                } else {
                    for (var i = 0; i < datasets.length; i++) {
                        if (isIncluded(datasets[i].access, accessValues) && isIncluded(datasets[i].type, rtValues) && isIncluded(datasets[i].data, dtValues)) {
                            newDatasets.push(datasets[i]);
                        }      
                    }
                }  
                /*Variable needed to display the number of found results*/
                var count = 0;

                /*Fill the results segment*/
                document.getElementById("results").innerHTML = `
                    ${newDatasets.map(function(data) {
                        /*Check if the search term is included within the authors array*/
                        var found = false;
                        for(var i=0; i<data.authors.length; i++) {
                            if (data.authors[i].match(/<?php echo $_GET["search"]?>/i)) {
                                found = true;
                            }
                        }

                        /* Check if the search term is included within the title or text*/
                        if (data.title.match(/<?php echo $_GET["search"]?>/i) || data.text.match(/<?php echo $_GET["search"] ?>/i) || found) {
                            count++;
                            return `
                            <div class="d-flex flex-column mt-4" id="results" style="overflow: auto">
                                <div>
                                    <label class="bg-primary" style="color: white">&nbsp${data.date}&nbsp</label>
                                    <label class="bg-secondary" style="color: white">&nbsp${data.type}&nbsp</label>
                                    <label class="bg-danger" style="color: white">&nbsp${data.access}&nbsp</label>
                                </div>
                                <div class="d-inline-flex">
                                    <input class="form-check-input access m-4" type="checkbox" value="save_check" id="flexCheckDefault">
                                    <a href="View_${data.type}.php?id=${data.id}" class="mt-3" style="text-decoration: none;"> 
                                
                                        <font size="5">${data.title}</font size>
                                    </a>
                                </div>
                                <label class="mt-1"><b>${data.authors.join(", ")}</b></label>
                                <div class="d-flex pe-3">
                                    <p class="mt-2 text-line-limit">${data.text}</p>
                                </div>
                                <font size="2">Uploaded on ${data.uploaded}</font size>
                            </div>
                            `
                        } 
                    }).join("")}
                `
                if (count==0) {
                    document.getElementById("results").innerHTML = `
                        <h3 class="mt-4">No datasets were found ... :(</h3>
                        <div class="d-inline-flex flex-column mt-3">
                            <h4>Your options:</h4>
                            <a href="#" class="mt-3" style="font-size: 20px">Notify me, when a suitable dataset is avaliable</a>
                            <a href="request.php" class="mt-3" style="font-size: 20px">Submit Dataset Request for your research project</a>
                            <a href="upload.php" class="mt-3" style="font-size: 20px">Upload your own dataset for the given search</a>
                        </div>
                    `
                }

                /*Display the number of found results for the search term*/
                document.getElementById("results-header").innerHTML = `<h5>${count} results found for "<?php echo $_GET["search"] ?>"</h5>`
            }
                
            /*Function to sort the dataset array by the newest date*/
            function sortByNewest() {
                datasets.sort(function(a,b) {
                    return new Date(b.date) - new Date(a.date);
                });
                update();
            };

            /*Function to sort the dataset array by the oldest date*/
            function sortByOldest() {
                datasets.sort(function(a,b) {
                    return new Date(a.date) - new Date(b.date);
                });
                update();
            };

            function isIncluded(val, array) {
                if (array.includes(val) || array.length == 0) {
                    return true;
                } else {
                    return false;
                }
            }

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>