<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apply for dataset</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex mt-4">
            <div class="d-flex flex-column col-8">
                <h1 class="mt-4">Application for a corporate dataset</h1> 
                <hr>
                <div class="d-flex justify-content-between">
                    <h4 class="mt-2">You are going to apply to obtain the following dataset:</h4>
                    <div class="mt-2">
                        <button type="button" class="btn btn-light" onclick="history.back()">&#8592 Go Back</button>
                    </div>
                </div>
                <div class="container d-flex flex-column mt-5">   
                    <div class="d-flex" id="dataset"></div>
                </div>
        </div>
        <div class="d-flex col-1"></div>
        <div class="d-flex flex-column ps-2 col-3 mt-4 justify-content-evenly" style="border: black 1px solid; height: 340px; width: 320px; position: sticky; top: 50px;">
                <div>
                    <h5>Need help with the application?</h5>
                    <div>
                        <button type="button" class="btn btn-light mx-2 mt-2">&#8594 Click Here</button>
                    </div>
                </div>
                <div>
                    <h5>Don't have all the informations avaliable right now and want to continue later?</h5>
                    <div>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-lock">&nbspSave Progress</button>
                        <h5 class="mt-3 mx-2">or</h5>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-x" onclick="history.back()">&nbspCancel</button>
                    </div>
                </div>    
            </div>

        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column">
                    <div>
                        <label class="bg-primary" style="font-size: large; color: white">&nbsp${dataset.date}&nbsp</label>
                        <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                        <label class="bg-danger" style="font-size: large; color: white">&nbsp${dataset.access}&nbsp</label>
                    </div>  
                    <div class="d-flex justify-content-between">
                        <h1 class="mt-4">${dataset.title}</h1> 
                    </div>
                    <div class="mt-2">
                        <b>Author(s)</b>:
                        <label class="mt-1">${dataset.authors.join(", ")}</label>
                    </div>
                    <div class="mt-1">
                        <font size="2">Uploaded on ${dataset.uploaded}</font size>
                    </div>
                    <div class="mt-4">
                        <h3>Description</h3>
                        <hr>
                        <p class="mt-1">${dataset.text}</p>
                    </div>
                    <div class="mt-5">
                        <h3><nobr>Criteria that needs to be met to access this dataset</nobr></h3>
                        <p style="font-size: 14px">(Specified by the owner. <a href="#" style="color: black">Learn More</a>)</p>
                        <hr>
                        <p>${dataset.criteria}</p>
                    </div>
                    <div class="mt-5">
                        <h3><nobr>Please type in your application information</nobr></h3>
                        <p style="font-size: 14px">(This means all information on how you are planning to fulfill the data providers requirements. E.g. specific research question to be answered, license agreements to be signed, data protection rules to be complied with etc. <a href="#" style="color: black">Learn More</a>)</p>
                        <hr>
                        <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                    </div>
                    <div class="mt-4">
                        <a href="#">Terms and Conditions</a>
                        <div class="form-check mt-3">
                            <input class="form-check-input check1" type="checkbox" value="Video" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                I have read the terms and conditions of data usage and I agree with them
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success mt-3" onclick="submit()">Submit</button>
                    </div>
                `

            function submit() {
                if (document.querySelector('.check1').checked) {
                    alert("Thanks for you application! You will get a notification by E-mail once your application has been answered.");
                } else {
                    alert("Please agree to the terms and conditions of data usage.");
                }
            }
        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>