<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Portal Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">

</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main> 
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center main-container">
                    <!-- <h1 style="text-align: center;">Welcome to BERD@NFDI's Information Portal</h1> -->
                    <form method="GET" action="Search_data_portal.php" class="d-inline-flex mt-2" id="index-searchbar">
                        <div>
                            <input class="form-control me-2" type="search" name="search" placeholder="Search for Datasets ..." aria-label="Search">
                            <!--  -->
                            <div class="d-inline-flex mt-2">
                                <label class="form-check-label me-2" for="inlineCheckbox1">Quick filter: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    <label class="form-check-label" for="inlineCheckbox1">Open-source datasets</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Proprietary datasets</label>
                                </div>
                                
                                <!-- <a class="align-self-center rel-ml-1 " href="">more filters</a> -->
                            </div> 
                        </div> 
                            <!--  -->
                        <button class="btn btn-success mb-3 ms-2" type="button" onclick="document.location.href = `/Search_data_portal.php?search=+`" style="white-space: nowrap;">Search BERD</button>
                    </form>  
                       
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <div class="d-flex justify-content-evenly">
                        <div class="d-flex flex-column">
                            <h3>For data users (researchers)</h3>
                            <p>Access the world of Business, Economic and Related Data</p>
                            <hr>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>   -->
                            <p style="overflow:hidden; text-overflow: ellipsis; width:430px; height:120px;">Search and browse datasets curated by the BERD community, which ensures adherence to our quality standards for datasets, compliance with FAIR data principles, and relevance of content for Business, Economics and related fields. Our repository contains open-source datasets available for direct access, and proprietary datasets that require approval through the dataset owner. </p>  
                            <button type="button" class="btn btn-success mt-3" onclick="document.location.href = `/Search_data_portal.php?search=+`">Search Data</button>
                        </div>
                        <div class="col-1"></div>
                        <div class="d-flex flex-column">
                            <h3>For data providers (organizations)</h3>
                            <p>Corporate organizations & scientific dataset creators </p>
                            <hr>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>   -->
                            <p style="overflow:hidden; text-overflow: ellipsis; width:430px; height:120px;">Add datasets to our repository within a guided process that enables you to review adherence to dataset quality standards, compliance with FAIR data principles and relevance of content for the BERD community. Our Data Stewardship Program assists you in managing access options for others to your dataset, completing metadata and publishing your dataset through our repository. Access options for ...</p>
                            <button type="button" class="btn btn-success mt-3" onclick="document.location.href = `/upload.php`">Provide Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid bg-success">
            <div class="container">
                <div class="d-flex flex-column justify-content-center" style="min-height: 500px;">
                    <h2 style="text-align: center; color: white;">BERD Data Portal Facts</h2>
                    <div class="d-flex justify-content-evenly mt-5">
                        <div class="d-flex flex-column align-items-center bg-light" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">Total datasets</h3>
                            <h3 style="color: white">&nbsp</h3>
                            <h1 class="mt-2">3</h1>
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">Open-source</h3>
                            <h3>datasets</h3>
                            <h1 class="mt-2">2</h1>
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">Topic Areas</h3>
                            <h3 style="color: white">&nbsp</h3>
                            <h1 class="mt-2">#</h1>
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">...</h3>
                            <h3 style="color: white">&nbsp</h3>
                            <h1 class="mt-2">#</h1>
                        </div>
                    </div>
                    <div class="mt-3">
                        <p style="color: white;">BERD@NFDI is an initiative to build a powerful platform for collecting, processing, analyzing and preserving Business, Economic and Related Data, all in one place. We will facilitate the integrated management of algorithms and data along the whole research cycle, with a special focus on unstructured (big) data such as video, image, audio, text or mobile data. BERD@NFDI will provide infrastructures to the challenges of the expanded empirical research. We will not only foster community building, offer publicly available and online accessible data sets, and enhance data documentation and preservation guided by the FAIR principles. We will also provide an algorithm repository and benchmarks, computing and storage power to analyze (big) data as well as a broad set of APIs to interact with external systems. <a style="color: yellow;" href = "https://www.berd-nfdi.de/">Learn more </a> </p>
                    </div>
                </div>

                

            </div>
        </div>


        <div class="container-fluid">
            <div class="container">
                    <div class="d-flex flex-column align-items-center min-height-container m-4">
                        <h2 class="m-3" style="text-align: center;">Browse BERD</h2>
                            <div style="border:1px solid black;">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="opendata-tab" data-bs-toggle="tab" data-bs-target="#opendata" type="button" role="tab" aria-controls="opendata" aria-selected="true">Open-Source Datasets</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="propData-tab" data-bs-toggle="tab" data-bs-target="#propData" type="button" role="tab" aria-controls="propData" aria-selected="false">Proprietary Datasets</button>
                                    </li>
                                    <!-- <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Finance</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="other-tab" data-bs-toggle="tab" data-bs-target="#other" type="button" role="tab" aria-controls="other" aria-selected="false">Social Science</button>
                                    </li> -->
                                </ul>
                                <div class="tab-content m-3" id="myTabContent">
                                    <div class="tab-pane fade show active" id="opendata" role="tabpanel" aria-labelledby="opendata-tab">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="opendata-tab" data-bs-toggle="tab" data-bs-target="#opendata" type="button" role="tab" aria-controls="opendata" aria-selected="true">Marketing</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="opendata-tab" data-bs-toggle="tab" data-bs-target="#opendata" type="button" role="tab" aria-controls="opendata" aria-selected="false">Economics</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="opendata-tab" data-bs-toggle="tab" data-bs-target="#opendata" type="button" role="tab" aria-controls="opendata" aria-selected="false">Finance</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="opendata-tab" data-bs-toggle="tab" data-bs-target="#opendata" type="button" role="tab" aria-controls="opendata" aria-selected="false">Social Science</button>
                                            </li>
                                        </ul>
                                        
                                        <div class="d-flex justify-content-evenly">
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 1</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 2</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 3</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light mt-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 4</h3>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <!--  -->
                                    <div class="tab-pane fade" id="propData" role="tabpanel" aria-labelledby="propData-tab">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="propData-tab" data-bs-toggle="tab" data-bs-target="#propData" type="button" role="tab" aria-controls="propData" aria-selected="true">Marketing</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="propData-tab" data-bs-toggle="tab" data-bs-target="#propData" type="button" role="tab" aria-controls="propData" aria-selected="false">Economics</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="propData-tab" data-bs-toggle="tab" data-bs-target="#propData" type="button" role="tab" aria-controls="propData" aria-selected="false">Finance</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="propData-tab" data-bs-toggle="tab" data-bs-target="#propData" type="button" role="tab" aria-controls="propData" aria-selected="false">Social Science</button>
                                            </li>
                                        </ul>
                                        
                                        <div class="d-flex justify-content-evenly">
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 1</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 2</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 3</h3>
                                                
                                            </div>
                                            <div class="d-flex flex-column align-items-center bg-light mt-3" style="width: 235px; height: 215px;">
                                                <h3 class="mt-4">Data set 4</h3>
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>    
                        </div>
             </div>
        </div>
    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>