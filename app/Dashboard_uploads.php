<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Dashboard</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">


    <!-- collapse -->
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
               <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="berd_home.php">Home</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="communities_page.php">Communities</a>
                        </li> 
                     </ul>
                     <a href="#" class = "ms-auto">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="28" height="28"> 
                     </a>
               </div>
            </div>
        </nav>
        <!-- Navigation bar-->
      <!--  <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script> -->
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="invenio-page-body">
            <div class="ui container fluid page-subheader-outer with-submenu rel-pt-2 ml-0-mobile mr-0-mobile">
                <div class="ui container relaxed grid page-subheader mr-0-mobile ml-0-mobile">
                    <div class="row pb-0 m-3">
                        <div class="container d-flex justify-content-between">
                            <div class="d-flex">
                                <img class="mt-1" src="../user.jpg" title="Profile Picture" width="140" height="140">
                                <div class="ms-3">
                                    <label><b>John Doe</b></label><br>
                                    <label><b>jdoe</b></label><br>
                                    <label><b>jdoe@example.org</b></label><br>
                                </div>
                            </div>
                            <div class="sixteen wide mobile sixteen wide tablet three wide computer right aligned middle aligned column">
                                <a href="/uploads/new?community=62298ca9-6946-4ab9-a5f5-edfdd1e02bce" class="ui positive button labeled icon rel-mt-1">
                                <i class="setting icon" aria-hidden="true"></i> Manage account </a>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="ui container secondary pointing stackable menu page-subheader pl-0 pr-0">

                    <a class="item" onclick="javascript:location.href='Dashboard_favorites.php'" ><i aria-hidden="true" class="settings icon"></i>Favorites</a>
                
                    <a class="item active"><i aria-hidden="true" class="settings icon"></i>Uploads & Proposals</a>

                    <a class="item" onclick="javascript:location.href='Dashboard_communities.php'" ><i aria-hidden="true" class="settings icon"></i>Communities</a>

                    <a class="item" onclick="javascript:location.href='Dashboard_training.php'"><i aria-hidden="true" class="settings icon"></i>Training & Education</a>
                    <a class="item" onclick="javascript:location.href='Dashboard_resources.php'"><i aria-hidden="true" class="settings icon"></i>Resources</a>
                        
                </div>

                <div class="ui container grid communities-settings m-4">
                    
                    <div class="container full-height-container d-flex flex-column">
                        <!-- <p><a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                             Resources
                        </a></p>   -->

                        <div class="container d-flex justify-content-between">
                            <p><button type="button" class="btn btn-primary ms-4">My uploaded resources <!-- <i class="bi bi-arrow-down"></i> --></button></p>
                            <p><button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Upload new resource </button></p>
                        </div>    

                        <div class="d-flex collapse" id="myCollapse">
                            <div class="flex-column col-3 m-3" >
                                <input class="form-control me-2" type="search" name="search" placeholder="Search uploads" aria-label="Search">
                                <h4>Filter</h4>
                            
                                <div class="flex-column mt-4">

                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Dataset" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Datasets
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="ML_approach" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            ML Approaches
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Preprocessing_method" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Preproc. Methods
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Working_paper" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Papers
                                        </label>
                                    </div>
                                    
                                </div>
                            
                            
                            </div>
                            <div class="container d-flex flex-column mt-3" style="overflow-y: auto;" > 
                                <div class="d-flex justify-content-between ">
                                    
                                    <div id="results-header"><h5>3 results(s) found</h5></div>
                                    <div class="dropdown">
                                        <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Sort by
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="#" onclick="sortByNewest()">Newest</a></li>
                                            <li><a class="dropdown-item" href="#" onclick="sortByOldest()">Oldest</a></li>
                                            <li><a class="dropdown-item" href="#">Best Match</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div>
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbspIn review&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspJune 8, 2022&nbsp</label>
                                        <label class="bg-danger" style="color: white">&nbspDataset&nbsp</label>
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-download m-2"></i>
                                            <!-- <input class="form-check-input access m-4" type="checkbox" value="save_check" id="flexCheckDefault"> -->
                                            <a href=""  style="text-decoration: none;"> 
                                                DatasetX
                                            </a>
                                        </div>    
                                        <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                    </div>
                                    
                                    <!-- <div class="d-flex pe-3">
                                        <p class="mt-2 text-line-limit">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div> -->
                                    <p>Doe, Jane</p>
                                    Published June 8, 2022
                                </div>

                                <div class="flex-column mt-3">
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbspJune 7, 2022&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspML Approach&nbsp</label>
                                        <!-- <label class="bg-danger" style="color: white">&nbspOpen&nbsp</label> -->
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-check2 m-2"></i>
                                            <!-- <input class="form-check-input access m-4" type="checkbox" value="save_check" id="flexCheckDefault"> -->
                                            <a href=""  style="text-decoration: none;"> 
                                                ML Approach Y
                                            </a>
                                        </div>    
                                        <div>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> View </button>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                        </div> 
                                    </div>
                                    
                                    <!-- <div class="d-flex pe-3">
                                        <p class="mt-2 text-line-limit">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div> -->
                                    <p>Doe, Jane</p>
                                    Published June 7, 2022
                                </div>

                                <div class="mt-3">
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbsp2022-05-15&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspDataset&nbsp</label>
                                        <label class="bg-danger" style="color: white">&nbspOpen&nbsp</label>
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-x-circle m-2"></i>
                                            <a href=""  style="text-decoration: none;"> 
                                                Preprocessing Method Z
                                            </a>
                                        </div>
                                        <div>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> View feedback </button>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                        </div>    
                                    </div>
                                    
                                    <p>Doe, Jane</p>
                                    Published June 7, 2022
                                </div>

                            </div>
                        </div>   
                        
                        
                        <hr>

                        <div class="container d-flex justify-content-between">
                            <p><button type="button" class="btn btn-primary ms-4">My proposed resources <!-- <i class="bi bi-arrow-down"></i> --></button></p>
                            <p><button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Propose new resource </button></p>
                        </div> 

                        <div class="d-flex collapse" id="demo1">

                            <div class="d-flex flex-column col-3 m-3">
                                <input class="form-control me-2" type="search" name="search" placeholder="Search proposals" aria-label="Search">   
                                <h4>Filter</h4>
                            
                                <div class="d-flex flex-column mt-4">
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Dataset" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Datasets
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="ML_approach" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            ML Approaches
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Preprocessing_method" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Preproc. Methods
                                        </label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input resource-type" type="checkbox" value="Working_paper" id="flexCheckDefault" onchange="update()">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Papers
                                        </label>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="container d-flex flex-column mt-3" style="overflow-y: auto;" > 
                                <div class="d-flex justify-content-between ">
                                    
                                    <div id="results-header"><h5>3 results(s) found</h5></div>
                                    <div class="dropdown">
                                        <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Sort by
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="#" onclick="sortByNewest()">Newest</a></li>
                                            <li><a class="dropdown-item" href="#" onclick="sortByOldest()">Oldest</a></li>
                                            <li><a class="dropdown-item" href="#">Best Match</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div>
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbspPending&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspJune 8, 2022&nbsp</label>
                                        <label class="bg-danger" style="color: white">&nbspDataset&nbsp</label>
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-download m-2"></i>
                                            <!-- <input class="form-check-input access m-4" type="checkbox" value="save_check" id="flexCheckDefault"> -->
                                            <a href=""  style="text-decoration: none;"> 
                                                Dataset U
                                            </a>
                                        </div>    
                                        <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                    </div>
                                    
                                    <!-- <div class="d-flex pe-3">
                                        <p class="mt-2 text-line-limit">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div> -->
                                    <p>Doe, Jane</p>
                                    Published June 8, 2022
                                </div>

                                <div class="flex-column mt-3">
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbspJune 7, 2022&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspML Approach&nbsp</label>
                                        <!-- <label class="bg-danger" style="color: white">&nbspOpen&nbsp</label> -->
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-check2 m-2"></i>
                                            <!-- <input class="form-check-input access m-4" type="checkbox" value="save_check" id="flexCheckDefault"> -->
                                            <a href=""  style="text-decoration: none;"> 
                                                ML Approach V
                                            </a>
                                        </div>    
                                        <div>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> View </button>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                        </div> 
                                    </div>
                                    
                                    <!-- <div class="d-flex pe-3">
                                        <p class="mt-2 text-line-limit">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                    </div> -->
                                    <p>Doe, Jane</p>
                                    Published June 7, 2022
                                </div>

                                <div class="mt-3">
                                    <div>
                                        <label class="bg-primary" style="color: white">&nbspRejected&nbsp</label>
                                        <label class="bg-secondary" style="color: white">&nbspJune 7,2022&nbsp</label>
                                        <label class="bg-danger" style="color: white">&nbspPreprocessing Method&nbsp</label>
                                    </div>
                                    <div class="container d-flex justify-content-between">
                                        <div class="m-2">
                                            <i class="bi bi-x-circle m-2"></i>
                                            <a href=""  style="text-decoration: none;"> 
                                                Preprocessing Method W
                                            </a>
                                        </div>
                                        <div>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> View feedback </button>
                                            <button class="btn btn-secondary"><i class="setting icon" aria-hidden="true"></i> Edit </button>
                                        </div>    
                                    </div>
                                    
                                    <p>Doe, Jane</p>
                                    Published June 7, 2022
                                </div>

                            </div>

                            

                        </div>

                        
                    </div>
                </div>
                
            </div>
                
        </div>

    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

