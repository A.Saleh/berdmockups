<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="../index.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form method="GET" action="requests_admin.php" class="d-inline-flex col-4">
                        <input class="form-control me-2" type="search" name="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-light me-2" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../dashboard.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../upload.php">Upload</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='../login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='../register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container full-height-container d-flex flex-column">
            <div class="d-flex align-items-center mt-5">
                <div>
                    <h1>Dataset Requests</h1>
                </div>
                <div class="ms-4">
                    <button type="button" class="btn btn-light" onclick="history.back()">&#8592 Go Back</button>
                </div>
            </div>
            <hr>
            <div class="d-flex mt-3">
                <div class="col-3">
                </div>
                <div class="d-flex justify-content-between col-9">
                    <div id="results-header"></div>
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Sort by
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#" onclick="sortByNewest()">Newest</a></li>
                            <li><a class="dropdown-item" href="#" onclick="sortByOldest()">Oldest</a></li>
                            <li><a class="dropdown-item" href="#">Best Match</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="d-flex flex-column col-3 mt-2">
                <h3>Admin Filters</h3>
                    <div class="d-flex flex-column mt-4">
                        <h5>Requests</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input requests" type="checkbox" value="Open" id="flexCheckDefault" onchange="update()" checked>
                            <label class="form-check-label" for="flexCheckDefault">
                                Open
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input requests" type="checkbox" value="Forwarded" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Forwarded
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input requests" type="checkbox" value="Declined" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Declined
                            </label>
                        </div>
                    </div>
                    <h3 class="mt-5">Regular Filters</h3>
                    <div class="d-flex flex-column mt-4">
                        <h5>Data Type</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Structured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Structured
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input data-type" type="checkbox" value="Unstructured" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Unstructured
                            </label>
                        </div>
                    </div>
                    <div class="d-flex flex-column mt-4">
                        <h5>Resource Type</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Publication" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Publication
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Dataset" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckChecked">
                                Dataset
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Image" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Image
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Video" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Video
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Audio" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Audio
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input resource-type" type="checkbox" value="Other" id="flexCheckDefault" onchange="update()">
                            <label class="form-check-label" for="flexCheckDefault">
                                Other
                            </label>
                        </div>
                    </div>
                </div>
                <div class="flex-column mt-3" id="results" style="overflow-y: auto; height: 800px;"> 
                </div>
            </div>
        </div>

        <!-- JS -->
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "../js/requests.js"; ?>
            
            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            update();

            /*Display datasets*/
            function update() {

                /*Applying the filters*/
                var resourceType = document.querySelectorAll('.resource-type');
                var dataType = document.querySelectorAll('.data-type');
                var reqs = document.querySelectorAll('.requests');
                var dtValues = [];
                var rtValues = [];
                var reqValues = [];
                var newRequests = [];

                for (var rt of resourceType) {
                    if (rt.checked) {
                        rtValues.push(rt.value);
                    }
                }

                for (var dt of dataType) {
                    if (dt.checked) {
                        dtValues.push(dt.value);
                    }
                }

                for (var rq of reqs) {
                    if (rq.checked) {
                        reqValues.push(rq.value);
                    }
                }

                if (rtValues.length == 0 && dtValues.length == 0 && reqValues.length == 0) {
                    newRequests = requests.slice();
                } else {
                    for (var i = 0; i < requests.length; i++) {
                        if (isIncluded(requests[i].type, rtValues) && isIncluded(requests[i].data, dtValues) && isIncluded(requests[i].request_status, reqValues)) {
                            newRequests.push(requests[i]);
                        }      
                    }
                }  
                /*Variable needed to display the number of found results*/
                var count = 0;

                /*Fill the results segment*/
                document.getElementById("results").innerHTML = `
                    ${newRequests.map(function(data) {
                        /*Check status of the data to display the right icon*/
                        var icon = "open";
                        if (data.request_status == "Declined") {
                            icon = "cross";
                        }
                        if (data.request_status == "Forwarded") {
                            icon = "check";
                        }

                        /* Check if the search term is included within the title or text*/
                        if (data.title_ds.match(/<?php echo $_GET["search"]?>/i) || data.title_rp.match(/<?php echo $_GET["search"]?>/i) || data.text_ds.match(/<?php echo $_GET["search"] ?>/i) || data.text_rp.match(/<?php echo $_GET["search"] ?>/i)) {
                            count++;
                            return `
                            <div class="d-flex flex-column mt-4 mb-5" id="results" style="overflow: auto">
                                <div>
                                    <label class="bg-primary" style="color: white">&nbsp${data.uploaded}&nbsp</label>
                                    <label class="bg-secondary" style="color: white">&nbsp${data.type}&nbsp</label>
                                </div>
                                <div class="d-inline-flex">
                                    <a href="view_request_admin.php?id=${data.id}" class="mt-2" style="text-decoration: none;">
                                        <font size="5">${data.title_ds}</font size>
                                    </a>
                                </div>
                                <div class="d-flex pe-3">
                                    <p class="mt-2 text-line-limit">${data.text_ds}</p>
                                </div>
                                <div class="d-flex align-items-center">
                                    <b><font size="4">Status: </font size></b>
                                    <img class="ms-2" src="../icons/${icon}.jpg" title="${data.request_status}" width="25" height="25">
                                </div>
                            </div>
                            `
                        } 
                    }).join("")}
                `
                if (count==0) {
                    document.getElementById("results").innerHTML = `
                        <h3 class="mt-4">No datasets were found ... :(</h3>
                        <div class="d-inline-flex flex-column mt-3">
                            <h4>Your options:</h4>
                            <a href="#" class="mt-3" style="font-size: 20px">Notify me, when a suitable dataset is avaliable</a>
                            <a href="request.php" class="mt-3" style="font-size: 20px">Submit Dataset Request for your research project</a>
                            <a href="upload.php" class="mt-3" style="font-size: 20px">Upload your own dataset for the given search</a>
                        </div>
                    `
                }

                /*Display the number of found results for the search term*/
                document.getElementById("results-header").innerHTML = `<h5>${count} results found for "<?php echo $_GET["search"] ?>"</h5>`
            }
                
            /*Function to sort the dataset array by the newest date*/
            function sortByNewest() {
                requests.sort(function(a,b) {
                    return new Date(b.uploaded) - new Date(a.uploaded);
                });
                update();
            };

            /*Function to sort the dataset array by the oldest date*/
            function sortByOldest() {
                requests.sort(function(a,b) {
                    return new Date(a.uploaded) - new Date(b.uploaded);
                });
                update();
            };

            function isIncluded(val, array) {
                if (array.includes(val) || array.length == 0) {
                    return true;
                } else {
                    return false;
                }
            }

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>