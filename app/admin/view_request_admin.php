<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>
        <div class="container d-flex flex-column mt-5">
            <div class="d-flex flex-column col-8">
                <div>
                    <h3>Review this dataset request</h3>
                    <hr>
                </div>
                <p><b>Note:</b> When approving a dataset request you need to select a dataset from a specific corporate data provider after.</p>
                <div class="d-flex mt-4 align-items-center"> 
                    <div><h5 style="color: #d1131c"><u>Your review:</u></h5>
                    </div>
                    <div class="dropdown ms-3">  
                        <button class="btn btn-light dropdown-toggle" type="button" id="review" data-bs-toggle="dropdown" aria-expanded="false">
                        &nbsp&#10004 Approve
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="review">
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Approve">&nbsp&#10004 Approve</a></li>
                            <li><a class="dropdown-item" href="javascript:void(0)" value="Decline">&nbsp&#10060 Decline</a></li>
                        </ul> 
                    </div>
                </div>
                <div class="mt-4" id="status"></div>
                <div class="d-flex flex-column" id="comments" style="overflow-y: auto; max-height: 500px;"></div>
                <div class="d-flex flex-column mt-5">
                    <label for="exampleFormControlTextarea1" class="form-label"><h4>Leave a comment to explain your decision (optional)</h4></label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                    <div>
                        <button class="btn btn-success mt-4" type="submit">Submit</button> 
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            /*Displays selected item as "title" of the dropdown button*/ 
            $(".dropdown-menu li a").click(function(){
                $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
                $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });

            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "../js/requests.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < requests.length; i++) {
                if (<?php echo $_GET["id"]?> == requests[i].id ) {
                    dataset = requests[i];
                }
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column col-8">
                    <h1 class="mt-4">You are reviewing the following request:</h1> 
                    <hr>
                    <div class="mt-4">
                        <label class="bg-primary" style="font-size: large; color: white">&nbsp${dataset.uploaded}&nbsp</label>
                        <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                    </div>  
                    <div class="d-flex justify-content-between">
                        <h1 class="mt-4">${dataset.title_ds}</h1> 
                        <div>
                            <button type="button" class="btn btn-light" onclick="history.back()">&#8592 Go Back</button>
                        </div>
                    </div>
                    <div class="mt-4">
                        <h3>Description of the needed dataset</h3>
                        <hr>
                        <p class="mt-1">${dataset.text_ds}</p>
                    </div>
                    <div class="mt-4">
                        <h3>Description of the requesters research project</h3>
                        <hr>
                        <h4 class="mt-4 mb-3">Title: <u>${dataset.title_rp}</u></h4>
                        <p>${dataset.text_rp}</p>
                    </div>
                </div>
                <div class="d-flex col-1"></div>
                <div class="d-flex flex-column ps-2 col-2" style="border: black 1px solid; height: 350px;">
                    <h5 class="mt-4">Requested Metadata</h5>
                    <hr>
                    <u><b>Data Type</b></u>
                    ${dataset.data}
                    <u class="mt-2"><b>Resource Type</b></u>
                    ${dataset.type}
                    <u class="mt-2"><b>Language</b></u>
                    ${dataset.language}
                    <u class="mt-2"><b>Data Format</b></u>
                    ${dataset.format}
                </div>
            `

            /*Check status of the data to display the right icon*/
            var icon = "open";
            if (dataset.request_status == "Declined") {
                icon = "cross";
            }
            if (dataset.request_status == "Forwarded") {
                icon = "check";
            }

            document.getElementById("status").innerHTML = `
                <h6 class="mt-1">(Current request status:<img class="ms-2" src="../icons/${icon}.jpg" title="${dataset.proposal_status}" width="25" height="25">)</h6>
            `

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>