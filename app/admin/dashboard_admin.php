<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>
    
    <main>
        <div class="container d-flex flex-column align-items-center mt-4">
            <h1 class="mt-4">Admin Dashboard</h1> 
            <div class="container mt-4">
                <h2 style="text-align: left">All about datasets:</h2>
                <hr> 
            </div>
            <div class="card-container d-flex justify-content-between mt-3">
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-danger">
                        <a href="#" class="text-danger"><h5 class="card-title">See all datasets</h5></a>
                        <h6 class="card-subtitle mb-2 text-muted">Last updated on 2022-06-21</h6>
                        <p class="card-text mt-3">An overview of all datasets currently available on the platform.</p>
                        <a href="upload_admin.php" class="card-link text-danger">Add a new dataset</a>
                    </div>
                </div>
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-primary">
                        <a href="proposals_admin.php?search=+" class="text-primary"><h5 class="card-title">Review proposals</h5></a>
                        <h6 class="card-subtitle mb-2 text-muted">Last updated on 2022-06-13</h6>
                        <p class="card-text mt-3">An overview of all datasets proposals.</p>
                        <br>
                        <a href="view_proposal_admin.php?id=1" class="card-link text-primary">Get me to the latest propose</a>
                    </div>
                </div>
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-success">
                        <a href="requests_admin.php?search=+" class="text-success"><h5 class="card-title">Review dataset requests</h5></a>
                        <h6 class="card-subtitle mb-2 text-muted">Last updated on 2022-05-17</h6>
                        <p class="card-text mt-3">An overview of all research dataset requests.</p>
                        <a href="request.php" class="card-link text-success">Get me to the latest request</a>
                    </div>
                </div>
            </div>
            <div class="container mt-5">
                <h2 style="text-align: left">All about your account:</h2>
                <hr> 
            </div>
            <div class="card-container d-flex justify-content-between mt-3">
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-info">
                        <a href="#" class="text-info"><h5 class="card-title">Change contact details</h5></a>
                        <h6 class="card-subtitle mb-2 text-muted">Last updated on 2022-04-02</h6>
                        <p class="card-text mt-3">Always keep your contact information updated, so others can stay in touch with you.</p>
                    </div>
                </div>
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-warning">
                        <a href="#" class="text-warning"><h5 class="card-title">Change Password</h5></a>
                        <p class="card-text mt-4">Make sure to always have a save and unique password for your account.</p>
                        <a href="search.php?search=+" class="card-link text-warning">What is a 'save' password?</a>
                    </div>
                </div>
                <div class="card" style="width: 20rem; height: 12rem;">
                    <div class="card-body border border-secondary">
                        <a href="#" class="text-secondary"><h5 class="card-title">Settings</h5></a>
                        <h6 class="card-subtitle mb-2 text-muted">Last updated on 2022-03-12</h6>
                        <p class="card-text mt-3">General account settings.</p>
                        <a href="search.php?search=+" class="card-link text-secondary">Get help.</a>
                    </div>
                </div>
            </div>
            <div style="height: 70px">
            </div>
        </div>

        <script>
        /*Displays selected item as "title" of the dropdown button*/ 
        $(".dropdown-menu li a").click(function(){
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });
        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>