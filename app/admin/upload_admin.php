<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex mt-4">
            <div class="d-flex flex-column col-8">
                <h1 class="mt-4">Upload a Dataset</h1> 
                <hr>
                <div>
                    <h4 class="mt-3">Do you want to upload a dataset yourself or provide a link to an external repository, where the dataset will be downloaded from?</h4>
                    <div class="d-flex align-items-center mt-4">
                        <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Upload
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="javascript:void(0)" onclick="uploadOption()">Upload</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" onclick="linkOption()">Link</a></li>
                            </ul>
                        </div>
                        <div id="option">
                                <button type="button" class="btn btn-light bi bi-upload mx-4">&nbspUpload dataset</button> 
                        </div>
                    </div>
                </div>
                    <div><h3 class="mt-5">Please enter the information of your Dataset below</h3>
                    <p style="font-size: 14px">(Boxes with a * are obligatory)</p> 
                    <hr>
                </div>
                <form style="width: 550px;">
                        <h5 class="mt-3 mb-3"><nobr>Enter the title and the authors of your dataset*</nobr></h5>
                        <div class="mb-3 input-group mt-2">
                            <div class="input-group-text"><i class="bi bi-card-text"></i></div>
                            <input type="text" class="form-control" id="inputName" placeholder="Title">
                        </div>
                        <div class="mb-3 input-group">
                            <div class="input-group-text"><i class="bi bi-person"></i></div>
                            <input type="text" class="form-control" id="inputEmail" placeholder="Author(s)">
                        </div>
                        <h5 class="mt-5"><nobr>Select the metadata, that describes your dataset best</nobr></h5>
                        <p style="font-size: 14px">(Obligatory for open datasets. <a href="#" style="color: black">Learn More</a>)</p>
                        <div class="dropdown mb-3">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Resource Type
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Publication">Publication</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Dataset">Dataset</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Image">Image</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Video">Video</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Audio">Audio</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" value="Other">Other</a></li>
                            </ul>
                        </div>
                        <div class="dropdown mb-3">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Language
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="javascript:void(0)">English</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">German</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">French</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">Italian</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">Spanish</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">Other</a></li>
                            </ul>
                        </div>
                        <div class="dropdown mb-3">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Data-Format
                            </button>
                            <ul class="dropdown-menu" id="dataformat" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="javascript:void(0)">pdf</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">doc</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">mp3</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">wma</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">mp4</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)">Other</a></li>
                            </ul>
                        </div>
                        <h5 class="mt-5"><nobr>Add a description to your dataset*</nobr></h5>
                        <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                        <h5 class="mt-5"><nobr>If your dataset already has a digital object identifier (DOI), you can add it below</nobr></h5>
                        <a href="#" style="font-size: 14px; color: black">(More information about DOIs)</a>
                        <div class="mb-3 input-group mt-3">
                            <div class="input-group-text"><i class="bi bi-card-text"></i></div>
                            <input type="email" class="form-control" id="inputName" placeholder="DOI">
                        </div>
                        <h5 class="mt-5"><nobr>Set the access rights for your dataset*</nobr></h5>
                        <p style="font-size: 14px"><a href="#" style="color: black">Learn More</a>)</p>
                        <div class="dropdown mb-3 mt-3">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Open
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="javascript:void(0)" onclick="openAccess()">Open</a></li>
                                <li><a class="dropdown-item" href="javascript:void(0)" onclick="marketplaceAccess()">Marketplace</a></li>
                            </ul>
                        </div>
                        <div class="mb-3" id="marketplace"></div>
                        <h5 class="mt-5"><nobr>Submit the Dataset</nobr></h5>
                        <p style="font-size: 14px">(The dataset will be immediately avabliable on the plattform)</p>
                        <button type="submit" class="btn btn-success mt-3">Submit</button>
                    </form>
            </div>
            <div class="d-flex col-1"></div>
            <div class="d-flex flex-column ps-2 col-3 mt-4 justify-content-evenly" style="border: black 1px solid; height: 280px; width: 320px; position: sticky; top: 50px;">
                <div>
                    <h5>Don't have all the informations avaliable right now and want to continue later?</h5>
                    <div>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-lock">&nbspSave Progress</button>
                        <h5 class="mt-3 mx-2">or</h5>
                        <button type="button" class="btn btn-light mx-2 mt-2 bi bi-x" onclick="history.back()">&nbspCancel</button>
                    </div>
                </div>    
            </div>
        </div>

        <script>

        /*Displays selected item as "title" of the dropdown button*/ 
        $(".dropdown-menu li a").click(function(){
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });

        /*Variable to hide and show marketplace related content (default true)*/
        var hide = true;

        /*Variable to hide and show marketplace related content (default true)*/
        var upload = true;

        function uploadOption() {
            upload = true;
            update1();
        };

        function linkOption() {
            upload = false;
            update1();
        };

        function update1() {
            if (!upload) {
                document.getElementById("option").innerHTML = `
                <div class="input-group mx-4" style="width: 500px;">
                    <div class="input-group-text"><i class="bi bi-card-text"></i></div>
                    <input type="email" class="form-control" id="inputName" placeholder="Insert the link here ...">
                </div>
                `
            } else {
                document.getElementById("option").innerHTML = `
                    <button type="button" class="btn btn-light bi bi-upload mx-4">&nbspUpload dataset</button>  
                `
            } 
        };

        function openAccess() {
            hide = true;
            update2();
        };

        function marketplaceAccess() {
            hide = false;
            update2();
        };

        function update2() {
            if (!hide) {
                document.getElementById("marketplace").innerHTML = `
                    <h5 class="mt-4"><nobr>Please specify the criteria that need to be met to access the dataset*</nobr></h5>
                    <p style="font-size: 14px">(e.g. specific research question to be answered, license agreements to be signed, data protection rules to be complied with etc.)</font size>
                    <textarea class="form-control mt-3" id="exampleFormControlTextarea1" rows="7"></textarea>
                `
            } else {
                document.getElementById("marketplace").innerHTML = `
                `
            } 
        };

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>