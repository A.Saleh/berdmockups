<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Community management page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">

</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="index.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="dashboard.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="invenio-page-body">
                <div class="ui container fluid page-subheader-outer with-submenu rel-pt-2 ml-0-mobile mr-0-mobile">
                    <div class="ui container relaxed grid page-subheader mr-0-mobile ml-0-mobile">
                        <div class="row pb-0">
                            <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                                <div class="ui rounded image community-image mt-5 rel-mr-2 left-floated">
                                    <img src="/icons/user.jpg" alt="" onerror="this.onerror=null;this.src='/static/images/square-placeholder.png'">
                                </div>
                                <div class="flex rel-mb-1">
                                    <h2 class="ui header mb-0">Community X</h2>
                                </div>
                                <div></div>
                            </div>
                            <div class="sixteen wide mobile sixteen wide tablet three wide computer right aligned middle aligned column">
                                <a onclick="document.location.href = `/upload.php`" class="ui positive button labeled icon rel-mt-1">
                                <i class="plus icon" aria-hidden="true"></i> New upload </a>
                            </div>
                        
                        </div>
                        
                    </div>
                    
                            <div class="ui container secondary pointing stackable menu page-subheader pl-0 pr-0">

                                <a class="item" onclick="javascript:location.href='community_management_page_home_tab.php'" ><i aria-hidden="true" class="settings icon"></i>Home</a>
                            
                                <a class="item" onclick="javascript:location.href='community_management_page_settings_tab.php'" ><i aria-hidden="true" class="settings icon"></i>Settings</a>

                                <a class="item" onclick="javascript:location.href='community_management_page_search_tab.php'"><i aria-hidden="true" class="search icon" href="community_management_page_search_tab"></i>Search</a>

                                <a class="item active" ><i aria-hidden="true" class="settings icon"></i>Members</a>
                                    
                            </div>

                            <div class="ui container">
                                <div class="container">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        
                                        <form method="GET" action="search.php" class="d-inline-flex" id="index-searchbar">
                                                <input class="form-control me-2" type="search" name="search" placeholder="Search members.." aria-label="Search">
                                                <button class="btn btn-light" type="button" >Search</button>
                                                
                                        </form>  

                                    </div>

                                </div>
                                
                                <div class="ui center aligned segment"><div class="ui icon header"><i aria-hidden="true" class="users icon"></i>This community has no public members.</div><br></div>
                            </div>

                           

                    
                </div>

                
                
        </div>

    </main>
    
    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>

</html>