<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center full-height-container">
                    <h1 style="color: #198754;">Register at BERD@NFDI</h1>
                    <p style="color: #198754;">Already have an account? <a href="/login.php">Log In</a></p>
                    <div class="d-flex justify-content-center align-items-center bg-light mt-3" style="width: 800px; height: 600px">
                        <form style="width: 400px;">
                            <h3 style="color: #198754; text-align: center;">Please enter your information</h2>
                            <div class="mb-3 input-group mt-4">
                                <div class="input-group-text"><i class="bi bi-person"></i></div>
                                <input type="email" class="form-control" id="inputName" aria-describedby="emailHelp" placeholder="Full name">
                            </div>
                            <div class="mb-3 input-group">
                                <div class="input-group-text"><i class="bi bi-envelope"></i></div>
                                <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="E-mail">
                            </div>
                            <div class="mb-3 input-group">
                                <div class="input-group-text"><i class="bi bi-key"></i></div>
                                <input type="email" class="form-control" id="inputPassword1" aria-describedby="emailHelp" placeholder="Password">
                            </div>
                            <div class="mb-3 input-group">
                                <div class="input-group-text"><i class="bi bi-key"></i></div>
                                <input type="email" class="form-control" id="inputPassword2" aria-describedby="emailHelp" placeholder="Please repeat your password">
                            </div>
                            <div class="mb-3 form-check">
                                <a href="#">Terms and Conditions</a>
                            </div>
                            <div class="mb-3 form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">I have read the terms and conditions and I agree with them</label>
                            </div>
                            <div class="mb-3 form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Sign up for the Newsletter</label>
                            </div>
                            <button type="submit" class="btn btn-success">Sign Up</button>
                            <div class="text-center">
                                
                                <p>or Sign up with:</p>
                                <button type="button" class="btn btn-success">
                                 Facebook
                                </button>

                                <button type="button" class="btn btn-success">
                                Google
                                </button>

                                <button type="button" class="btn btn-success">
                                Twitter
                                </button>

                                <button type="button" class="btn btn-success">
                                Github
                                </button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </main>

    <footer>
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>