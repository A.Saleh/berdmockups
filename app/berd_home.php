<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BERD Home page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">


</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main> 
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center main-container">
                    
                    <form method="GET" action="search.php" class="d-inline-flex mt-2" id="index-searchbar">
                            <input class="form-control me-2" type="search" name="search" placeholder="Search for Resources ..." aria-label="Search">
                            <button class="btn btn-light" type="button" onclick="document.location.href = `/search.php?search=+`">Search</button>      
                    </form>  
                    <h1 class= "mt-5" style="text-align: center;">A Platform for Business, Economic and Related Data</h1>
                    <p>BERD@NFDI is a powerful platform for collecting, processing, analyzing and preserving Business,
                        Economic and Related Data all in one place. We facilitate the integrated management of algorithms
                        and data along the whole research cycle, with a special focus on unstructured (big) data such as 
                        video, images, audio, text or mobile data.
                </div>
            </div>
        </div>
        <div class="container-fluid bg-success">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <h2 style="text-align: center; color: white;">BERD Portals</h2>
                    <div class="d-flex justify-content-evenly">
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4"><a href="data_portal_page.php">Data Portal</a></h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4"><a href="machine_learning_portal.php">Machine Learning Approaches & Tasks</a></h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4">Training & Education</h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">Show all...</h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <h2 style="text-align: left; color: black;">Explore</h2>
                    <div class="d-flex justify-content-evenly">
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            
                            <h3 class="m-4"><a href="communities_page.php"> 120 Communities</a></h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4">223475 Resources</h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-success">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <h2 style="text-align: left; color: white;">News</h2>
                    <div class="ui grid">

                        <div class="row m-2">
                                <div class="ui segment ">
                                    <div class="twelve wide computer sixteen wide mobile ten wide tablet column">
                                        <h3 class="ui small header">July 4, 2022</h3>
                                        <p>The datasets have become more Fair</p>
                                    </div>
                                </div>
                        </div>  

                        <div class="row m-2">
                            <div class="ui segment ">
                                <div class="left floated twelve wide computer sixteen wide mobile ten wide tablet column">
                                    <h3 class="ui small header">May 14, 2022</h3>
                                    <p>The BERD@NFDI is hosting an event</p>
                                </div>
                            </div>    
                        </div>   
                    </div>
                </div>
            </div>
        </div>


        

        <div class="container-fluid ">
            <div class="container">
                <div class="d-flex flex-column justify-content-center " style="min-height: 300px;">
                    <h2 style="text-align: left; color: black;">Partners</h2>

                    <div class="ui grid">
                        <!-- <div class="d-flex flex-row justify-content-between mt-5">
                            <h1 class="me-2">LOGO1</h1>
                            <h1 class="me-2">LOGO2</h1>  
                            <h1 class="me-2">LOGO3</h1>  
                            <h1>LOGO4</h1>         
                        </div>
                        <div class="d-flex flex-row justify-content-between mt-5">
                            <h1 class="me-2">LOGO5</h1>
                            <h1 class="me-2">LOGO6</h1>  
                            <h1 class="me-2">LOGO7</h1>  
                            <h1>LOGO8</h1>         
                        </div> -->
                        <div class="four wide column"><h3>LOGO1</h3></div>
                        <div class="four wide column"><h3>LOGO2</h3></div>
                        <div class="four wide column"><h3>LOGO3</h3></div>
                        <div class="four wide column"><h3>LOGO4</h3></div>
                        <div class="four wide column"><h3>LOGO5</h3></div>
                        <div class="four wide column"><h3>LOGO6</h3></div>
                        <div class="four wide column"><h3>LOGO7</h3></div>
                        <div class="four wide column"><h3>LOGO8</h3></div>
                    </div>
                        
                </div>
            </div>
        </div>

        
    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>