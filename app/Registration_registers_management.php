<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Registers Management</title>

    <!-- -->
<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<!-- Custom CSS -->
<link rel="stylesheet" href="/css/style.css">

<!-- Bootstrap Icons -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<!-- Semantic UI -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">


<!-- collapse -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- -->
    
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>


</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
               <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="berd_home.php">Home</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="communities_page.php">Communities</a>
                        </li> 
                     </ul>
                     <a href="#" class = "ms-auto">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="28" height="28"> 
                     </a>
               </div>
            </div>
        </nav>
        <!-- Navigation bar-->
      <!--  <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script> -->
        <!-- End of navigation bar-->
    </header>

    <main>
        
          
        <!-- Application form -->
        <div class="container d-flex mt-4">
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column col-11">
                <h2 class="ui header mb-0">Current Event</h2>
                <label class="mt-3">Data protection Basics</label><br>
                <label class="mt-3">09.11.2022, 12:00-12:45 Uhr</label><br>
                <div class="mt-3">
                    
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Date</th>
                                <th>2022-11-09</th>
                                <th><a href="#"><i class="edit icon"></i></a></th>
                            </tr>
                        
                            <tr>
                                <th>Event Series</th>
                                <th>Data Literacy Essentials</th>
                                <th><a href="#"><i class="edit icon"></i></a></th>

                            </tr>
                            <tr>
                                <th>Meeting link</th>
                                <th>https://uni-mannheim.zoom.us</th>
                                <th><a href="#"><i class="edit icon"></i></a></th>

                            </tr>

                        </tbody>
                    </table>
                    
                </div>

                <h3 class="ui header mb-0">Registrations</h2>
                <div class="d-flex justify-content-between mt-4 mb-2 ">
                  <!-- <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#demo">Show Details</button> -->
                  <button class="btn btn-success" type="button" >Show Details</button>
                  <button class="btn btn-success">Send an email</button>
                  <button class="btn btn-success">Delete all registrations</button>
               </div>
               
               <div class="d-flex mt-4 mb-2 ">
                    <ul class="mt-2"> 
                        <li>Number of Registrations: 4</li>
                        <li>Verified: 3</li>
                        <ul>
                        <li>uni-mannheim.de: 2</li>
                        <li>gmail: 1</li>    
                        </ul>
                        <li>Unverified: 1</li>
                    </ul>
                </div>
               <!-- <div id="demo" class="collapse">
                    <ul class="mt-2"> 
                        <li>Number of Registrations: 4</li>
                        <li>Verified: 3</li>
                        <ul>
                        <li>uni-mannheim.de: 2</li>
                        <li>gmail: 1</li>    
                        </ul>
                        <li>Unverified: 1</li>
                    </ul>
                </div> -->

               <div class="mt-3">
                   
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Email <a href="#"><i class="download icon"></i></a></th>
                                <th>Gender</th>
                                <th>Education</th>
                                <th>Verified</th>
                                <th>Times (Reistered - Confirmed)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>philio.zumstein@uni-mannheim.de</th>
                                <th>Male</th>
                                <th>Bachelor of Science</th>
                                <th>Yes</th>
                                <th>2022-10-18 19:30:00 <br> 2022-10-18 19:30:00</th>
                                <th>
                                    <a href="#"><i class="trash icon"></i></a>
                                </th>

                            </tr>
                            <tr>
                                <th>ramzi.mraidi@gmail.de</th>
                                <th>Male</th>
                                <th>Master of Science</th>
                                <th>Yes</th>
                                <th>2022-10-18 19:30:00 <br> 2022-10-18 19:30:00</th>
                                <th>
                                    <a href="#"><i class="trash icon"></i></a>
                                </th>

                            </tr>

                            <tr>
                                <th>jgessner@uni-mannheim.de</th>
                                <th>Female</th>
                                <th>Master of Science</th>
                                <th>Yes</th>
                                <th>2022-10-18 19:30:00 <br> 2022-10-18 19:30:00</th>
                                <th>
                                    <a href="#"><i class="trash icon"></i></a>
                                </th>

                            </tr>

                            <tr>
                                <th>moritz.moller@uni-koeln.de</th>
                                <th>Male</th>
                                <th>Bachelor of Science</th>
                                <th>No</th>
                                <th>2022-10-18 19:30:00 <br> 2022-10-18 19:30:00</th>
                                <th>
                                    <a href="#"><i class="check icon"></i></a>
                                    <a href="#"><i class="trash icon"></i></a>
                                </th>

                            </tr>

                        </tbody>
                    </table>
                    
                </div>

                
            </div>

            
            
         </div>
         <!--  -->
        


    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    <!-- Scripts -->
    
    
</body>
</html>