<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Machine Learning Portal Page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">

</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main> 
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center main-container">
                    <!-- <h1 style="text-align: center;">Welcome to BERD@NFDI's Information Portal</h1> -->
                    <form class="d-inline-flex mt-2" id="index-searchbar">
                        <div>
                            <input class="form-control me-2" type="search" name="search" placeholder="Search for Resources ..." aria-label="Search">
                            <!--  -->
                            <div class="d-inline-flex mt-2">
                                <label class="form-check-label me-2" for="inlineCheckbox1">Quick filter: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    <label class="form-check-label" for="inlineCheckbox1">ML Aprroaches</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">Tasks</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                    <label class="form-check-label" for="inlineCheckbox3">Pre-processing Method</label>
                                </div>
                                
                                <!-- <a class="align-self-center rel-ml-1 " href="">more filters</a> -->
                            </div> 
                        </div> 
                            <!--  -->
                        <button class="btn btn-success mb-3 ms-2" type="button" style="white-space: nowrap;">Search BERD</button>
                    </form>  
                       
                </div>
            </div>
        </div>

        <div class="container-fluid bg-success mt-2">
            <div class="container">
                <div class="d-flex flex-column min-height-container">
                    <h2  class="mt-3" style="text-align: center; color: white">Interactive Analysis</h2>
                        
                        <img class="m-4" src="../graph.PNG" title="Graph" > 
                        
                    </div>
                        
                </div>
            </div>
        </div>
        
        <div class="container-fluid ">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <h2 style="text-align: left; ">Blogs</h2>
                    <div class="ui grid">

                        <div class="row m-2">
                                <div class="ui segment ">
                                    <div class="twelve wide computer sixteen wide mobile ten wide tablet column">
                                        <h3 class="ui small header">July 4, 2022</h3>
                                        <a href="#"><p>How to use AI for research</p></a>
                                    </div>
                                </div>
                        </div>  

                        <div class="row m-2">
                            <div class="ui segment ">
                                <div class="left floated twelve wide computer sixteen wide mobile ten wide tablet column">
                                    <h3 class="ui small header">May 14, 2022</h3>
                                    <a href="#"><p>Recommendations for tools to use</p></a>
                                </div>
                            </div>    
                        </div> 
                        
                        <div class="row m-2">
                            <div class="ui segment ">
                                <div class="left floated twelve wide computer sixteen wide mobile ten wide tablet column">
                                    <h3 class="ui small header">May 14, 2022</h3>
                                    <a href="#"><p>Python for Machine learning research</p></a>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        


        <div class="container-fluid">
            <div class="container">
                    
             </div>
        </div>
    </main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>