<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project details</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5"> 
            <div class="d-flex flex-column">
                
                <div class="d-flex">
                    <h3>Name of the project X</h1> 
                        
                </div>
                
                    <div class=" d-flex ">
                        <div class="d-flex">
                            <img class="mt-1" src="../company_logo.jpg" title="Profile Picture" width="140" height="140"> 
                            <div class="ms-3">
                                <label><b>Names of involved researchers: </b></label><br>
                                <label>Mustermann, Mustermann, Mustermann, Mustermann, Mustermann</label><br>
                                <label><b>Starting date: </b> 15.09.2022</label><br>
                            </div>
                        </div>
                        
                    
                </div>
                
                
                <hr>

                <div class="mt-3">
                    <h3>Project Description</h3>
                    
                    <p class="mt-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                    
                </div>

                <hr>

                <div class="d-flex justify-content-around mt-3 mb-3">

                    <div>
                        <label ><b> Dataset </b></label>
                        <div class = "mt-2">
                            <label ><b> Name of dataset: </b></label>
                            <label> Dataset X </label>
                        </div>   
                        <div class = "mt-2">
                            <label ><b> Link: </b></label>
                            <a href="#"> Link to dataset details page </a>
                        </div>  
                    </div>
                    <div>
                        <label ><b> Corporate contact person </b></label>
                        <div class = "mt-2">
                            <label ><b> Name: </b></label>
                            <label> Peter Adams </label>
                        </div>
                        <div class = "mt-2">
                            <label ><b> Email: </b></label>
                            <a href="#"> peter.adams@cool-company.com </a>
                        </div>
                    </div>
                </div>

                
                
                <div class="d-flex justify-content-between mt-4 ">
                  <button class="btn btn-success">Access dataset</button>
                  <button class="btn btn-success">Review uploaded documents</button>
                  <button class="btn btn-success">Upload new document</button>
                  <button class="btn btn-success">Close project</button>
               </div>

                
            </div>
        </div>
    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>