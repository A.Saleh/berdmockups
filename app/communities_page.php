<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Community page</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">



</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- The index site doesn't import the navbar.html since its using a slightly changed navbar design -->
        <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
                <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Support</a>
                        </li> 
                    </ul>
                    <a href="#" class = "ms-auto">
                        <button class="btn btn-light btn-sm me-1" type="button" onclick="window.location='login.php';">Log In</button>
                        <button class="btn btn-light btn-sm" type="button" onclick="window.location='register.php';">Sign Up</button>
                    </a>
                </div>
            </div>
        </nav> -->
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>
    <main> 
        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center main-container">
                    <h1 style="text-align: center;">BERD Communities</h1>
                    <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                    <form method="GET" action="search_community.php" class="d-inline-flex mt-2" id="index-searchbar">
                            <input class="form-control me-2" type="search" name="search" placeholder="Search for Communities ..." aria-label="Search">
                            <button class="btn btn-light me-2" type="button" onclick="document.location.href = `/search_community.php?search=+`">Search</button>
                            <button class="btn btn-light"  type="button" onclick="window.location='community_creation_page.php';">Create new community</button>
                    </form>  

                    


                </div>
            </div>
        </div>

        
        <!-- <div class="container-fluid bg-success">
            <div class="container">
                <div class="d-flex flex-column align-items-center min-height-container"> 
                    
                        <h2 class="mt-5" style="text-align: center; color: white" >New Communities</h2>
                        <a class="align-self-center rel-ml-1 " href="">See all</a>
                       
                
                    <div class="container">
                        <div class="d-flex flex-row justify-content-between mt-5">
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4" >Community A</h1>
                            </div>
                                <h3 class="me-5" style='color: white'>Community B</h1>  
                                <h3 class="me-5" style='color: white'>Community C</h1>  
                                <h3 style='color: white'>Community D</h1>         
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>             -->
        <div class="container-fluid bg-success">
            <div class="container">
                <div class="d-flex flex-column justify-content-center min-height-container">
                    <h2 style="text-align: center;">New Communities</h2>
                    <a class="align-self-center rel-ml-1 " href="">See all</a>

                    <div class="d-flex justify-content-evenly">
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4">Community A</h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4">Community B</h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="m-4">Community C</h3>
                            
                        </div>
                        <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                            <h3 class="mt-4">Community D</h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="container">
                <div class="d-flex flex-column align-items-center min-height-container"> 
                    <h2 class="mt-5" style="text-align: center;">Trending Communities</h2>
                    <a class="align-self-center rel-ml-1 " href="">See all</a>
                    <div class="container mb-4">
                        <div class="d-flex flex-row justify-content-between mt-5">
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community A</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community B</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community C</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="mt-4">Community D</h3>
                                
                            </div>        
                        </div>

                        <div class="d-flex flex-row justify-content-between mt-5">
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community E</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community F</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="m-4">Community G</h3>
                                
                            </div>
                            <div class="d-flex flex-column align-items-center bg-light mt-5" style="width: 235px; height: 215px;">
                                <h3 class="mt-4">Community H</h3>
                                
                            </div>         
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </main> 
    
    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>

</html>