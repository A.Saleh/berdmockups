<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        <div class="container d-flex flex-column mt-5">   
            <div class="d-flex" id="dataset"></div>
        </div>
        <div class="container d-flex flex-column mt-4">
            <div class="d-flex flex-column col-8">
                <div>
                    <h3>Comments</h3>
                    <hr>
                </div>
                <div class="d-flex flex-column" id="comments" style="overflow-y: auto; max-height: 500px;"></div>
                <div class="d-flex flex-column mt-2">
                    <label for="exampleFormControlTextarea1" class="form-label"><h4>Add a comment</h4></label>
                    <div class="d-flex align-items-center">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                        <div>
                            <button class="btn btn-light ms-2" type="submit">Submit</button>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn-light mt-2" type="submit">Add Image</button> 
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            /*Since there isn't a connected database yet, we need to import an array of datasets*/ 
            <?php include "./js/datasets.js"; ?>

            /*Since there isn't a connected database yet, we need to import the comments for the datasets*/
            <?php include "./js/comments.js"; ?>

            /*Find the needed data set over it's id*/ 
            var dataset;
            for (var i=0; i < datasets.length; i++) {
                if (<?php echo $_GET["id"]?> == datasets[i].id ) {
                    dataset = datasets[i];
                }
            }

            /*Find the comments for the dataset over it's id*/ 
            var dataset_comments = [];
            for (var i=0; i < comments.length; i++) {
                if (<?php echo $_GET["id"]?> == comments[i].id ) {
                    dataset_comments.push(comments[i]);
                }
            }

            /*Depending whether the dataset is open source of a marketplace dataset, you can either download
            or apply to use the dataset*/
            var button = "Download";
            if (dataset.access == "Marketplace") {
                button = "Apply";
            }

            /*Fill the dataset segment*/
            document.getElementById("dataset").innerHTML = `
                <div class="d-flex flex-column col-8">
                    <div>
                        <label class="bg-primary" style="font-size: large; color: white">&nbsp${dataset.date}&nbsp</label>
                        <label class="bg-secondary" style="font-size: large; color: white">&nbsp${dataset.type}&nbsp</label>
                        <label class="bg-danger" style="font-size: large; color: white">&nbsp${dataset.access}&nbsp</label>
                    </div>  
                    <div class="d-flex justify-content-between">
                        <h1 class="mt-4">${dataset.title}</h1> 
                        <div>
                            <button type="button" class="btn btn-light" onclick="history.back()">&#8592 Go Back</button>
                        </div>
                    </div>
                    <div class="mt-2">
                        <b>Author(s)</b>:
                        <label class="mt-1">${dataset.authors.join(", ")}</label>
                    </div>
                    <div class="mt-1">
                        <font size="2">Uploaded on ${dataset.uploaded}</font size>
                    </div>
                    <div class="mt-5">
                        <h3>Citation</h3>
                        <hr>
                        <p>${dataset.cite} ${dataset.doi}</p>
                    </div>
                    
                    
                    <div class="mt-4">
                        <h3>Description</h3>
                        <hr>
                        <p class="mt-1">${dataset.text}</p>
                        <hr>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <button type="button" class="btn btn-light" onclick="checkValue()">${button}</button>
                            <label class="ps-2"><i>&copy ${dataset.copyright}</i></label>
                        </div>
                        <div>
                            <button type="button" class="btn btn-light mx-2 mt-2">&nbspSave</button>
                        </div> 
                    </div>
                    
                </div>
                <div class="d-flex col-1"></div>
                <div class="d-flex flex-column ps-2 col-2" style="border: black 1px solid; height: 450px;">
                    <h5 class="mt-4">Metadata</h5>
                    <hr>
                    <u><b>DOI</b></u>
                    ${dataset.doi}
                    <u class="mt-2"><b>Data Type</b></u>
                    ${dataset.data}
                    <u class="mt-2"><b>Resource Type</b></u>
                    ${dataset.type}
                    <u class="mt-2"><b>Language</b></u>
                    ${dataset.language}
                    <u class="mt-2"><b>Data Format</b></u>
                    ${dataset.format}
                    <u class="mt-2"><b>Size</b></u>
                    ${dataset.size}
                </div>
                
                
                `

            /*Checks how many comments there are for the dataset. If 0, it will show a default message*/
            var count = 0;
            
            /*Fill the comment segment*/
            document.getElementById("comments").innerHTML = `
                ${dataset_comments.map(function(data) {
                    count++;
                    return `
                    <div class="d-flex align-items-center mt-2">
                        <b>${data.author}</b>
                        <font size="2"><i>&nbsp commented on ${data.date}</i></font size>
                    </div>
                    <p class="mt-2">${data.text}</p>
                    <div class="d-flex mb-4">
                        <a href="#"><i class="bi bi-hand-thumbs-up"></i></a>
                        <font size="3">&nbsp${data.likes}</font size>
                    </div>
                    `
                }).join("")}
            `

            if (count==0) {
                document.getElementById("comments").innerHTML = `<h4 class="mt-2 mb-5">No comments added yet.</h4>`
            }

            function checkValue() {
                if (button == "Apply") {
                    document.location.href = `/apply_for_dataset.php?id=${dataset.id}`
                }
            }

        </script>
    </main>

    <footer>
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>