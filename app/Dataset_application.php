<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dataset application</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        
        <div class="ui container fluid">
            <div class="row pb-0 mt-4">
                <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                    <div class="flex rel-mb-1">
                        <h3 class="ui header mb-0">Dataset application</h2>
                    </div>
                </div>
            </div>
        </div>    
        <!-- Application form -->
        <div class="container d-flex">
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column col-11">

                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault"><b> Project Name: </b></label>
                    <input class="ms-3" type="text" placeholder="Type something here .." >
                </div>

               <label class="form-check-label mt-2" for="flexCheckDefault"><b>Questions related to your planned research with this dataset</b></label>

               <!-- Question 1 -->
               <label class="form-check-label mt-2" for="flexCheckDefault">
               1- Please provide a paragraph summary describing the purpose and scope of your project, noting your research questions.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 2 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
               2- For the dataset that you are applying for. describe how this exact data is relevant to your project.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 3 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
               3- List any additional datasets that you plan to use as part of this project.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 4 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
                4- List each co-author for this project and include 
                    <ol type="1">
                        <li>name</li>
                        <li>institution</li>
                        <li>title or position</li>
                    </ol>
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 5 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
                5- Does your project involve external funding or support ? If yes, please
                    <ol type="1">
                        <li>describe the source</li>
                        <li>list what the funding pays for (i.e., salaries, RAs, conferences etc.)</li>
                        <li>tell us whether the funder requires copyright or editorial control.</li>
                    </ol>
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 6 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
                6- Do you plan to reveal manufacturer and/or brand names in your papers and publications? If yes,
                list the manufacturer and the brands. If you do not know yet, you can provide this information later.
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>
                <!-- Question 7 -->
                <label class="form-check-label mt-2" for="flexCheckDefault">
                7- Do you plan to or anticipate that you may explore any of the following topics: anti-trust, collusions, or violations of law? <br>
                <b>Note: We do not allow researchers to report brand or manufacturer names for research on these topics. </b>
               </label>
               <div>
                  <textarea class="form-control mt-2" style="height:200px; font-size:14pt;" .....> </textarea>
                </div>

                <div class="mt-4">
                  <input type="checkbox">
                  <label class="ms-3">I hereby agree to the terms and conditions of data use.</label>
               </div>

               <div class="d-flex justify-content-between mt-4 ">
                  <button class="btn btn-success">Previous</button>
                  <button class="btn btn-success">Save progress</button>
                  <button class="btn btn-success">Submit</button>
               </div>
            </div>

            
            
         </div>
         <!--  -->
        


    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>