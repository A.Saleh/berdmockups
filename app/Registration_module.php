<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registeration Module</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script>
        <!-- End of navigation bar-->
    </header>

    <main>
        
        <div class="ui container fluid">
            <div class="row pb-0 mt-4">
                <div class="sixteen wide mobile sixteen wide tablet thirteen wide computer column">
                    <div class="flex rel-mb-1">
                        <h3 class="ui header mb-0">Data protection workshop</h2>
                    </div>
                </div>
            </div>
        </div>    
        <!-- Application form -->
        <div class="container d-flex">
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column col-11">

                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault"><b> 09.11.2022, 12:00 - 12:45 </b></label>
                </div>

                <div class="mt-2">
                    <label class="form-check-label mt-2" for="flexCheckDefault"><b> To register for this event, please enter your email address below.
                        You will receive the link to the event one day before the event. </b></label>
                </div>

                <label class="form-check-label mt-2" for="flexCheckDefault">
                    Email address *
                </label>

                <div class="mt-2">
                    <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                    <input type="text" class="form-control mt-2" id="inputName" placeholder="i.e user.name@uni-mannheim.de">
                </div>

                <label class="form-check-label mt-2" for="flexCheckDefault">
                    Gender *
                </label>

                <div class="mt-2">
                    <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                    <input type="radio"  id="inputName">
                    <label>Male</label>
                    <input class="ms-3" type="radio"  id="inputName">
                    <label>Female</label>
                </div>

                <label class="form-check-label mt-2" for="flexCheckDefault">
                    Eduation *
                </label>

                <div class="mt-2">
                    <!-- <div class="input-group-text"><i class="bi bi-card-text"></i></div> -->
                    <input type="text" class="form-control mt-2" id="inputName" placeholder="i.e Bachelor of Science or Master of Science ..etc">
                </div>

               
                <div class="mt-4">
                  <input type="checkbox">
                  <label class="ms-3">I have proof of passing the prerequisities of this module</label>
               </div>

               <div class="mt-4">
                  <input type="checkbox">
                  <label class="ms-3">I have basic knowledge in Python, Data structures and algorithms.</label>
               </div>


               <div class="mt-4">
                  <input type="checkbox">
                  <label class="ms-3">I confirm that my email address will be saved for registeration. Please read the information on <u>data protection</u>.</label>
               </div>

               <div class="d-flex justify-content-between mt-4 ">
                  <button class="btn btn-success"><a href="Confirmation_of_Registration.php" style="color : white">Submit</a></button>
                  
               </div>
            </div>

            
            
         </div>
         <!--  -->
        


    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>