<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Dashboard</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">


    <!-- collapse -->
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>

<body class ="d-flex flex-column min-vh-100">
    <header>
        <!-- Navigation bar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
               <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="berd_home.php">Home</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="communities_page.php">Communities</a>
                        </li> 
                     </ul>
                     <a href="#" class = "ms-auto">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="28" height="28"> 
                     </a>
               </div>
            </div>
        </nav>
        <!-- End of navigation bar-->
    </header>

    <main>
    <div class="invenio-page-body">
        <div class="ui container fluid page-subheader-outer with-submenu rel-pt-2 ml-0-mobile mr-0-mobile">
            <!-- User section -->
            <div class="ui container relaxed grid page-subheader mr-0-mobile ml-0-mobile">
                <div class="row pb-0 m-3">
                    <div class="container d-flex justify-content-between">
                        <div class="d-flex">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="140" height="140">
                            <div class="ms-3">
                                <label><b>John Doe</b></label><br>
                                <label><b>jdoe</b></label><br>
                                <label><b>jdoe@example.org</b></label><br>
                            </div>
                        </div>
                        <div class="sixteen wide mobile sixteen wide tablet three wide computer right aligned middle aligned column">
                            <a href="/uploads/new?community=62298ca9-6946-4ab9-a5f5-edfdd1e02bce" class="ui positive button labeled icon rel-mt-1">
                            <i class="setting icon" aria-hidden="true"></i> Manage account </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of User section -->
            <!-- Tabs -->
            <div class="ui container secondary pointing stackable menu page-subheader pl-0 pr-0">
                <a class="item" onclick="javascript:location.href='Dashboard_favorites.php'"><i aria-hidden="true" class="settings icon"></i>Favorites</a>
                <a class="item" onclick="javascript:location.href='Dashboard_uploads.php'" ><i aria-hidden="true" class="settings icon"></i>Uploads & Proposals</a>
                <a class="item" onclick="javascript:location.href='Dashboard_communities.php'"><i aria-hidden="true" class="settings icon"></i>Communities</a>
                <a class="item" onclick="javascript:location.href='Dashboard_training.php'"><i aria-hidden="true" class="settings icon"></i>Training & Education</a>
                <a class="item active"><i aria-hidden="true" class="settings icon"></i>Resources</a>
            </div>
            <!-- End of Tabs -->
            <!-- Content of the Resource tab -->
            <div class="ui container grid communities-settings m-4">
                <div class="container full-height-container d-flex flex-column">
                    
                    <!-- Open source datasets section -->
                    <div class="mt-3 ms-4">
                        <h4>Open source datasets </h4>
                        <div class="mt-2 row">
                            <div class="col-sm-6">
                                <h5>News and updates of saved datasets</h5>
                            
                                <div class="d-flex justify-content-between">
                                    <p class="p-2">Updated metadata available</p> 
                                    <p class="p-2">14</p>
                                </div>
                            
                                <hr>
                                <div class="d-flex justify-content-between">
                                    <p class="p-2">Updated version available</p> 
                                    <p class="p-2">14</p>
                                </div>
                                <hr>
                                <div class="d-flex justify-content-between">
                                    <p class="p-2">New comments on datasets</p> 
                                    <p class="p-2">14</p>
                                </div>

                            </div>
                            <div class="col-sm-6 ">
                                <h5>Recommended Datasets</h5>
                                <div class="d-flex justify-content-evenly">
                                    <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 150px;">
                                        <h3 class="mt-4">Data set 1</h3>
                                        
                                    </div>
                                    <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 150px;">
                                        <h3 class="mt-4">Data set 2</h3>
                                        
                                    </div>
                                    <div class="d-flex flex-column align-items-center bg-light m-3" style="width: 235px; height: 150px;">
                                        <h3 class="mt-4">Data set 3</h3>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Open source datasets section -->
                    <hr>
                    <!-- Data Marketplace section  -->
                    <div class="mt-3 ms-4">

                        <h4>Data Marketplace </h4>
                        <h5> Dataset applications </h5>

                        <div class="mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Dataset</th>
                                        <th>Organization</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>SW Sales</th>
                                        <th>Mercedes Benz </th>
                                        <th>upload documents</th>
                                        <th><button class="btn btn-success">Edit application</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i><i class="bi bi-trash fs-4"></i></th>

                                    </tr>

                                    <tr>
                                        <th>Retail Sales</th>
                                        <th>Mercedes Benz </th>
                                        <th>confirm & submit</th>
                                        <th><button class="btn btn-success">Edit application</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i><i class="bi bi-trash fs-4"></i></th>

                                    </tr>

                                    <tr>
                                        <th>Productions costs</th>
                                        <th>Mercedes Benz </th>
                                        <th>accepted</th>
                                        <th><button class="btn btn-success">Edit application</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i><i class="bi bi-trash fs-4"></i></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <h5> Acquired datasets </h5>

                        <div class="mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Dataset</th>
                                        <th>Organization</th>
                                        <th>Actions</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>SW Sales</th>
                                        <th>Mercedes Benz </th>
                                        <th><button class="btn btn-success">See files</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i></th>

                                    </tr>

                                    <tr>
                                        <th>Retial Sales</th>
                                        <th>Mercedes Benz </th>
                                        <th><button class="btn btn-success">See files</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i></th>

                                    </tr>

                                    <tr>
                                        <th>Production costs</th>
                                        <th>Mercedes Benz </th>
                                        <th><button class="btn btn-success">See files</button></th>
                                        <th><i class="bi bi-person-fill fs-4"></i></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- Projects section -->
                  

                        <h5>Projects </h5>

                        <div class="mt-3 row">
                            
                            <div class="col-sm-3">
                                
                                    <div class="dropdown mb-3 mt-3">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                        Status: Select
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="relation">
                                            <li><a class="dropdown-item" >pending</a></li>
                                            <li><a class="dropdown-item" >in progress</a></li>
                                            <li><a class="dropdown-item" >closed</a></li>
                                        </ul>
                                    </div>
                            </div>
                            
                            <div class="col-sm-3">
                                        
                                    <div class="dropdown mb-3 mt-3">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                        Subject area: Select
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="relation">
                                            <li><a class="dropdown-item" >Marketing</a></li>
                                            <li><a class="dropdown-item" >Economics</a></li>
                                            <li><a class="dropdown-item" >Finance</a></li>
                                            <li><a class="dropdown-item" >Social Science</a></li>
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-sm-3">
                                
                                    <button class="btn btn-success mt-3" type="button" id="relation" data-bs-toggle="dropdown" aria-expanded="false">
                                    Apply filters
                                    </button>
                                    
                            </div>

                        </div>


                        <div class="mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Short name</th>
                                        <th>Topic</th>
                                        <th>Status</th>
                                        <th>Collaborators</th>
                                        <th>Associated datasets</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Test</th>
                                        <th>Test </th>
                                        <th>Test</th>
                                        <th>Names</th>
                                        <th>Datasets</th>
                                        <th><a href= "Project_detail_page.php"><button class="btn btn-success" >View details</button></a></th>

                                    </tr>

                                    <tr>
                                        <th>Test</th>
                                        <th>Test </th>
                                        <th>Test</th>
                                        <th>Names</th>
                                        <th>Datasets</th>
                                        <th><a href= "Project_detail_page.php"><button class="btn btn-success">View details</button></a></th>

                                    </tr>

                                    <tr>
                                        <th>Test</th>
                                        <th>Test </th>
                                        <th>Test</th>
                                        <th>Names</th>
                                        <th>Datasets</th>
                                        <th><a href= "Project_detail_page.php"><button class="btn btn-success">View details</button></a></th>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- End of Projects section -->
                        
                    </div>
                    <!-- End of Data marketplace section -->


                </div>
            </div>
            <!-- End of Content of the Resource tab -->
        </div>
    </div>
</main>

    <footer class="mt-auto">
        <!-- Footer -->
        <div id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

