<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Event Management</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <!-- Semantic UI -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">


    <!-- collapse -->
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body class ="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <div class="container">
               <a class="navbar-brand" href="berd_home.php"><b>BERD@NFDI</b></a>
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="berd_home.php">Home</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="Dashboard_favorites.php">Dashboard</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="upload.php">Upload</a>
                        </li> 
                        <li class="nav-item">
                           <a class="nav-link active" aria-current="page" href="communities_page.php">Communities</a>
                        </li> 
                     </ul>
                     <a href="#" class = "ms-auto">
                        <img class="mt-1" src="../user.jpg" title="Profile Picture" width="28" height="28"> 
                     </a>
               </div>
            </div>
        </nav>
        <!-- Navigation bar-->
      <!--  <div id="nav-placeholder"></div>
        <script>
            $(function(){
                $("#nav-placeholder").load("/html/navbar.html");
            });
        </script> -->
        <!-- End of navigation bar-->
    </header>

    <main>
        
          
        <!-- Application form -->
        <div class="container d-flex mt-4">
            <!-- <div class="d-flex col-1"></div> -->
            <div class="d-flex flex-column col-11">
                <h3 class="ui header mb-0">Events</h2>
                <div class="mt-3">
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Date</th>
                                <th>online</th>
                                <th>Start time</th>
                                <th>End time</th>
                                <th>Registered</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Data Protection Basics</th>
                                <th>2022-11-09</th>
                                <th>Yes</th>
                                <th>12:30</th>
                                <th>13:15</th>
                                <th>16</th>
                                <th>
                                    <a href="#"><i class="copy icon"></i></a>
                                    <a href="#"><i class="edit icon"></i></a>
                                    <a href="#"><i class="trash icon"></i></a>
                                    <button class="btn btn-success"><a href="Registration_registers_management.php" style="color : white">Show registrations</a></button>
                                    
                                </th>

                            </tr>
                            <tr>
                                <th>GDPR applicability</th>
                                <th>2022-11-23</th>
                                <th>No</th>
                                <th>12:30</th>
                                <th>12:45</th>
                                <th>8</th>
                                <th>
                                    <a href="#"><i class="copy icon"></i></a>
                                    <a href="#"><i class="edit icon"></i></a>
                                    <a href="#"><i class="trash icon"></i></a>
                                    <button class="btn btn-success"><a href="Registration_registers_management.php" style="color : white">Show registrations</a></button>
                                    
                                </th>

                            </tr>

                        </tbody>
                    </table>
                    <div class="d-flex justify-content-between mt-4 ">
                        <button class="btn btn-success">Create an event</button>
                        
                    </div>
                </div>

                
            </div>

            
            
         </div>
         <!--  -->
        


    </main>


    <footer class="mt-auto">
        <!-- Footer -->
        <div class="mt-5" id="footer-placeholder"></div>
        <script>
            $(function(){
                $("#footer-placeholder").load("/html/footer.html");
            });
        </script>
        <!-- End of footer -->
    </footer>
    
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>
</html>